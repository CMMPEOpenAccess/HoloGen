﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Hierarchy.OptionTypes;
using System.Windows;
using System.Windows.Controls;

namespace HoloGen.UI.OptionEditors
{
    /// <summary>
    /// User interface selector for different <see cref="Option" /> types.
    /// </summary>
    public class NodeDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate BooleanDataTemplate { get; set; }
        public DataTemplate IntegerDataTemplate { get; set; }
        public DataTemplate DoubleDataTemplate { get; set; }
        public DataTemplate SelectDataTemplate { get; set; }
        public DataTemplate LargeTextDataTemplate { get; set; }
        public DataTemplate TextDataTemplate { get; set; }
        public DataTemplate PathDataTemplate { get; set; }
        public DataTemplate PathListDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item == null) return null;

            // TODO: Replace me with a static type dictionary
            // Issue #28
            var node = item as INode;
            if (node is BooleanOption)
                return BooleanDataTemplate;
            if (node is IntegerOption)
                return IntegerDataTemplate;
            if (node is DoubleOption)
                return DoubleDataTemplate;
            if (node is ISelectOption)
                return SelectDataTemplate;
            if (node is LargeTextOption)
                return LargeTextDataTemplate;
            if (node is TextOption)
                return TextDataTemplate;
            if (node is PathOption)
                return PathDataTemplate;
            if (node is PathListOption) return PathListDataTemplate;

            return TextDataTemplate;
        }
    }
}