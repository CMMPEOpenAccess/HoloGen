﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using HoloGen.IO;
using HoloGen.UI.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace HoloGen.UI.OptionEditors
{
    /// <summary>
    /// User interface editor for <see cref="PathOption" />s.
    /// </summary>
    public partial class PathEditor : UserControl, INotifyPropertyChanged, IDataErrorInfo
    {
        private PathOption _option;

        // ReSharper disable once NotAccessedField.Local
        private string _selectedPath = string.Empty;

        public PathEditor()
        {
            InitializeComponent();

            Loaded += Editor_Loaded;
        }

        public string SelectedPath
        {
            get => _option?.Value.FullName ?? "";
            set
            {
                _selectedPath = value;
                _option.Value = new FileInfo(value);
                OnPropertyChanged("SelectedPath");
                OnPropertyChanged("Value");
                OnPropertyChanged("ViewButtonEnabled");
            }
        }

        public bool ViewButtonEnabled => _option != null && _option.Valid;

        public string Error => string.Empty;

        public string this[string columnName]
        {
            get
            {
                var error = _option?.Error;
                return error != null && error.Count > 0 ? error[0] : string.Empty;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Editor_Loaded(object sender, RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            _option = (PathOption) DataContext;
            OnPropertyChanged("SelectedPath");
            OnPropertyChanged("Value");
            OnPropertyChanged("ViewButtonEnabled");
            _option.PropertyChanged += Option_PropertyChanged;
        }

        private void Option_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Value");
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                EnsurePathExists = true,
                EnsureFileExists = true,
                AllowNonFileSystemItems = false,
                Multiselect = false
            };
            if (_option.IsFolder) dialog.IsFolderPicker = true;
            var filterParts = _option.Filter.Split('|');
            for (var i = 0; i < filterParts.Length; i += 2) dialog.Filters.Add(new CommonFileDialogFilter(filterParts[i + 0], filterParts[i + 1]));
            var result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok) SelectedPath = dialog.FileName;
        }

        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
            if (_option.IsFolder)
            {
                try
                {
                    var proc = new ProcessStartInfo
                    {
                        FileName = SelectedPath,
                        Verb = "open"
                    };
                    Process.Start(proc);
                }
                catch (Exception)
                {
                    // Ignore errors
                }
            }
            else
            {
                var file = new FileInfo(SelectedPath);
                var res = new JsonImageFileLoader().Load(file);
                var error = res.Item1;
                var loaded = res.Item2;
                if (error == null || error != "")
                    ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                        HoloGen.Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                        error);
                else
                    TabHandlerFramework.GetActiveHandler()?.AddTab(loaded, file);
            }
        }
    }
}