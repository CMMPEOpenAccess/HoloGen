﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Serial;
using System;
using System.IO;

namespace HoloGen.IO
{
    /// <summary>
    /// Saver for files in a JSON format
    /// </summary>
    public class JsonFileSaver<S, T> : FileSaver<S, T>
        where S : HierarchySaveable, new()
        where T : HierarchyVersion, new()
    {
        public override string Save(FileInfo file, S content)
        {
            if (file == null) return Resources.Properties.Resources.JsonFileLoader_Load_ErrorNullInputFile;

            var serialiser = new JsonSerialiser<S, T>();

            var res = serialiser.Serialise(content);

            var resStat = res.Item1;
            var resCont = res.Item2;

            switch (resStat)
            {
                case SerialiseResult.Success:
                    break;
                case SerialiseResult.NoSuchFile:
                    return Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileDoesNotExist;
                case SerialiseResult.UnknownError:
                    return Resources.Properties.Resources.JsonFileLoader_Load_ErrorAnUnknownErrorOccured;
                default:
                    return Resources.Properties.Resources.JsonFileLoader_Load_ErrorAnUnknownErrorOccured;
            }

            try
            {
                File.WriteAllText(file.FullName, resCont);
            }
            catch (PathTooLongException)
            {
                return Resources.Properties.Resources.JsonFileSaver_Save_ErrorUnableToSaveFileFilePathIsTooLong;
            }
            catch (DirectoryNotFoundException)
            {
                return Resources.Properties.Resources.JsonFileSaver_Save_ErrorUnableToSaveFileDirectoryNotFound;
            }
            catch (UnauthorizedAccessException)
            {
                return Resources.Properties.Resources.JsonFileSaver_Save_ErrorUnableToSaveFileInadequateFileAccessPermissions;
            }
            catch (Exception)
            {
                return Resources.Properties.Resources.JsonFileSaver_Save_ErrorUnableToSaveFile;
            }

            return "";
        }
    }
}