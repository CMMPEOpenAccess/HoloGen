﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using csmatio.io;
using csmatio.types;
using HoloGen.Image;
using System.Collections.Generic;
using System.IO;

namespace HoloGen.IO.Convertors.Export
{
    /// <summary>
    /// Handle exporting images to a *.mat format
    /// </summary>
    public class MatExporter
    {
        public string Export(FileInfo file, ComplexImage image)
        {
            var toSave = new List<MLArray> {Convert2DArray(image)};

            var mfr = new MatFileWriter(file.FullName, toSave, false);

            return "";
        }

        private MLSingle Convert2DArray(ComplexImage input)
        {
            var values = input.Values;
            var reals = new float[values.GetLength(0)][];
            var imags = new float[values.GetLength(0)][];

            for (var i = 0; i < values.GetLength(0); i++)
            {
                reals[i] = new float[values.GetLength(1)];
                imags[i] = new float[values.GetLength(1)];

                for (var j = 0; j < values.GetLength(1); j++)
                {
                    reals[i][j] = (float) values[i, j].Real;
                    imags[i][j] = (float) values[i, j].Imaginary;
                }
            }

            return new MLSingle("TARGET", reals, imags);
        }
    }
}