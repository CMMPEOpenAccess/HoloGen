﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

namespace HoloGen.SLMControl.Converter
{
    internal class SLMImageWriter
    {
        private readonly Func<byte, byte, byte, bool> _binariser;
        private readonly uint _bitOffset;
        private readonly uint _bitRate;
        private readonly uint _inputResX;
        private readonly uint _inputResY;
        private readonly uint _outputResX;
        private readonly uint _outputResY;
        private readonly uint _packingDensity;
        private uint _currentBit;
        private uint _imagesPackedSoFar;
        private Bitmap _outputBitmap;
        private byte[] _outputBytes;
        private BitmapData _outputData;

        public SLMImageWriter(
            uint bitRate,
            uint bitOffset,
            uint packingDensity,
            uint inputResX,
            uint inputResY,
            uint outputResX,
            uint outputResY,
            Func<byte, byte, byte, bool> binariser)
        {
            Debug.Assert(bitRate >= 1);
            Debug.Assert(packingDensity >= 1);
            Debug.Assert(inputResX >= 1);
            Debug.Assert(inputResY >= 1);
            Debug.Assert(outputResX >= 1);
            Debug.Assert(outputResY >= 1);
            Debug.Assert(inputResX * inputResY * packingDensity <= outputResX * outputResY * bitRate);
            Debug.Assert(bitRate + bitOffset <= 24);
            Debug.Assert(_bitOffset % 8 == 0);

            _bitRate = bitRate;
            _bitOffset = bitOffset;
            _packingDensity = packingDensity;
            _inputResX = inputResX;
            _inputResY = inputResY;
            _outputResX = outputResX;
            _outputResY = outputResY;
            _binariser = binariser;

            ResetOutputBitmap();
        }

        private void ResetOutputBitmap()
        {
            _outputBitmap = new Bitmap((int) _outputResX, (int) _outputResY, PixelFormat.Format24bppRgb);
            _outputData = _outputBitmap.LockBits(
                new Rectangle(0, 0, (int) _outputResX, (int) _outputResY),
                ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb
            );
            _outputBytes = new byte[Math.Abs(_outputData.Stride) * _outputData.Height];
            System.Runtime.InteropServices.Marshal.Copy(_outputData.Scan0, _outputBytes, 0, Math.Abs(_outputData.Stride) * _outputData.Height);
        }

        public bool NeedNewOutput()
        {
            return _imagesPackedSoFar >= _packingDensity;
        }

        public bool ResetOutputIfFull(out Bitmap output)
        {
            if (NeedNewOutput())
            {
                output = GetOutput();
                ResetOutputBitmap();
                return true;
            }

            output = null;
            return false;
        }

        public Bitmap GetOutput()
        {
            System.Runtime.InteropServices.Marshal.Copy(_outputBytes, 0, _outputData.Scan0, Math.Abs(_outputData.Stride) * _outputData.Height);
            _outputBitmap.UnlockBits(_outputData);
            _imagesPackedSoFar = 0;
            _currentBit = 0;
            return _outputBitmap;
        }

        public void WriteImage(Bitmap inputBitmap)
        {
            Debug.Assert(inputBitmap.Size.Width == _inputResX);
            Debug.Assert(inputBitmap.Size.Height == _inputResY);
            Debug.Assert(_bitOffset % 8 == 0);

            // WARNING: GDI+ Uses the order blue, green, red!
            var offset = (_bitOffset / 8 + 2) % 3;

            // Lock the input bitmap for editting
            var inputData = inputBitmap.LockBits(
                new Rectangle(0, 0, (int) _inputResX, (int) _inputResY),
                ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb
            );
            var inputBytes = new byte[Math.Abs(inputData.Stride) * inputData.Height];
            System.Runtime.InteropServices.Marshal.Copy(inputData.Scan0, inputBytes, 0, Math.Abs(inputData.Stride) * inputData.Height);

            // Get the current location in the output image
            long bout = 3 * _imagesPackedSoFar * _inputResX * _inputResY / _bitRate;

            for (long bin = 0; bin < _inputResX * _inputResY; bin++)
            {
                // WARNING: GDI+ Uses the order blue, green, red!
                if (_binariser(inputBytes[bin * 3 + 2], inputBytes[bin * 3], inputBytes[bin * 3 + 1]))
                {
                    byte change = 128;
                    change >>= (int) _currentBit % 8;
                    _outputBytes[bout + _currentBit / 8 + offset] |= change;
                }

                // Increment the currently editted bit
                _currentBit++;
                if (_currentBit >= _bitRate)
                {
                    bout += 3;
                    _currentBit = 0;
                }
            }

            inputBitmap.UnlockBits(inputData);
            _imagesPackedSoFar++;
        }
    }
}