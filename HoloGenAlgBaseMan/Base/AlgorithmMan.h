// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <complex>
#include <vector>
#include <map>
#include "../../HoloGenAlgBaseCuda/Base/Export/AlgorithmWrap.h"
#include "../../HoloGenAlgBaseCuda/Base/Export/MetricTypeCuda.h"
#include "QuantiserMan.h"
#include "IlluminationType.h"
#include "InitialSeedType.h"
#include "MetricType.h"
#include <functional>

namespace HoloGen {
	namespace Alg {
		namespace Base {
			namespace Cuda {
				enum class MetricTypeCuda;
			}
		}
	}
}

using namespace System;
using namespace System::Numerics;
using namespace System::Collections::Generic;
using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Managed;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Managed
			{
				/// Top-level algorithm wrapper in Managed C++
				public ref class AlgorithmMan
				{
				private:

					AlgorithmWrap* Wrap;

				protected:
					AlgorithmMan(AlgorithmWrap* wrap) :
						SLMResolutionX(0),
						SLMResolutionY(0)
					{
						Wrap = wrap;
					}

					~AlgorithmMan()
					{
						delete Wrap;
					}

				private:

					static IlluminationTypeCuda TranslateIlluminationType(IlluminationType input)
					{
						switch (input)
						{
						case HoloGen::Alg::Base::Managed::IlluminationType::File:
							return IlluminationTypeCuda::File;
						case HoloGen::Alg::Base::Managed::IlluminationType::Gaussian:
							return IlluminationTypeCuda::Gaussian;
						case HoloGen::Alg::Base::Managed::IlluminationType::Planar:
							return IlluminationTypeCuda::Planar;
						default:
							throw gcnew Exception("Unknown illumination type!");
						}
					}

					static InitialSeedTypeCuda TranslateInitialSeedType(InitialSeedType input)
					{
						switch (input)
						{
						case HoloGen::Alg::Base::Managed::InitialSeedType::File:
							return InitialSeedTypeCuda::File;
						case HoloGen::Alg::Base::Managed::InitialSeedType::BackProject:
							return InitialSeedTypeCuda::BackProject;
						case HoloGen::Alg::Base::Managed::InitialSeedType::Random:
							return InitialSeedTypeCuda::Random;
						default:
							throw gcnew Exception("Unknown initial seed source!");
						}
					}

					static MetricType TranslateMetricType(MetricTypeCuda input)
					{
						switch (input)
						{
						case MetricTypeCuda::MeanSquaredError:
							return MetricType::MeanSquaredError;
						case MetricTypeCuda::Efficiency:
							return MetricType::Efficiency;
						default:
							throw gcnew Exception("Unknown initial seed source!");
						}
					}

					int SLMResolutionX;
					int SLMResolutionY;

				public:

					array<Complex, 2>^ GetResult()
					{
						return Convert(Wrap->GetResult());
					}

					void SetBaseParameters(
						int SLMResolutionX,
						int SLMResolutionY,
						int MaxIterations,
						float MaxError,
						int ReportingSteps,
						bool RandomisePhase,
						bool NormaliseTarget,
						bool TieNormalisationToZero,
						IlluminationType IlluminationType,
						InitialSeedType InitialSeedType,
						float IlluminationPower)
					{
						Wrap->SetBaseParameters(
							SLMResolutionX,
							SLMResolutionY,
							MaxIterations,
							MaxError,
							ReportingSteps,
							RandomisePhase,
							NormaliseTarget,
							TieNormalisationToZero,
							TranslateIlluminationType(IlluminationType),
							TranslateInitialSeedType(InitialSeedType),
							IlluminationPower);

						this->SLMResolutionX = SLMResolutionX;
						this->SLMResolutionY = SLMResolutionY;
					}

					void SetQuantiser(QuantiserMan^ quantiser)
					{
						Wrap->SetQuantiser(quantiser->GetNativePointer());
					}

					void SetLogger(LoggerDelegate^ log)
					{
						Wrap->SetLogger(
							static_cast<LoggingCallback>(Marshal::GetFunctionPointerForDelegate(log).ToPointer()));
					}

					void SetTargetImage(array<Complex, 2>^ image)
					{
						Wrap->SetTargetImage(Convert(image));
					}

					void SetRegionImage(array<int, 2>^ image)
					{
						Wrap->SetRegionImage(Convert(image));
					}

					void SetIlluminationImage(array<Complex, 2>^ image)
					{
						Wrap->SetIlluminationImage(Convert(image));
					}

					void SetInitialImage(array<Complex, 2>^ image)
					{
						Wrap->SetInitialImage(Convert(image));
					}

					int RunStartup()
					{
						return Wrap->RunStartup();
					}

					Dictionary<MetricType, float>^ RunIterations()
					{
						std::map<MetricTypeCuda, float> nativeMetrics = Wrap->RunIterations();
						auto managedMetrics = gcnew Dictionary<MetricType, float>();

						for (const auto metric : nativeMetrics)
						{
							managedMetrics->Add(TranslateMetricType(metric.first), metric.second);
						}

						return managedMetrics;
					}

					int RunCleanup()
					{
						return Wrap->RunCleanup();
					}

				private:
					array<Complex, 2>^ Convert(std::vector<std::complex<float>> toConvert);
					std::vector<std::complex<float>> Convert(array<Complex, 2>^ toConvert);
					array<int, 2>^ Convert(std::vector<int> toConvert);
					std::vector<int> Convert(array<int, 2>^ toConvert);
				};
			}
		}
	}
}

#endif
