// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#include "../stdafx.h"
#pragma once
#include "TestHandlerMan.h"
#include "../../HoloGenAlgBaseCuda/Base/Export/TestHandlerWrap.h"

using namespace System;
using namespace System::Numerics;
using namespace HoloGen::Alg::Base::Managed;

// ReSharper disable CppExpressionWithoutSideEffects

Int64 TestHandlerMan::TestFFT(int resolution, int cycles, LoggerDelegate^ log, int testType)
{
	return TestHandlerWrap::TestFFT(
		resolution,
		cycles,
		static_cast<LoggingCallback>(Marshal::GetFunctionPointerForDelegate(log).ToPointer()),
		testType);
}

array<Complex, 2>^ TestHandlerMan::Convert(std::vector<std::complex<float>> toConvert, size_t nx, size_t ny)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t SIZE = toConvert.size();
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	array<Complex, 2>^ tempArr = gcnew array<Complex, 2>(ny, nx);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const size_t i = y * nx + x;
			tempArr[y, x] = Complex(toConvert[i].real(), toConvert[i].imag());
		}
	}
	return tempArr;
}

std::vector<std::complex<float>> TestHandlerMan::Convert(array<Complex, 2>^ toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = toConvert->GetLength(1);
	const size_t ny = toConvert->GetLength(0);
	auto tempVec = std::vector<std::complex<float>>(nx * ny);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const size_t i = y * nx + x;
			tempVec[i] = std::complex<float>(toConvert[y, x].Real, toConvert[y, x].Imaginary);
		}
	}
	return tempVec;
}

// ReSharper restore CppExpressionWithoutSideEffects
