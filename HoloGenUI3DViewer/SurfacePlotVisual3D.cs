﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HelixToolkit.Wpf;
using HoloGen.Image;
using HoloGen.Utils;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace HoloGen.UI.X3.Viewer
{
    /// <summary>
    /// Control for viewing a <see cref="ComplexImage" /> in 3D.
    /// </summary>
    public class SurfacePlotVisual3D : ModelVisual3D
    {
        public static readonly DependencyProperty ImageViewProperty =
            DependencyProperty.Register("ImageView", typeof(ImageView), typeof(SurfacePlotVisual3D),
                new UIPropertyMetadata(null, ModelChanged));
        // TODO: Tidy! - Issue #29

        private readonly ModelVisual3D _visualChild;

        public SurfacePlotVisual3D()
        {
            _visualChild = new ModelVisual3D();
            Children.Add(_visualChild);
        }

        public ImageView ImageView
        {
            get => (ImageView) GetValue(ImageViewProperty);
            set => SetValue(ImageViewProperty, value);
        }

        private static void ModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((SurfacePlotVisual3D) d).UpdateModel();
        }

        private void UpdateModel()
        {
            _visualChild.Content = CreateModel();
        }

        private Model3D CreateModel()
        {
            var plotModel = new Model3DGroup();

            var minX = 0;
            var maxX = ImageView.Dimension.XSize;
            var minY = 0;
            var maxY = ImageView.Dimension.YSize;
            var minZ = ImageView.Scale.MinValue;
            var maxZ = ImageView.Scale.MaxValue;
            var dimensionFactor = Math.Sqrt((maxX - minX) * (maxY - minY));
            var scaleMultZ = dimensionFactor / (maxZ - minZ);
            var lineThickness = dimensionFactor / 300;
            var fontSize = dimensionFactor / 50;
            var stepX = GetStepForRange(maxX - minX);
            var stepY = GetStepForRange(maxY - minY);
            var stepZ = GetStepForRange((int) (maxZ - minZ));


            var points = new Point3D[maxY, maxX];

            // set the texture coordinates by z-value or ColorValue
            var texcoords = new Point[maxY, maxX];
            for (var y = 0; y < maxY; y++)
            for (var x = 0; x < maxX; x++)
            {
                points[y, x] = new Point3D(x, y, ImageView.Values[y, x] * scaleMultZ);
                texcoords[y, x] = new Point(x, y);
            }

            var surfaceMeshBuilder = new MeshBuilder();
            surfaceMeshBuilder.AddRectangularMesh(points, texcoords);

            var surfaceModel = new GeometryModel3D(surfaceMeshBuilder.ToMesh(),
                MaterialHelper.CreateMaterial(new ImageBrush(BitmapUtils.ToBitmapImage(ImageView.ImageWithoutScale)), null, null, 1, 0));
            surfaceModel.BackMaterial = surfaceModel.Material;

            var axesMeshBuilder = new MeshBuilder();
            for (var x = 0; x < maxX; x += stepX)
            {
                var label = TextCreator.CreateTextLabelModel3D(x.ToString(), Brushes.Black, true, fontSize,
                    new Point3D(x, minY - fontSize * 2.5, minZ),
                    new Vector3D(0, 1, 0), new Vector3D(-1, 0, 0));
                plotModel.Children.Add(label);
            }

            {
                var label = TextCreator.CreateTextLabelModel3D("X-axis", Brushes.Black, true, fontSize,
                    new Point3D((minX + maxX) * 0.5,
                        minY - fontSize * 6, minZ * scaleMultZ),
                    new Vector3D(1, 0, 0), new Vector3D(0, 1, 0));
                plotModel.Children.Add(label);
            }
            for (var y = 0; y < maxY; y += stepY)
            {
                var label = TextCreator.CreateTextLabelModel3D(y.ToString(), Brushes.Black, true, fontSize,
                    new Point3D(minX - fontSize * 3, y, minZ),
                    new Vector3D(1, 0, 0), new Vector3D(0, 1, 0));
                plotModel.Children.Add(label);
            }

            {
                var label = TextCreator.CreateTextLabelModel3D("Y-axis", Brushes.Black, true, fontSize,
                    new Point3D(minX - fontSize * 6,
                        (minY + maxY) * 0.5, minZ * scaleMultZ),
                    new Vector3D(0, 1, 0), new Vector3D(-1, 0, 0));
                plotModel.Children.Add(label);
            }
            for (var z = 0; z < maxZ; z += stepZ)
            {
                var label = TextCreator.CreateTextLabelModel3D(z.ToString(), Brushes.Black, true, fontSize,
                    new Point3D(minX - fontSize * 3, maxY, z * scaleMultZ),
                    new Vector3D(1, 0, 0), new Vector3D(0, 0, 1));
                plotModel.Children.Add(label);
            }

            {
                var label = TextCreator.CreateTextLabelModel3D("Z-axis", Brushes.Black, true, fontSize,
                    new Point3D(minX - fontSize * 6, maxY,
                        (minZ + maxZ) * 0.5 * scaleMultZ),
                    new Vector3D(0, 0, 1), new Vector3D(1, 0, 0));
                plotModel.Children.Add(label);
            }

            var bb = new Rect3D(minX, minY, minZ * scaleMultZ, maxX - minX, maxY - minY, (maxZ - minZ) * scaleMultZ);
            axesMeshBuilder.AddBoundingBox(bb, lineThickness);

            var axesModel = new GeometryModel3D(axesMeshBuilder.ToMesh(), Materials.Black);

            plotModel.Children.Add(surfaceModel);
            plotModel.Children.Add(axesModel);

            return plotModel;
        }

        private int GetStepForRange(int range)
        {
            var step = 1;
            if (range / step < 10) return step;
            while (true)
            {
                step = step * 5;
                if (range / step < 10) return step;
                step = step * 2;
                if (range / step < 10) return step;
            }
        }
    }
}