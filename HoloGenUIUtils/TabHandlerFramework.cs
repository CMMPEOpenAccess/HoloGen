﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System.Collections.Generic;

namespace HoloGen.UI.Utils
{
    /// <summary>
    /// Framework for tab handlers so tabs don't need to be aware of the main window
    /// </summary>
    public class TabHandlerFramework
    {
        private static readonly List<ITabHandler> Handlers = new List<ITabHandler>();

        private static ITabHandler _handler;

        //ITabHandler MainWindowViewModel = TabHandlerFramework.GetActiveHandler();

        public static void RegisterHandler(ITabHandler handler)
        {
            Handlers.Add(handler);
        }

        public static void DeregisterHandler(ITabHandler handler)
        {
            Handlers.Remove(handler);
        }

        public static ITabHandler GetActiveHandler()
        {
            if (_handler != null && Handlers.Contains(_handler))
                return _handler;
            if (Handlers.Count > 0)
                return Handlers[0];
            return null;
        }

        public static void SetActiveTabHandler(ITabHandler handler)
        {
            if (Handlers.Contains(handler)) _handler = handler;
        }
    }
}