﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Image;
using HoloGen.IO;
using HoloGen.Options;
using System.IO;

namespace HoloGen.UI.Utils
{
    /// <summary>
    /// Generic object that can handle tabs. e.g. the main window
    /// </summary>
    public interface ITabHandler
    {
        ITabViewModel Selected { get; set; }

        void AddTab(ITabViewModel newTab, bool selectNewTab = true);
        void RemoveTab(ITabViewModel oldTab);

        // TODO: Remove these specific cases. I'll need to move SetupTabViewModel et. al. down a layer to do so.
        // The specific case code should go into the relevant menu etc. commands
        // Issue #23 - move to a service model
        void AddTab(OptionsRoot data, FileInfo fileLocation, bool selectNewTab = true);
        void AddTab(ComplexImage image, FileInfo fileLocation, bool selectNewTab = true, bool needsSaving = false);
        void AddTab(BatchDataObject data, FileInfo fileLocation, bool selectNewTab = true);
        void NewHologramSetupFile();
        void NewSLMControlTab();
        void NewBatchProcessingFile();
        void NewBrowserTab(FileInfo pdf);
    }
}