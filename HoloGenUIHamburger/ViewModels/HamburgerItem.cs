﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.UI.Utils.MVVM;
using System;

namespace HoloGen.UI.Hamburger.ViewModels
{
    /// <summary>
    /// For the top-level of a <see cref="Option" />s hierarchy, simple databinding isn't sufficient
    /// to work with simple data binding. Instead the <see cref="HierarchyPage" />s are wrapped in
    /// a <see cref="HamburgerItem" /> or <see cref="HamburgerMenuItem" /> and the
    /// <see cref="HamburgerTabViewModel" /> used to join the two.
    /// </summary>
    public abstract class HamburgerItem<T> : BindableBase
    {
        private Func<HamburgerItem<T>, bool> _callback;
        private T _data;
        private object _icon;
        private bool _isEnabled = true;
        private string _name;
        private string _tooltip;

        public object Icon
        {
            get => _icon;
            set => SetProperty(ref _icon, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string ToolTip
        {
            get => _tooltip;
            set => SetProperty(ref _tooltip, value);
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetProperty(ref _isEnabled, value);
        }

        public T Data
        {
            get => _data;
            set => SetProperty(ref _data, value);
        }

        public Func<HamburgerItem<T>, bool> Callback
        {
            get => _callback;
            set => SetProperty(ref _callback, value);
        }
    }
}