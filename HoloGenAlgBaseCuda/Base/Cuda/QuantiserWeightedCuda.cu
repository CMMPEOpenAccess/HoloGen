// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include "QuantiserWeightedCuda.cuh"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/complex.h>
#include <cuda_runtime.h>
#include <cufft.h>
#include <thrust/random.h>
#include <limits>
#include "../Export/Globals.h"

constexpr float PI = 3.14159265359;
constexpr float PI2 = 6.28318530718;

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Export;

struct quantiseDiscreteAmpWeighted
{
	const float MinSLMAmplitude;
	const float MaxSLMAmplitude;
	const int Levels;
	const float Weighting;

	quantiseDiscreteAmpWeighted(const float minSLMAmplitude, const float maxSLMAmplitude, const int levels, const float weighting) :
		MinSLMAmplitude(minSLMAmplitude),
		MaxSLMAmplitude(maxSLMAmplitude),
		Levels(levels),
		Weighting(weighting)
	{
		// TODO: Assert on the starting conditions
	}

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<thrust::complex<float>, float>& illumination)
	{
		const auto illuminationUnitVector = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputMagnitude = input.real(); //sqrtf(input.imag() * input.imag() + input.real() * input.real());
		const auto minIllumination = illuminationMagnitude * MinSLMAmplitude;
		const auto maxIllumination = illuminationMagnitude * MaxSLMAmplitude;
		const auto section = (maxIllumination - minIllumination) / (Levels - 1);
		const auto discretisedMagnitude = minIllumination + section * roundf((inputMagnitude - minIllumination) / section);
		const auto cappedMagnitude = discretisedMagnitude <= minIllumination
			? minIllumination
			: discretisedMagnitude >= maxIllumination
			? maxIllumination
			: discretisedMagnitude;
		const auto weightedMagnitude = (cappedMagnitude - inputMagnitude) * Weighting + inputMagnitude;
		return thrust::complex<float>(
			illuminationUnitVector.real() * weightedMagnitude,
			illuminationUnitVector.imag() * weightedMagnitude);
	}
};

struct quantiseContAmpWeighted
{
	const float MinSLMAmplitude;
	const float MaxSLMAmplitude;
	const float Weighting;

	quantiseContAmpWeighted(const float minSLMAmplitude, const float maxSLMAmplitude, const float weighting) :
		MinSLMAmplitude(minSLMAmplitude),
		MaxSLMAmplitude(maxSLMAmplitude),
		Weighting(weighting)
	{
		// TODO: Assert on the starting conditions
	};

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<thrust::complex<float>, float>& illumination)
	{
		const auto illuminationUnitVector = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputMagnitude = input.real(); //sqrtf(input.imag() * input.imag() + input.real() * input.real());
		const auto minIllumination = illuminationMagnitude * MinSLMAmplitude;
		const auto maxIllumination = illuminationMagnitude * MaxSLMAmplitude;
		const auto cappedMagnitude = inputMagnitude <= minIllumination
			                             ? minIllumination
			                             : inputMagnitude >= maxIllumination
			                             ? maxIllumination
			                             : inputMagnitude;
		const auto weightedMagnitude = (cappedMagnitude - inputMagnitude) * Weighting + inputMagnitude;
		return thrust::complex<float>(
			illuminationUnitVector.real() * weightedMagnitude,
			illuminationUnitVector.imag() * weightedMagnitude);
	}
};

struct quantiseDiscretePhaseWeighted
{
	const float MinSLMAngle;
	const float MaxSLMAngle;
	const int Levels;
	const float Section;
	const float WrapMinSLMAngle;
	const float WrapMaxSLMAngle;
	const float Weighting;

	quantiseDiscretePhaseWeighted(const float minSLMAngle, const float maxSLMAngle, const int levels, const float weighting) :
		MinSLMAngle(minSLMAngle),
		MaxSLMAngle(maxSLMAngle),
		Levels(levels),
		Section((MaxSLMAngle - MinSLMAngle) / (Levels - 1)),
		WrapMinSLMAngle(maxSLMAngle + fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0),
		WrapMaxSLMAngle(minSLMAngle - fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0),
		Weighting(weighting)
	{
		// TODO: Assert on the starting conditions
	};

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<float, float>& illumination)
	{
		const auto illuminationAngle = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputAngle = Globals::Arg(input.real(), input.imag());
		auto differenceAngle = fmod(inputAngle - illuminationAngle, PI2);
		while ((differenceAngle - MaxSLMAngle) > PI2)
			differenceAngle -= PI2;
		while ((differenceAngle - MinSLMAngle) < -PI2)
			differenceAngle += PI2;
		if (differenceAngle > MaxSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle < WrapMaxSLMAngle ? MaxSLMAngle : MinSLMAngle));
		if (differenceAngle < MinSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle > WrapMinSLMAngle ? MinSLMAngle : MaxSLMAngle));
		const auto discretisedAngle = illuminationAngle + MinSLMAngle + Section * roundf((differenceAngle - MinSLMAngle) / Section);
		const auto weightedAngle = (discretisedAngle - inputAngle) * Weighting + inputAngle;
		return thrust::polar<float>(illuminationMagnitude, weightedAngle);
	}
};

struct quantiseContPhaseWeighted
{
	const float MinSLMAngle;
	const float MaxSLMAngle;
	const float WrapMinSLMAngle;
	const float WrapMaxSLMAngle;
	const float Weighting;

	quantiseContPhaseWeighted(const float minSLMAngle, const float maxSLMAngle, const float weighting) :
		MinSLMAngle(minSLMAngle),
		MaxSLMAngle(maxSLMAngle),
		WrapMinSLMAngle(maxSLMAngle + fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0),
		WrapMaxSLMAngle(minSLMAngle - fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0),
		Weighting(weighting)
	{
		// TODO: Assert on the starting conditions
	};

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<float, float>& illumination)
	{
		const auto illuminationAngle = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputAngle = Globals::Arg(input.real(), input.imag());
		auto differenceAngle = fmod(inputAngle - illuminationAngle, PI2);
		while ((differenceAngle - MaxSLMAngle) > PI2)
			differenceAngle -= PI2;
		while ((differenceAngle - MinSLMAngle) < -PI2)
			differenceAngle += PI2;
		if (differenceAngle > MaxSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle < WrapMaxSLMAngle ? MaxSLMAngle : MinSLMAngle));
		if (differenceAngle < MinSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle > WrapMinSLMAngle ? MinSLMAngle : MaxSLMAngle));
		const auto weightedAngle = (differenceAngle + illuminationAngle - inputAngle) * Weighting + inputAngle;
		return thrust::polar<float>(illuminationMagnitude, weightedAngle);
	}
};

int QuantiserWeightedCuda::InPlaceQuantise(
	thrust::device_vector<thrust::complex<float>>& input
)
{
	// Quantise with discrete levels
	if (Levels != std::numeric_limits<int>::max())
	{
		if (ModulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationUnitVectors->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseDiscreteAmpWeighted(MinSLMValue, MaxSLMValue, Levels, Weighting));
		}
		else
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationPhases->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseDiscretePhaseWeighted(MinSLMValue, MaxSLMValue, Levels, Weighting));
		}
	}
	else
	{
		if (ModulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationUnitVectors->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseContAmpWeighted(MinSLMValue, MaxSLMValue, Weighting));
		}
		else
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationPhases->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseContPhaseWeighted(MinSLMValue, MaxSLMValue, Weighting));
		}
	}

	return 0;
}
