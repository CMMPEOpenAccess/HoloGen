// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <cufft.h>
#include "../Export/FFTScaleType.h"
#include "../Export/FFTDirectionCuda.h"
#include "../Export/LoggingCallback.h"
#include <thrust/complex.h>

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Cuda
			{
				using namespace HoloGen::Alg::Base::Cuda;

				class FFTHandlerCuda
				{
				public:

					static int PlanFFT2(
						cufftHandle& plan,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger
					);

					static int FFT2(
						cufftHandle& plan,
						const thrust::device_vector<thrust::complex<float>>& inputImage,
						thrust::device_vector<thrust::complex<float>>& outputImage,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger,
						const FFTDirectionCuda direction = FFTDirectionCuda::Forward,
						const FFTScaleType rescale = FFTScaleType::ScaleResult
					);

					static int FFT2WithShift(
						cufftHandle& plan,
						const thrust::device_vector<thrust::complex<float>>& inputImage,
						thrust::device_vector<thrust::complex<float>>& outputImage,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger,
						const FFTDirectionCuda direction = FFTDirectionCuda::Forward,
						const FFTScaleType rescale = FFTScaleType::ScaleResult
					);

					static void FFTShift(
						const thrust::device_vector<thrust::complex<float>>& inputImage,
						thrust::device_vector<thrust::complex<float>>& outputImage,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger
					);

					static void FFTShift(
						thrust::device_vector<thrust::complex<float>>& image,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger
					);

					static std::vector<std::complex<float>> FFT2(
						const std::vector<std::complex<float>>& image,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger,
						const FFTDirectionCuda direction = FFTDirectionCuda::Forward
					);

					static std::vector<std::complex<float>> FFT2WithShift(
						const std::vector<std::complex<float>>& image,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger,
						const FFTDirectionCuda direction = FFTDirectionCuda::Forward
					);

					static std::vector<std::complex<float>> FFTShift(
						const std::vector<std::complex<float>>& inputImage,
						const int slmResolutionX,
						const int slmResolutionY,
						LoggingCallback logger
					);

					static int CleanUp(
						cufftHandle plan
					);
				};
			}
		}
	}
}
