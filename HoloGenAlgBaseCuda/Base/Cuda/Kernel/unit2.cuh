// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once
#include "crt/host_defines.h"

#include <thrust/complex.h>

namespace HoloGen {
	namespace Alg
	{
		namespace Base
		{
			namespace Cuda
			{
				namespace Kernel
				{
					struct unit2
					{

						// Can't use thrust::arg<float> as it doesn't errror check
						// I'm not sure that it follows IEC 60559 either in VS141
						__device__ thrust::complex<float> operator() (const thrust::complex<float>& n)
						{

							// imag and real are inline funcs
							if (n.imag() == 0)
							{
								if (n.real() >= 0)
								{
									return thrust::complex<float>(1, 0);
								}
								else
								{
									return thrust::complex<float>(-1, 0);
								}
							}

							if (n.real() == 0)
							{
								if (n.imag() >= 0)
								{
									return thrust::complex<float>(0, 1);
								}
								else
								{
									return thrust::complex<float>(0, -1);
								}
							}

							const auto mag = std::sqrt(n.imag() * n.imag() + n.real() * n.real());

							return thrust::complex<float>(n.real() / mag, n.imag() / mag);
						}
					};
				}
			}
		}
	}
}


