// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <complex>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/complex.h>
#include "FFTUpdaterCuda.cuh"
#include "../Export/Globals.h"

using namespace HoloGen::Alg::Base::Cuda;

constexpr float PI = 3.14159265359;
constexpr float PI2 = 6.28318530718;

struct updateFFT
{
	const int SLMResolutionX;
	const int SLMResolutionY;
	const int PixelLocationX;
	const int PixelLocationY;
	const thrust::complex<float> PixelOldValue;
	const thrust::complex<float> PixelNewValue;
	const thrust::complex<float> CachedValue1;
	const float CachedValue2;
	const float CachedValue3;

	updateFFT(
		const int slmResolutionX,
		const int slmResolutionY,
		const int pixelLocationX,
		const int pixelLocationY,
		const thrust::complex<float> pixelOldValue,
		const thrust::complex<float> pixelNewValue) :
		SLMResolutionX(slmResolutionX),
		SLMResolutionY(slmResolutionY),
		PixelLocationX(pixelLocationX),
		PixelLocationY(pixelLocationY),
		PixelOldValue(pixelOldValue),
		PixelNewValue(pixelNewValue),
		CachedValue1((pixelNewValue - pixelOldValue) / std::sqrt(static_cast<float>(slmResolutionX * slmResolutionY))),
		CachedValue2(-static_cast<float>(pixelLocationX) / static_cast<float>(slmResolutionX)),
		CachedValue3(-static_cast<float>(pixelLocationY) / static_cast<float>(slmResolutionY))
	{
	};

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const unsigned int& index)
	{
		const int CurrentYLocation = index / SLMResolutionX;
		const int CurrentXLocation = index - CurrentYLocation * SLMResolutionX;
		
		return input + CachedValue1 * thrust::polar<float>(
			1, 
			CachedValue2 * static_cast<float>(CurrentXLocation) + CachedValue3 * static_cast<float>(CurrentYLocation));
	}
};

int FFTUpdaterCuda::UpdateFFT2(
	thrust::device_vector<thrust::complex<float>>& modifyImage,
	const int slmResolutionX,
	const int slmResolutionY,
	const int pixelLocationX,
	const int pixelLocationY,
	const thrust::complex<float>& pixelOldValue,
	const thrust::complex<float>& pixelNewValue,
	LoggingCallback logger)
{
	const thrust::counting_iterator<unsigned int> indexSequence(0);
	thrust::transform(
		modifyImage.begin(),
		modifyImage.end(),
		indexSequence,
		modifyImage.begin(),
		updateFFT(slmResolutionX, slmResolutionY, pixelLocationX, pixelLocationY, pixelOldValue, pixelNewValue)
	);

	return 0;
}
