// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include "../Export/SLMModulationSchemeCuda.h"
#include "../Export/LoggingCallback.h"
#include <thrust/complex.h>
#include <thrust/device_vector.h>

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Cuda
			{
				using namespace HoloGen::Alg::Base::Cuda;
				using namespace HoloGen::Alg::Base::Export;

				class QuantiserCuda
				{
				protected:

					thrust::device_vector<thrust::complex<float>>* Illumination{};
					thrust::device_vector<thrust::complex<float>>* IlluminationUnitVectors{};
					thrust::device_vector<float>* IlluminationMagnitudes{};
					thrust::device_vector<float>* IlluminationPhases{};

					LoggingCallback Logger = nullptr;

					QuantiserCuda();

				public:
					SLMModulationSchemeCuda ModulationScheme;
					float MaxSLMValue;
					float MinSLMValue;
					int Levels;

					virtual ~QuantiserCuda()
					{
					}

					void SetBaseParameters(
						const float maxSLMValue,
						const float minSLMValue,
						const int levels,
						const SLMModulationSchemeCuda modulationScheme)
					{
						this->MaxSLMValue = maxSLMValue;
						this->MinSLMValue = minSLMValue;
						this->Levels = levels;
						this->ModulationScheme = modulationScheme;
					}

					void __stdcall SetLogger(LoggingCallback cb);

					virtual int InPlaceQuantise(
						thrust::device_vector<thrust::complex<float>>& input
					) = 0;

					inline void SetIllumination(
						thrust::device_vector<thrust::complex<float>>* illumination,
						thrust::device_vector<thrust::complex<float>>* illuminationUnitVectors,
						thrust::device_vector<float>* illuminationMagnitudes,
						thrust::device_vector<float>* illuminationPhases)
					{
						Illumination = illumination;
						IlluminationUnitVectors = illuminationUnitVectors;
						IlluminationMagnitudes = illuminationMagnitudes;
						IlluminationPhases = illuminationPhases;
					}
				};
			}
		}
	}
}
