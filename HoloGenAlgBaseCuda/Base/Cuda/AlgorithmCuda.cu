// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once
#include <thrust/inner_product.h>
#include "AlgorithmCuda.cuh"
#include <vector>
#include <complex>
#include <thrust/complex.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/copy.h>
#include <vector>
#include "Randomiser.cuh"
#include "QuantiserCuda.cuh"
#include "FFTHandlerCuda.cuh"
#include <string>
#include "../Export/Globals.h"
#include <thrust/iterator/constant_iterator.h>
#include <thrust/functional.h>
#include "../Export/SLMModulationSchemeCuda.h"
#include "Kernel/abs2.cuh"
#include "Kernel/abssquare2.cuh"
#include "Kernel/square2.cuh"
#include "Kernel/arg2.cuh"
#include "Kernel/unit2.cuh"

struct inRegion
{
	__device__ bool operator()(const int& inRegion)
	{
		return inRegion > 0;
	}
};

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Cuda::Kernel;
using namespace HoloGen::Alg::Base::Export;

int AlgorithmCuda::RunBaseStartup()
{
	// Warning: Be careful about extracting bits of this into the parent class. 
	// The dynamic linking required by the Managed C++/Cuda C interface means that
	// whole program optimisation is limited over library boundaries. Extracting
	// operations like data copying may result in a reduction in efficiency.
	// For more info, we compile with -rdc=true and -dlink

	Globals::Log(Logger, "Beginning top-level setup...");

	// 
	// Handle the TARGET image
	//
	if (Target.empty())
	{
		Globals::Log(Logger, ">>>>> No target image! Cannot continue! <<<<<");
		return 1;
	}
	else
	{
		Globals::Log(Logger, "Copying in target image...");
	}

	if (RandomisePhase)
	{
		Globals::Log(Logger, "Randomising phase...");
		Randomiser::RandomisePhase(Target, Quantiser->MinSLMValue, Quantiser->MaxSLMValue);
	}

	// 
	// Handle the ILLUMINATION image
	//
	if (Illumination.empty())
	{
		Illumination = thrust::device_vector<thrust::complex<float>>(Target.size(), std::complex<float>(IlluminationPower, 0));
		DecomposeIllumination();
	}
	else
	{
		Globals::Log(Logger, "Copying in illumination image...");
	}
	if (Quantiser != nullptr)
	{
		Quantiser->SetIllumination(&Illumination, &IlluminationUnitVectors, &IlluminationMagnitudes, &IlluminationPhases);
	}

	// Rescale the target
	if (NormaliseTarget)
	{
		RescaleTarget();
	}

	// 
	// Handle the DIFFRACTION and REPLAY images
	//
	if (InitialSeedType == InitialSeedTypeCuda::File)
	{
		if (ReplayField.empty())
		{
			Globals::Log(Logger, ">>>>> No initial image! Cannot continue! <<<<<");
			return 1;
		}
		else
		{
			Globals::Log(Logger, "Copying in initial image...");
		}
	}
	else if (InitialSeedType == InitialSeedTypeCuda::BackProject)
	{
		Globals::Log(Logger, "Backprojecting target image...");
		const int result = BackProjectInProgressImage();
		if (result != 0) return result;
	}
	else if (InitialSeedType == InitialSeedTypeCuda::Random)
	{
		Globals::Log(Logger, "Randomising initial image...");
		if (Quantiser->ModulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			DiffractionField = thrust::device_vector<thrust::complex<float>>(SLMResolutionX * SLMResolutionY);
			Randomiser::RandomAmplitude(DiffractionField, Quantiser->MinSLMValue, Quantiser->MaxSLMValue);
		}
		else
		{
			DiffractionField = thrust::device_vector<thrust::complex<float>>(
				SLMResolutionX * SLMResolutionY, thrust::complex<float>(1.0, 0.0));
			Randomiser::RandomisePhase(DiffractionField, Quantiser->MinSLMValue, Quantiser->MaxSLMValue);
		}
		ReplayField = thrust::device_vector<thrust::complex<float>>(SLMResolutionX * SLMResolutionY);
	}
	else
	{
		Globals::Log(Logger, ">>>>> Unknown initial image source! <<<<<");
		return 1;
	}

	// Return success indicator
	return 0;
}

void AlgorithmCuda::RescaleTarget()
{
	// The power of an image is equal to the power of its FFT so we
	// can choose constant C so that:
	// C = average_pixel_value * SUM(illumination(x,y)) / SUM(target(x,y))

	Globals::Log(Logger, "Normalising target image...");
	float average_pixel_power = 0.0;
	if (Quantiser->ModulationScheme == SLMModulationSchemeCuda::Phase)
	{
		average_pixel_power = 1.0;
	}
	else
	{
		// Find average power assuming an even distribution of powers
		// TODO: Handle other power distributions
		if (Quantiser->Levels < 100)
		{
			const float section = (Quantiser->MaxSLMValue - Quantiser->MinSLMValue) / (Quantiser->Levels - 1);
			for (int i = 0; i < Quantiser->Levels; i++)
			{
				average_pixel_power = average_pixel_power + static_cast<float>(i * i) * section * section;
			}
			average_pixel_power = average_pixel_power / static_cast<float>(Quantiser->Levels);
		}
		else
		{
			average_pixel_power = (pow(Quantiser->MaxSLMValue,3) - pow(Quantiser->MinSLMValue,3)) / 3.0;
		}
	}

	const auto targetMagnitudeSquareSum = thrust::transform_reduce(
		TargetMagnitudes.begin(),
		TargetMagnitudes.end(),
		square2(),
		static_cast<float>(0.0),
		thrust::plus<float>());

	const auto illuminationMagnitudeSquareSum = thrust::transform_reduce(
		IlluminationMagnitudes.begin(),
		IlluminationMagnitudes.end(),
		square2(),
		static_cast<float>(0.0),
		thrust::plus<float>());

	const auto scale = std::sqrt(average_pixel_power * illuminationMagnitudeSquareSum / targetMagnitudeSquareSum);

	thrust::transform(
		Target.begin(),
		Target.end(),
		thrust::make_constant_iterator<float>(scale),
		Target.begin(),
		thrust::multiplies<thrust::complex<float>>());

	// Update the decomposition of the target
	DecomposeTarget();
}

int AlgorithmCuda::BackProjectInProgressImage()
{
	DiffractionField = thrust::device_vector<thrust::complex<float>>(SLMResolutionX * SLMResolutionY);
	ReplayField = thrust::device_vector<thrust::complex<float>>(SLMResolutionX * SLMResolutionY);

	// Create FFT
	cufftHandle plan;
	int result;

	// Plan the FFT operation
	result = FFTHandlerCuda::PlanFFT2(
		plan,
		SLMResolutionX,
		SLMResolutionY,
		Logger);
	if (result != 0)
	{
		Globals::Log(Logger, "FAILED initial image back projection!");
		return 1;
	}

	// Do the FFT
	result = FFTHandlerCuda::FFT2WithShift(
		plan,
		Target,
		DiffractionField,
		SLMResolutionX,
		SLMResolutionY,
		Logger,
		FFTDirectionCuda::Inverse,
		FFTScaleType::ScaleResult);
	if (result != 0)
	{
		Globals::Log(Logger, "FAILED initial image back projection!");
		return 1;
	}

	// Do the scaling
	thrust::device_vector<float> diffractionMagnitudes(DiffractionField.size(), 1.0);
	thrust::transform(
		DiffractionField.begin(),
		DiffractionField.end(),
		diffractionMagnitudes.begin(),
		abs2());
	const float diffractionMagnitudeSum = thrust::reduce(
		diffractionMagnitudes.begin(),
		diffractionMagnitudes.end(),
		static_cast<float>(0.0),
		thrust::plus<float>());
	const float diffractionMagnitudeMean = diffractionMagnitudeSum / static_cast<float>(DiffractionField.size());
	const float scaleFactor = static_cast<float>(
		(Quantiser->MaxSLMValue - Quantiser->MinSLMValue) /
		(diffractionMagnitudeMean * std::sqrt(2.0)));
	thrust::transform(
		DiffractionField.begin(),
		DiffractionField.end(),
		thrust::make_constant_iterator(scaleFactor),
		DiffractionField.begin(),
		thrust::multiplies<thrust::complex<float>>());

	// Clean Up
	result = FFTHandlerCuda::CleanUp(plan);
	if (result != 0)
	{
		Globals::Log(Logger, "FAILED initial image back projection!");
		return 1;
	}

	return 0;
}

void __stdcall AlgorithmCuda::SetLogger(LoggingCallback cb)
{
	Logger = cb;
}

// ReSharper disable once CppMemberFunctionMayBeConst
int AlgorithmCuda::RunBaseCleanup()
{
	Globals::Log(Logger, "Beginning top-level cleanup...");

	// Return success indicator
	return 0;
}

void AlgorithmCuda::SetTargetImage(const std::vector<std::complex<float>>& hostImage)
{
	// Can reinterpret because thrust and std complex have identical memory layouts
	const auto host_mod = reinterpret_cast<const std::vector<thrust::complex<float>>*>(&hostImage);
	Target = thrust::device_vector<thrust::complex<float>>(*host_mod);
	DecomposeTarget();
}

void AlgorithmCuda::DecomposeTarget()
{
	TargetUnitVectors = thrust::device_vector<thrust::complex<float>>(Target.size());
	TargetMagnitudes = thrust::device_vector<float>(Target.size());
	TargetPhases = thrust::device_vector<float>(Target.size());
	thrust::transform(
		Target.begin(),
		Target.end(),
		TargetUnitVectors.begin(),
		unit2());
	thrust::transform(
		Target.begin(),
		Target.end(),
		TargetMagnitudes.begin(),
		abs2());
	thrust::transform(
		Target.begin(),
		Target.end(),
		TargetPhases.begin(),
		arg2());

	// TODO: We shouldn't need to check this. The target image should be zero everywhere
	// except the region of interest. We don't enforce this currently though
	if (Region.size() != 0)
	{
		auto TargetMagnitudesInRegion = thrust::device_vector<float>(Target.size(), 0);
		thrust::copy_if(
			TargetMagnitudes.begin(),
			TargetMagnitudes.end(),
			Region.begin(),
			TargetMagnitudesInRegion.begin(),
			inRegion());
		TargetMagnitudeSumInRegion = thrust::reduce(
			TargetMagnitudesInRegion.begin(),
			TargetMagnitudesInRegion.end(),
			static_cast<float>(0.0),
			thrust::plus<float>());
	}
}

void AlgorithmCuda::SetRegionImage(const std::vector<int>& hostImage)
{
	Region = thrust::device_vector<int>(hostImage);

	NumRegionPixels = thrust::reduce(
		Region.begin(),
		Region.end(),
		0.0,
		thrust::plus<float>()
	);
	
	NumRegionPixelsReciprocal = 1 / static_cast<float>(NumRegionPixels);
	DecomposeTarget();
}

void AlgorithmCuda::SetIlluminationImage(const std::vector<std::complex<float>>& hostImage)
{
	// Can reinterpret because thrust and std complex have identical memory layouts
	const auto host_mod = reinterpret_cast<const std::vector<thrust::complex<float>>*>(&hostImage);
	Illumination = thrust::device_vector<thrust::complex<float>>(*host_mod);
	DecomposeIllumination();
	if (Quantiser != nullptr)
	{
		Quantiser->SetIllumination(&Illumination, &IlluminationUnitVectors, &IlluminationMagnitudes, &IlluminationPhases);
	}
}

void AlgorithmCuda::DecomposeIllumination()
{
	IlluminationUnitVectors = thrust::device_vector<thrust::complex<float>>(Illumination.size());
	IlluminationMagnitudes = thrust::device_vector<float>(Illumination.size());
	IlluminationPhases = thrust::device_vector<float>(Illumination.size());
	thrust::transform(
		Illumination.begin(),
		Illumination.end(),
		IlluminationUnitVectors.begin(),
		unit2());
	thrust::transform(
		Illumination.begin(),
		Illumination.end(),
		IlluminationMagnitudes.begin(),
		abs2());
	thrust::transform(
		Illumination.begin(),
		Illumination.end(),
		IlluminationPhases.begin(),
		arg2());
}

void AlgorithmCuda::SetInitialImage(const std::vector<std::complex<float>>& hostImage)
{
	// Can reinterpret because thrust and std complex have identical memory layouts
	const std::vector<thrust::complex<float>>* host_mod = reinterpret_cast<const std::vector<thrust::complex<float>>*>(&hostImage);
	ReplayField = thrust::device_vector<thrust::complex<float>>(*host_mod);
}

std::vector<std::complex<float>> AlgorithmCuda::GetResult()
{
	// Can reinterpret because thrust and std complex have identical memory layouts
	thrust::host_vector<thrust::complex<float>> host_mod = DiffractionField;
	std::vector<std::complex<float>> result(host_mod.size());
	thrust::copy(
		host_mod.begin(),
		host_mod.end(),
		result.begin());
	return result;
}

void AlgorithmCuda::SetQuantiser(QuantiserCuda* quantiser)
{
	Quantiser = quantiser;
	Quantiser->SetIllumination(&Illumination, &IlluminationUnitVectors, &IlluminationMagnitudes, &IlluminationPhases);
}

struct mseReduction
{
	const float ScaleFactor1;
	const float ScaleFactor2;

	mseReduction(const float scaleFactor1, const float scaleFactor2) :
		ScaleFactor1(scaleFactor1),
		ScaleFactor2(scaleFactor2)
	{

	}

	__device__ float operator()(
		const int& inRegion,
		const thrust::tuple<float, float>& values)
	{
		if (inRegion <= 0)
			return 0;
		const auto replayMagnitude = thrust::get<0>(values);
		const auto targetMagnitude = thrust::get<1>(values);
		const auto difference = replayMagnitude * ScaleFactor1 - targetMagnitude;
		return difference * difference * ScaleFactor2;
	}
};


float AlgorithmCuda::GetMSE()
{
	auto replayMagnitudesInRegion = thrust::device_vector<float>(ReplayField.size(), 0);
	thrust::transform_if(
		ReplayField.begin(),
		ReplayField.end(),
		Region.begin(),
		replayMagnitudesInRegion.begin(),
		abs2(),
		inRegion());

	const auto replayMagnitudeSumInRegion = thrust::reduce(
		replayMagnitudesInRegion.begin(),
		replayMagnitudesInRegion.end(),
		static_cast<float>(0.0),
		thrust::plus<float>());

	const float MSE = thrust::inner_product(
		Region.begin(),
		Region.end(),
		thrust::make_zip_iterator(
			thrust::make_tuple(
				replayMagnitudesInRegion.begin(),
				TargetMagnitudes.begin())),
		0.0,
		thrust::plus<float>(),
		mseReduction(TargetMagnitudeSumInRegion / replayMagnitudeSumInRegion, NumRegionPixelsReciprocal));

	return MSE;
}

std::map<MetricTypeCuda, float> AlgorithmCuda::GetStandardMetrics()
{
	Globals::Log(Logger, "Getting metrics...");

	std::map<MetricTypeCuda, float> metrics;
	

	metrics[MetricTypeCuda::MeanSquaredError] = GetMSE();
	metrics[MetricTypeCuda::Efficiency] = -1;
	return metrics;
}
