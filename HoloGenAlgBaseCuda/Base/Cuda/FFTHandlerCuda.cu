// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <complex>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/complex.h>
#include <thrust/device_ptr.h>
#include "FFTHandlerCuda.cuh"
#include "StridedChunkRange.cu"
#include <cuda_runtime.h>
#include <cufft.h>
#include <thrust/swap.h>
#include <thrust/functional.h>
#include <thrust/iterator/constant_iterator.h>
#include "../Export/Globals.h"

using namespace HoloGen::Alg::Base::Cuda;

// ReSharper disable IdentifierTypo

/// 
/// Plan a 2D Fourier Transform
/// Equivalent to fftshift(fft2(fftshift(input))) in Matlab
///
int FFTHandlerCuda::PlanFFT2(
	cufftHandle& plan,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger
)
{
	cufftResult result;
	if ((result = cufftPlan2d(&plan, slmResolutionY, slmResolutionX, CUFFT_C2C)) != CUFFT_SUCCESS)
	{
		Globals::Log(logger, "CUFFT error in FFTHandlerCuda: Plan creation failed");
		return result;
	}

	return 0;
}

///
/// Execute a 2d Fourier Transform 
/// Equivalent to fftshift(fft2(fftshift(input))) in Matlab
///
int FFTHandlerCuda::FFT2WithShift(
	cufftHandle& plan,
	const thrust::device_vector<thrust::complex<float>>& inputImage,
	thrust::device_vector<thrust::complex<float>>& outputImage,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger,
	const FFTDirectionCuda direction,
	const FFTScaleType rescale
)
{
	FFTShift(
		inputImage,
		outputImage,
		slmResolutionX,
		slmResolutionY,
		logger);

	const int result = FFT2(
		plan,
		outputImage,
		outputImage,
		slmResolutionX,
		slmResolutionY,
		logger,
		direction,
		rescale
	);

	FFTShift(
		outputImage,
		slmResolutionX,
		slmResolutionY,
		logger);

	return result;
}

///
/// Execute a 2d Fourier Transform 
/// Equivalent to fft2(input) in Matlab
///
int FFTHandlerCuda::FFT2(
	cufftHandle& plan,
	const thrust::device_vector<thrust::complex<float>>& inputImage,
	thrust::device_vector<thrust::complex<float>>& outputImage,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger,
	const FFTDirectionCuda direction,
	const FFTScaleType rescale
)
{
	// ReSharper disable CppLocalVariableMayBeConst
	auto inputData = const_cast<cufftComplex*>(reinterpret_cast<const cufftComplex*>(thrust::raw_pointer_cast(inputImage.data())));
	auto outputData = const_cast<cufftComplex*>(reinterpret_cast<const cufftComplex*>(thrust::raw_pointer_cast(outputImage.data())));
	// ReSharper restore CppLocalVariableMayBeConst

	const auto dir = direction == FFTDirectionCuda::Forward ? CUFFT_FORWARD : CUFFT_INVERSE;

	cufftResult result1;
	if ((result1 = cufftExecC2C(plan, inputData, outputData, dir)) != CUFFT_SUCCESS)
	{
		Globals::Log(logger, "CUFFT error in FFTHandlerCuda: ExecC2C Forward failed");
		return result1;
	}

	cudaError_t result2;
	if ((result2 = cudaDeviceSynchronize()) != cudaSuccess)
	{
		Globals::Log(logger, "Cuda error in FFTHandlerCuda: Failed to synchronize\n");
		return result2;
	}

	if (rescale == FFTScaleType::ScaleResult)
	{
		const int size = slmResolutionX * slmResolutionY;
		const float scale = 1.0 / std::sqrt(static_cast<float>(size));

		thrust::transform(
			outputImage.begin(),
			outputImage.end(),
			thrust::make_constant_iterator(scale),
			outputImage.begin(),
			thrust::multiplies<thrust::complex<float>>());
	}

	return 0;
}

// matlab fftshift works like this
// x = 
//  |  1  2  3  4  5 |
//  |  6  7  8  9 10 |
//  | 11 12 13 14 15 |
//  | 16 17 18 19 20 |
//  | 21 22 23 24 25 |
// fftshifft(x) = 
//  | 13  2  3  4  5 |
//  |  6  7  8  9 10 |
//  | 11 12  1  2  3 |
//  |  4  5  6  7  8 |
//  |  9 10 11 12 13 |
// This is great when either the X or Y resolution is even as you end up with cases like this
// X is even, Y is even:
//  | a a b b |
//  | a a b b |
//  | b b a a |
//  | b b a a |
// X is even, Y is bdd:
//  | a a b b |
//  | a a b b |
//  | a a a a |
//  | b b a a |
//  | b b a a |
// X is bdd, Y is even:
//  | a a a b b |
//  | a a a b b |
//  | b b a a a |
//  | b b a a a |
// X is bdd, Y is bdd:
//  | a a a 1 1 |    | b b 2 2 2 |
//  | a a a 1 1 |	 | b b 2 2 2 |
//  | a a a 1 1 | => | 1 1 a a a |
//  | 2 2 2 b b |	 | 1 1 a a a |
//  | 2 2 2 b b |	 | 1 1 a a a |
// The final case is the bne that causes prbblems as it can't be dbne in place easily.
// Instead we dbuble cbpy with sbme special cases.
//  | ? a a b b |
//  | a a a b b |
//  | a a ? a a |
//  | b b a a a |
//  | b b a a ? |
// That means for us we have
// fftshifft(x) = 
//  | 13 14 15 16 17 |
//  | 18 19 20 21 22 |
//  | 23 24  1  2  3 |
//  |  4  5  6  7  8 |
//  |  9 10 11 12 25 |
// This has the additional benefit of fftshifft(fftshifft()) being the identity, unlike in Matlab

// TODO: Benchmark this

// In-place version
void FFTHandlerCuda::FFTShift(
	thrust::device_vector<thrust::complex<float>>& image,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger
)
{
	const int blockBdimX = slmResolutionX / 2;
	const int blockBdimY = slmResolutionY / 2;
	const int blockAdimX = slmResolutionX - blockBdimX;
	const int blockAdimY = slmResolutionY - blockBdimY;
	const int blockAleng = blockAdimX + (blockAdimY - 1) * slmResolutionX;

	// Cache some values for our special case
	const thrust::complex<float> topLeftValue = image.front();
	const thrust::complex<float> centreValue = image.begin()[blockAleng];
	const thrust::complex<float> bottomRightValue = image.back();

	// Todo: Check that this is faster than the multiple kernel launches but no second reference tier approach
	// Todo: Possibly faster to not use thrust for this
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> tl(
		image.begin(),
		image.begin() + blockAleng,
		slmResolutionX,
		blockAdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> br(
		image.begin() + blockBdimX + blockBdimY * slmResolutionX,
		image.end(),
		slmResolutionX,
		blockAdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> tr(
		image.begin() + blockAdimX,
		image.begin() + blockBdimY * slmResolutionX,
		slmResolutionX,
		blockBdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> bl(
		image.begin() + blockAdimY * slmResolutionX,
		image.end(), // We don't need this as never used
		slmResolutionX,
		blockBdimX);
	thrust::swap_ranges(tl.begin(), tl.end(), br.begin());
	thrust::swap_ranges(tr.begin(), tr.end(), bl.begin());

	// Uncache our special case values
	if ((slmResolutionX % 2) & (slmResolutionY % 2))
	{
		image.front() = centreValue;
		image.begin()[blockAleng] = topLeftValue;
		image.back() = bottomRightValue;
	}
}

// ReSharper disable once CppParameterNeverUsed
// Out of place version
void FFTHandlerCuda::FFTShift(
	const thrust::device_vector<thrust::complex<float>>& inputImage,
	thrust::device_vector<thrust::complex<float>>& outputImage,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger
)
{
	auto& inputImage2 = const_cast<thrust::device_vector<thrust::complex<float>>&>(inputImage);

	const int blockBdimX = slmResolutionX / 2;
	const int blockBdimY = slmResolutionY / 2;
	const int blockAdimX = slmResolutionX - blockBdimX;
	const int blockAdimY = slmResolutionY - blockBdimY;;

	// Todo: Check that this is faster than the multiple kernel launches but no second reference tier approach
	// Todo: Possibly faster to not use thrust for this
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> tli(
		inputImage2.begin(),
		inputImage2.begin() + blockAdimX + (blockAdimY - 1) * slmResolutionX,
		slmResolutionX,
		blockAdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> bri(
		inputImage2.begin() + blockBdimX + blockBdimY * slmResolutionX,
		inputImage2.end(),
		slmResolutionX,
		blockAdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> tri(
		inputImage2.begin() + blockAdimX,
		inputImage2.begin() + blockBdimY * slmResolutionX,
		slmResolutionX,
		blockBdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> bli(
		inputImage2.begin() + blockAdimY * slmResolutionX,
		inputImage2.begin() + slmResolutionY * slmResolutionX - blockAdimX,
		slmResolutionX,
		blockBdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> tlo(
		outputImage.begin(),
		outputImage.end(), // We don't need this as never used 
		slmResolutionX,
		blockAdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> bro(
		outputImage.begin() + blockBdimX + blockBdimY * slmResolutionX,
		outputImage.end(), // We don't need this as never used 
		slmResolutionX,
		blockAdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> tro(
		outputImage.begin() + blockAdimX,
		outputImage.end(), // We don't need this as never used 
		slmResolutionX,
		blockBdimX);
	StridedChunkRange<thrust::device_vector<thrust::complex<float>>::iterator> blo(
		outputImage.begin() + blockAdimY * slmResolutionX,
		outputImage.end(), // We don't need this as never used 
		slmResolutionX,
		blockBdimX);
	thrust::copy(tli.begin(), tli.end(), bro.begin());
	thrust::copy(tri.begin(), tri.end(), blo.begin());
	thrust::copy(bli.begin(), bli.end(), tro.begin());
	thrust::copy(bri.begin(), bri.end(), tlo.begin());
}

int FFTHandlerCuda::CleanUp(
	cufftHandle plan
)
{
	cufftDestroy(plan);

	return 0;
}


///
/// Execute a 2d Fourier Transform 
/// Equivalent to fftshift(fft2(fftshift(input))) in Matlab
///
std::vector<std::complex<float>> FFTHandlerCuda::FFT2WithShift(
	const std::vector<std::complex<float>>& image,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger,
	const FFTDirectionCuda direction
)
{
	// ReSharper disable once CppLocalVariableMayBeConst
	auto moddedImage = reinterpret_cast<const std::vector<thrust::complex<float>>*>(&image);
	auto thrustImage = thrust::device_vector<thrust::complex<float>>(*moddedImage);

	cufftHandle plan;
	PlanFFT2(
		plan,
		slmResolutionX,
		slmResolutionY,
		logger);

	FFTShift(
		thrustImage,
		slmResolutionX,
		slmResolutionY,
		logger);

	FFT2(
		plan,
		thrustImage,
		thrustImage,
		slmResolutionX,
		slmResolutionY,
		logger,
		direction,
		FFTScaleType::ScaleResult);

	FFTShift(
		thrustImage,
		slmResolutionX,
		slmResolutionY,
		logger);

	CleanUp(plan);

	thrust::host_vector<thrust::complex<float>> host(thrustImage);
	std::vector<std::complex<float>> result(thrustImage.size());
	thrust::copy(host.begin(), host.end(), result.begin());

	return result;
}

///
/// Execute a 2d Fourier Transform 
/// Equivalent to fft2(input) in Matlab
///
std::vector<std::complex<float>> FFTHandlerCuda::FFT2(
	const std::vector<std::complex<float>>& image,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger,
	const FFTDirectionCuda direction
)
{
	// ReSharper disable once CppLocalVariableMayBeConst
	auto moddedImage = reinterpret_cast<const std::vector<thrust::complex<float>>*>(&image);
	auto thrustImage = thrust::device_vector<thrust::complex<float>>(*moddedImage);

	cufftHandle plan;
	PlanFFT2(
		plan,
		slmResolutionX,
		slmResolutionY,
		logger);

	FFT2(
		plan,
		thrustImage,
		thrustImage,
		slmResolutionX,
		slmResolutionY,
		logger,
		direction,
		FFTScaleType::ScaleResult
	);

	CleanUp(plan);

	thrust::host_vector<thrust::complex<float>> host(thrustImage);
	std::vector<std::complex<float>> result(thrustImage.size());
	thrust::copy(host.begin(), host.end(), result.begin());

	return result;
}

///
/// Execute a 2d Fourier Transform 
/// Equivalent to fftshift(fft2(fftshift(input))) in Matlab
///
std::vector<std::complex<float>> FFTHandlerCuda::FFTShift(
	const std::vector<std::complex<float>>& inputImage,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger
)
{
	const auto inputImage2 = reinterpret_cast<const std::vector<thrust::complex<float>>*>(&inputImage);
	const auto inputImage3 = thrust::device_vector<thrust::complex<float>>(*inputImage2);

	thrust::device_vector<thrust::complex<float>> outputImage(inputImage2->size());

	FFTShift(
		inputImage3,
		outputImage,
		slmResolutionX,
		slmResolutionY,
		logger);

	thrust::host_vector<thrust::complex<float>> host(outputImage);
	std::vector<std::complex<float>> result(outputImage.size());
	thrust::copy(host.begin(), host.end(), result.begin());

	return result;
}

// ReSharper restore IdentifierTypo
