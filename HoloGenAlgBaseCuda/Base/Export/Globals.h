// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <complex>
#include <string>
#include <chrono>
#include <ctime>
#include <iomanip>
#include "LoggingCallback.h"

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Cuda
			{
				/// Global level functions that don't deserve their own class
				class Globals
				{
				public:

					static std::string Timestamp()
					{
						using namespace std::chrono;

						// get current time
						auto now = system_clock::now();

						// get number of milliseconds for the current second
						// (remainder after division into seconds)
						auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;

						// convert to std::time_t in order to convert to std::tm (broken time)
						auto timer = system_clock::to_time_t(now);

						// convert to broken time
						// ReSharper disable once CppDeprecatedEntity
						std::tm bt = *std::localtime(&timer);

						std::ostringstream oss;

						oss << '(' << std::put_time(&bt, "%T"); // HH:MM:SS
						oss << '.' << std::setfill('0') << std::setw(3) << ms.count() << ')' << ' ';

						return oss.str();
					}

					__host__ __device__ static inline float Abs(const float real, const float imag)
					{
						// imag and real are inline funcs
						if (imag == 0)
						{
							return real;
						}
						else if (real == 0)
						{
							return imag;
						}
						else
						{
							return sqrtf(imag * imag + real * real);
						}
					}

					__host__ __device__ static inline float Arg(const float real, const float imag)
					{
						constexpr float PI = 3.14159265359;
						constexpr float PI2 = PI * 2;

						if (imag == 0)
						{
							if (real >= 0)
							{
								return 0;
							}
							else
							{
								return PI;
							}
						}
						else
						{
							const float result = atan2f(imag, real);
							return result > 0.0 ? result : result + PI2;
						}
					}

					static void Log(LoggingCallback loggingCallback, std::string message)
					{
						const char* tmsg = message.c_str();
						if (loggingCallback != nullptr)
						{
							loggingCallback(tmsg);
						}
					}


					// ReSharper disable once CppParameterMayBeConst
					static inline void DebugLog(int itr, LoggingCallback loggingCallback, const std::string name, float value)
					{
						//#ifdef DEBUG
						Log(loggingCallback, std::to_string(itr) + " >> " + name + ": " + std::to_string(value));
						//#endif // DEBUG
					}

					// ReSharper disable once CppParameterMayBeConst
					static inline void DebugLog(int itr, LoggingCallback loggingCallback, const std::string name, std::complex<float> value)
					{
						//#ifdef DEBUG
						Log(loggingCallback, std::to_string(itr) + " >> " + name + ": (" + std::to_string(value.real()) + ", " + std::to_string(value.imag()) + ")");
						//#endif // DEBUG
					}

					// ReSharper disable once CppParameterMayBeConst
					static inline void DebugLog(int itr, LoggingCallback loggingCallback, const std::string name, thrust::complex<float> value)
					{
						//#ifdef DEBUG
						Log(loggingCallback, std::to_string(itr) + " >> " + name + ": (" + std::to_string(value.real()) + ", " + std::to_string(value.imag()) + ")");
						//#endif // DEBUG
					}
				};
			}
		}
	}
}
