// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once
#include "AlgorithmWrap.h"
#include "QuantiserWrap.h"
#include "../Cuda/AlgorithmCuda.cuh"

using namespace HoloGen::Alg::Base::Cuda;

void __stdcall AlgorithmWrap::SetLogger(LoggingCallback cb)
{
	return Wrap->SetLogger(cb);
}

std::map<MetricTypeCuda, float> AlgorithmWrap::RunIterations()
{
	// Do nothing as we are wrapping the abstract interface
	std::map<MetricTypeCuda, float> metrics;
	return metrics;
}

void AlgorithmWrap::SetTargetImage(const std::vector<std::complex<float>>& hostImage)
{
	return Wrap->SetTargetImage(hostImage);
}

void AlgorithmWrap::SetRegionImage(const std::vector<int>& hostImage)
{
	return Wrap->SetRegionImage(hostImage);
}

void AlgorithmWrap::SetIlluminationImage(const std::vector<std::complex<float>>& hostImage)
{
	return Wrap->SetIlluminationImage(hostImage);
}

void AlgorithmWrap::SetInitialImage(const std::vector<std::complex<float>>& hostImage)
{
	// Can reinterpret because thrust and std complex have identical memo
	return Wrap->SetInitialImage(hostImage);
}

std::vector<std::complex<float>> AlgorithmWrap::GetResult()
{
	return Wrap->GetResult();
}

void AlgorithmWrap::SetQuantiser(QuantiserWrap* quantiser)
{
	return Wrap->SetQuantiser(quantiser->GetNativePointer());
}

void AlgorithmWrap::SetBaseParameters(
	const size_t slmResolutionX,
	const size_t slmResolutionY,
	const int maxIterations,
	const float maxError,
	const int reportingSteps,
	const bool randomisePhase,
	const bool normaliseTarget,
	const bool tieNormalisationToZero,
	const IlluminationTypeCuda illuminationType,
	const InitialSeedTypeCuda initialSeedType,
	const float illuminationPower)
{
	Wrap->SetBaseParameters(
		slmResolutionX,
		slmResolutionY,
		maxIterations,
		maxError,
		reportingSteps,
		randomisePhase,
		normaliseTarget,
		tieNormalisationToZero,
		illuminationType,
		initialSeedType,
		illuminationPower);
}

HoloGen::Alg::Base::Export::AlgorithmWrap::~AlgorithmWrap()
{
	delete Wrap;
}
