// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <complex>
#include <map>
#include "../../Base/Export/AlgorithmWrap.h"

using namespace HoloGen::Alg::Base::Cuda;

namespace HoloGen
{
	namespace Alg
	{
		namespace GS
		{
			namespace Cuda
			{
				class GSAlgorithmStandardCuda;
			}

			namespace Export
			{
				using namespace HoloGen::Alg::Base::Cuda;
				using namespace HoloGen::Alg::Base::Export;
				using namespace HoloGen::Alg::GS::Cuda;
				using namespace HoloGen::Alg::GS::Export;

				class GSAlgorithmStandardWrap : public AlgorithmWrap
				{
				private:

					GSAlgorithmStandardCuda* Wrap; // Owned by base class. Do not cleanup

				protected:

					GSAlgorithmStandardWrap(GSAlgorithmStandardCuda* wrap) :
						AlgorithmWrap(reinterpret_cast<AlgorithmCuda*>(wrap)),
						Wrap(wrap)
					{
					}

				public:

					GSAlgorithmStandardWrap();

					virtual ~GSAlgorithmStandardWrap()
					{
						// "wrap" is deleted in the base class
					}

					void SetAlgorithmParameters(
						const int dummyValue);

					int RunStartup() override;

					std::map<MetricTypeCuda, float> RunIterations() override;

					int RunCleanup() override;
				};
			}
		}
	}
}
