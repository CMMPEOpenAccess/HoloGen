﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using Newtonsoft.Json;
using System;

namespace HoloGen.Serial
{
    /// <summary>
    /// JSON format deserialisation class
    /// </summary>
    public class JsonDeserialiser<S, T> : Deserialiser<S, T>
        where S : HierarchySaveable, new()
        where T : HierarchyVersion, new()
    {
        public override Tuple<DeserialiseResult, S> Deserialise(string fileContents)
        {
            try
            {
                var rawDeserial = JsonConvert.DeserializeObject<Tuple<T, S>>(fileContents);

                var fileVersion = rawDeserial.Item1;
                var options = rawDeserial.Item2;

                if (fileVersion == null || options == null) return new Tuple<DeserialiseResult, S>(DeserialiseResult.BadFormat, null);

                var currentVersion = new T();

                if (fileVersion.CurrentVersion > currentVersion.CurrentVersion)
                    return new Tuple<DeserialiseResult, S>(DeserialiseResult.FileFromLaterVersion, null);
                if (fileVersion.CurrentVersion < currentVersion.MinimumCompatibleVersion)
                    return new Tuple<DeserialiseResult, S>(DeserialiseResult.FileFromEarlierVersion, null);
                return new Tuple<DeserialiseResult, S>(DeserialiseResult.Success, options);
            }
            catch (Exception)
            {
                return new Tuple<DeserialiseResult, S>(DeserialiseResult.BadFormat, null);
            }
        }
    }
}