﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;

namespace HoloGen.Hierarchy.OptionTypes
{
    /// <summary>
    /// Extends <see cref="Option" /> wrapping a numeric parameter.
    /// Implements the <see cref="IHasWatermark" /> interface in addition.
    /// <see cref="MaxValue" /> and <see cref="MinValue" /> are the boundaries used to define the validity of this option.
    /// </summary>
    public abstract class NumericOption<T> : Option<T>, IHasWatermark
        where T : struct,
        IComparable,
        IComparable<T>,
        IConvertible,
        IEquatable<T> // Attempt to limit it to numbers or similar
    {
        protected NumericOption()
        {
        }

        protected NumericOption(T value) : base(value)
        {
        }

        [JsonIgnore]
        public abstract T MaxValue { get; }

        [JsonIgnore]
        public abstract T MinValue { get; }

        [JsonIgnore]
        public override bool Valid => (Value.CompareTo(MinValue) >= 0) & (Value.CompareTo(MaxValue) <= 0);

        [JsonIgnore]
        public override StringCollection Error
        {
            get
            {
                var collection = new StringCollection();
                if (!((Value.CompareTo(MinValue) >= 0) & (Value.CompareTo(MaxValue) <= 0)))
                    collection.Add(string.Format(Resources.Properties.Resources.NumericOption_Error_ValueMustBeGreaterThan0AndLessThat1, MinValue, MaxValue));
                return collection;
            }
        }

        public abstract string Watermark { get; }
    }
}