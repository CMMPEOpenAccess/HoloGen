﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace HoloGen.Hierarchy.OptionTypes
{
    /// <summary>
    /// Extends <see cref="BooleanOption" /> wrapping a boolean parameter.
    /// Due to the limits of the C# reflection interface, <see cref="SelectOption" /> defines
    /// <see cref="SetPushLocation(HierarchyFolder)" /> which must be called in the owner's constructor.
    /// <see cref="SelectOption" /> listens to changes in the selected <see cref="Possibility" /> and pushes the
    /// <see cref="ReflectiveChildrenElement{T}.Children" />
    /// of the <see cref="Possibility" /> into the <see cref="HierarchyFolder" /> owner.
    /// </summary>
    public abstract class BooleanOptionWithChildren : BooleanOption
    {
        [JsonIgnore] public HierarchyFolder PushLocation;

        protected BooleanOptionWithChildren()
        {
            PropertyChanged += BooleanOptionWithChildren_PropertyChanged;

            var type = typeof(IOption);

            foreach (var prop in GetType().GetProperties())
                if (type.IsAssignableFrom(prop.PropertyType) &&
                    prop.Name != "Reference")
                {
                    var child = (IOption) prop.GetValue(this);
                    child.Enabled = false;
                    Children.Add(child);
                    child.BindingName = prop.Name;
                }
        }

        [JsonIgnore]
        public ObservableCollection<IOption> Children { get; } = new ObservableCollection<IOption>();

        private void BooleanOptionWithChildren_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                if (Value)
                    foreach (var child in Children)
                    {
                        child.Enabled = true;
                        PushLocation.Children.Add(child);
                    }
                else
                    foreach (var child in Children)
                    {
                        child.Enabled = false;
                        PushLocation.Children.Remove(child);
                    }
            }
        }

        public override void Reset()
        {
            foreach (var child in Children) child.Reset();
        }

        /// <summary>
        /// C# is a little stricter than some on initialisation order. We have to initiate
        /// SelectOption in the class definition otherwise the static behaviour of WPF fails.
        /// If we do that, though, then we can't pass in the reference (PushLocation) from our
        /// superconstructor because 'this' (e.g. AlgorithmsFolder) does not exist yet at that
        /// point. If this was Java or C++ we would be fine. Yeah, this is hacky. Get over it.
        /// </summary>
        /// <param name="pushlocation"></param>
        public void SetPushLocation(HierarchyFolder pushlocation)
        {
            PushLocation = pushlocation;
            foreach (var child in Children)
            {
                child.Enabled = true;
                PushLocation.Children.Add(child);
            }
        }

        public override List<IOption> Flatten()
        {
            var flattened = new List<IOption> {this};
            foreach (var child in Children) flattened = flattened.Concat(child.Flatten()).ToList();
            return flattened;
        }

        public override void SetBindingPathRecursively(string baseBindingPath)
        {
            if (string.IsNullOrEmpty(BindingPath))
                BindingPath = baseBindingPath == "" ? BindingName : BindingName == "" ? baseBindingPath : baseBindingPath + "." + BindingName;
            foreach (var child in Children) child.SetBindingPathRecursively(BindingPath);
        }

        public override void SetLongNameRecursively(string baseLongName)
        {
            LongName = baseLongName != "" ? baseLongName + " >> " + Name : Name;
            foreach (var child in Children) child.SetLongNameRecursively(LongName);
        }
    }
}