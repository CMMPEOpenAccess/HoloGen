﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using HoloGen.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using HoloGen.Utils.ExtensionMethods;

namespace HoloGen.Hierarchy.OptionTypes
{
    /// <summary>
    /// Root hierarchy element. Defines <see cref="Name" /> and <see cref="ToolTip" /> fields that must be implemented by
    /// any implementors.
    /// Also defines the <see cref="Valid" /> and <see cref="Error" /> properties which return a boolean validity and error
    /// message <see cref="StringCollection" /> respectively.
    /// <see cref="Enabled" /> defaults to true and can be overriden by any extending classes.
    /// </summary>
    /// <typeparam name="T">Type of object that this wraps. e.g. <see cref="string" /></typeparam>
    [Serializable]
    public abstract class Option<T> : ObservableObject, IOption
    {
        private bool _enabled = true;

        [JsonIgnore] private Option<T> _reference;

        private string _searchTerm;

        private T _value;

        protected Option()
        {
            // ReSharper disable VirtualMemberCallInConstructor
            Value = Default;
            // ReSharper restore VirtualMemberCallInConstructor
        }

        protected Option(T value)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            Value = value;
            // ReSharper restore VirtualMemberCallInConstructor
        }

        [JsonIgnore]
        public virtual Option<T> Reference
        {
            get => _reference;
            set
            {
                if (GetType() != value.GetType())
                    throw new ArgumentException("Reference must be of the same type as the original object");

                if (_reference != null) _reference.ValueChanged -= Reference_ValueChanged;
                SetProperty(ref _reference, value);
                if (_reference != null) _reference.ValueChanged += Reference_ValueChanged;
            }
        }

        [JsonIgnore]
        public virtual bool ValueEqualToReference => Reference == null || CompareValues(Reference.Value);

        [JsonIgnore]
        public abstract T Default { get; }

        public virtual T Value
        {
            get => _value;
            set
            {
                var old = _value;
                SetProperty(ref _value, value);
                OnPropertyChanged(nameof(Valid));
                OnPropertyChanged(nameof(ValueEqualToReference));
                NotifyChanged();
                AddOtherChangeListeners();
                InvokeValueChanged(old);
            }
        }

        [JsonIgnore]
        public abstract string Name { get; }

        [JsonIgnore]
        public string BindingName { get; set; } = "";

        [JsonIgnore]
        public string BindingPath { get; set; } = "";

        [JsonIgnore]
        public string LongName { get; set; } = "";

        [JsonIgnore]
        public abstract string ToolTip { get; }

        [JsonIgnore]
        public virtual bool Valid => true;

        [JsonIgnore]
        public virtual StringCollection Error { get; } = new StringCollection();

        [JsonIgnore]
        public virtual bool Enabled
        {
            get => _enabled;
            set => SetProperty(ref _enabled, value);
        }

        [JsonIgnore]
        public string SearchTerm
        {
            get => _searchTerm;
            set
            {
                HasResult = string.IsNullOrEmpty(value) || HasSearchTerm(value);
                if (_searchTerm != value) SetProperty(ref _searchTerm, value);
            }
        }

        public event EventHandler Changed;

        [JsonIgnore]
        public bool HasResult { get; private set; } = true;

        public virtual void Reset()
        {
            if (!Value.Equals(Default)) Value = Default;
        }

        public virtual List<IOption> Flatten()
        {
            var flattened = new List<IOption> {this};
            return flattened;
        }

        public virtual void SetBindingPathRecursively(string baseBindingPath)
        {
            if (string.IsNullOrEmpty(BindingPath))
                BindingPath = baseBindingPath == "" ? BindingName : BindingName == "" ? baseBindingPath : baseBindingPath + "." + BindingName;
        }

        public virtual void SetLongNameRecursively(string baseLongName)
        {
            LongName = baseLongName != "" ? baseLongName + " >> " + Name : Name;
        }

        public abstract string ConvertToString();
        public abstract void ConvertFromString(string input);

        public override string ToString()
        {
            return Value.ToString();
        }

        protected virtual void Reference_ValueChanged(object sender, T oldValue)
        {
            if (CompareValues(oldValue))
            {
                OnPropertyChanged(nameof(ValueEqualToReference));
                Value = (T) Reference.Value.DeepCopy();
            }
        }

        public virtual bool CompareValues(T otherValue)
        {
            return EqualityComparer<T>.Default.Equals(otherValue, Value);
        }

        public event EventHandler<T> ValueChanged;

        protected void InvokeValueChanged(T oldValue)
        {
            ValueChanged?.Invoke(this, oldValue);
        }

        protected virtual void AddOtherChangeListeners()
        {
            // Do Nothing
        }

        protected void NotifyChanged()
        {
            Changed?.Invoke(this, null);
        }

        private bool HasSearchTerm(string val)
        {
            return
                CultureInfo.CurrentCulture.CompareInfo.IndexOf(Name, val, CompareOptions.IgnoreCase) >= 0 &&
                CultureInfo.CurrentCulture.CompareInfo.IndexOf(ToolTip, val, CompareOptions.IgnoreCase) >= 0;
        }
    }
}