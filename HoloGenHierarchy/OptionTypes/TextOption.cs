﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace HoloGen.Hierarchy.OptionTypes
{
    /// <summary>
    /// Extends <see cref="Option" /> wrapping a boolean parameter.
    /// Implements the <see cref="IHasWatermark" /> interface in addition.
    /// <see cref="CanClear" /> defines whether this a text property that can be cleared, e.g. a description, or one that
    /// can't, e.g. a file path.
    /// </summary>
    public abstract class TextOption : Option<string>, IHasWatermark
    {
        private bool _canClear = true;

        protected TextOption()
        {
        }

        protected TextOption(string value) : base(value)
        {
        }

        [JsonIgnore]
        public override bool Valid => Error.Count == 0;

        [JsonIgnore]
        public override StringCollection Error { get; } = new StringCollection();

        public virtual bool CanClear
        {
            get => _canClear;
            set => SetProperty(ref _canClear, value);
        }

        public abstract string Watermark { get; }

        public override string ConvertToString()
        {
            return Value;
        }

        public override void ConvertFromString(string input)
        {
            Value = input;
        }
    }
}