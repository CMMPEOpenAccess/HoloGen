﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using Newtonsoft.Json;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Root hierarchy element. Extends <see cref="HierarchyElement" />.
    /// The base class <see cref="ReflectiveChildrenElement" /> finds all members assignable from <typeparamref name="T" />
    /// and
    /// uses them to populate the <see cref="ReflectiveChildrenElement.Children" /> member collection.
    /// </summary>
    /// <typeparam name="T">
    /// Class/Interface that has <see cref="object.GetType()" /> called on it to get the type used to
    /// populate <see cref="Children" />
    /// </typeparam>
    public abstract class HierarchyRoot : HierarchyElement<HierarchyPage>
    {
        private int _id;

        protected HierarchyRoot()
        {
            // ReSharper disable VirtualMemberCallInConstructor
            SetBindingPathRecursively("");
            SetLongNameRecursively("");
            // ReSharper restore VirtualMemberCallInConstructor
        }

        [JsonIgnore]
        public int ID
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public override void SetLongNameRecursively(string baseLongName)
        {
            LongName = baseLongName;
            foreach (var child in Children) child.SetLongNameRecursively(LongName);
        }

        public HierarchyRoot Clone()
        {
            var fileContents = "";
            fileContents += JsonConvert.SerializeObject(this, Formatting.Indented);
            var rawDeserial = JsonConvert.DeserializeObject(fileContents, GetType());
            return (HierarchyRoot) rawDeserial;
        }
    }
}