﻿using Newtonsoft.Json;
using System.Collections.Specialized;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Gets the this child's parent or parents
    /// </summary>
    public interface IHasParent<T>
    {
        [JsonIgnore]
        T Parent { get; set; }
    }
}
