﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Gets the this child's parent or parents
    /// </summary>
    public interface IHasParents<T> : IHasParent<T>
    {
        [JsonIgnore]
        IList<T> Parents { get; }
    }
}
