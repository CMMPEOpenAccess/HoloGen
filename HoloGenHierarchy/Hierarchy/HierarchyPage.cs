﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using Newtonsoft.Json;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Page hierarchy element. Extends <see cref="HierarchyElement" />.
    /// The base class <see cref="ReflectiveChildrenElement" /> finds all members assignable from <typeparamref name="T" />
    /// and
    /// uses them to populate the <see cref="ReflectiveChildrenElement.Children" /> member collection.
    /// </summary>
    /// <typeparam name="T">
    /// Class/Interface that has <see cref="object.GetType()" /> called on it to get the type used to
    /// populate <see cref="Children" />
    /// </typeparam>
    public abstract class HierarchyPage : HierarchyElement<HierarchyFolder>
    {
        [JsonIgnore]
        public abstract object Icon { get; }
    }
}