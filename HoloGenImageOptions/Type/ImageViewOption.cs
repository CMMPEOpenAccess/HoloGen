﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System.Linq;
using HoloGen.Hierarchy.OptionTypes;
using Newtonsoft.Json;

namespace HoloGen.Image.Options.Type
{
    /// <summary>
    /// Selection option for image view style.
    /// </summary>
    public sealed class ImageViewOption : SelectOption<ImageViewPossibility>
    {
        public ImageViewPossibilities Possibilities2 = new ImageViewPossibilities();

        [JsonIgnore]
        public override string Name => Resources.Properties.Resources.ImageViewOption_VisualisationType;

        [JsonIgnore]
        public override string ToolTip => Resources.Properties.Resources.ImageViewOption_WaysToVisualiseThisImage;

        [JsonIgnore]
        public override PossibilityCollection<ImageViewPossibility> Possibilities => Possibilities2;

        public override ImageViewPossibility Default => Possibilities.Children.First();
    }
}