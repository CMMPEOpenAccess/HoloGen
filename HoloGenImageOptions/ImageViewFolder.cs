﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Image.Options.Type;

namespace HoloGen.Image.Options
{
    /// <summary>
    /// Folder of options relating to the image view style.
    /// </summary>
    public sealed class ImageViewFolder : HierarchyFolder
    {
        public ImageViewFolder()
        {
            DimensionOption.SetPushLocation(this);
            ImageViewOption.SetPushLocation(this);
            TransformTypeOption.SetPushLocation(this);
            ColorSchemeOption.SetPushLocation(this);
            ScaleLocationOption.SetPushLocation(this);
        }

        public DimensionOption DimensionOption { get; } = new DimensionOption();
        public ImageViewOption ImageViewOption { get; } = new ImageViewOption();
        public TransformTypeOption TransformTypeOption { get; } = new TransformTypeOption();
        public ColorSchemeOption ColorSchemeOption { get; } = new ColorSchemeOption();
        public ScaleLocationOption ScaleLocationOption { get; } = new ScaleLocationOption();

        public override string Name => Resources.Properties.Resources.ImageViewFolder_ImageVisualisation;

        public override string ToolTip => Resources.Properties.Resources.ImageViewFolder_SLMImageViewProfile;
    }
}