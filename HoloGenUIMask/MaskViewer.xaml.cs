﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Image;
using HoloGen.Mask.Options.ShapeType;
using HoloGen.UI.Mask.Drawer;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using NHotkey;
using NHotkey.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace HoloGen.UI.Mask
{
    /// <summary>
    /// User interface control handling drawing shapes on a masked image control as well as
    /// handling selection and deletion.
    /// </summary>
    public partial class MaskViewer : UserControl
    {
        private readonly Dictionary<Type, Drawer.Drawer> _drawers = new Dictionary<Type, Drawer.Drawer>();

        private readonly HotKey _hotKey = new HotKey(Key.Delete);

        private readonly StatusMessage _statusMessage = new StatusMessage();
        private Drawer.Drawer _drawer;
        private Drawer.Drawer _ellipseDrawer;
        private Drawer.Drawer _pointDrawer;
        private Drawer.Drawer _polyDrawer;
        private Drawer.Drawer _rectDrawer;

        public MaskViewModel ViewModel;

        public MaskViewer()
        {
            InitializeComponent();
        }

        private void MaskViewer_Loaded(object sender, RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                var model = DataContext as MaskViewModel;
                if ((DataContext != null) & (ViewModel == null))
                {
                    ViewModel = model;

                    SetDrawMode();
                }
            }

            if (_drawers.Keys.Count == 0)
            {
                _drawers.Add(typeof(ShapePolygon), _polyDrawer = new PolygonDrawer(DrawingCanvas, ViewModel));
                _drawers.Add(typeof(ShapePoint), _pointDrawer = new PointDrawer(DrawingCanvas, ViewModel));
                _drawers.Add(typeof(ShapeEllipse), _ellipseDrawer = new EllipseDrawer(DrawingCanvas, ViewModel));
                _drawers.Add(typeof(ShapeRectangle), _rectDrawer = new RectDrawer(DrawingCanvas, ViewModel));
                _drawer = _rectDrawer;
            }

            LoadSavedShapes();
        }

        private void LoadSavedShapes()
        {
            foreach (var tuple in ViewModel.SaveableShapeCollection)
            {
                var shape = tuple.Item1;
                var points = tuple.Item2;

                if (shape is ShapeType.Rectangle)
                {
                    var p1 = points[0];
                    var p2 = points[1];
                    var body = new Rectangle
                    {
                        Fill = new SolidColorBrush(ThemeColors.HighlightColor),
                        Stroke = new SolidColorBrush(ThemeColors.AccentColor),
                        StrokeThickness = 2,
                        Opacity = 0.6
                    };
                    body.SetValue(Canvas.LeftProperty, Math.Min(p1.X, p2.X));
                    body.SetValue(Canvas.TopProperty, Math.Min(p1.Y, p2.Y));
                    body.Width = Math.Abs(p1.X - p2.X);
                    body.Height = Math.Abs(p1.Y - p2.Y);

                    ViewModel.DisplayableShapeCollection.Add(body);
                    DrawingCanvas.Children.Add(body);
                }
                else if (shape is ShapeType.Ellipse)
                {
                    var p1 = points[0];
                    var p2 = points[1];
                    var body = new Ellipse
                    {
                        Fill = new SolidColorBrush(ThemeColors.HighlightColor),
                        Stroke = new SolidColorBrush(ThemeColors.AccentColor),
                        StrokeThickness = 2,
                        Opacity = 0.6
                    };
                    body.SetValue(Canvas.LeftProperty, Math.Min(p1.X, p2.X));
                    body.SetValue(Canvas.TopProperty, Math.Min(p1.Y, p2.Y));
                    body.Width = Math.Abs(p1.X - p2.X);
                    body.Height = Math.Abs(p1.Y - p2.Y);

                    ViewModel.DisplayableShapeCollection.Add(body);
                    DrawingCanvas.Children.Add(body);
                }
                else if (shape is ShapeType.Polygon)
                {
                    var body = new Polygon
                    {
                        Fill = new SolidColorBrush(ThemeColors.HighlightColor),
                        Stroke = new SolidColorBrush(ThemeColors.AccentColor),
                        StrokeThickness = 2,
                        Opacity = 0.6
                    };
                    foreach (var point in points)
                    {
                        body.Points.Add(point);
                    }

                    ViewModel.DisplayableShapeCollection.Add(body);
                    DrawingCanvas.Children.Add(body);
                }
                else if (shape is ShapeType.Point)
                {
                    var p = points[0];
                    var body = new Rectangle
                    {
                        Fill = new SolidColorBrush(ThemeColors.AccentColor),
                        Width = 6,
                        Height = 6
                    };

                    body.SetValue(Canvas.LeftProperty, p.X - 3);
                    body.SetValue(Canvas.TopProperty, p.Y - 3);

                    ViewModel.DisplayableShapeCollection.Add(body);
                    DrawingCanvas.Children.Add(body);
                }
            }
        }

        private void DrawingCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            var p = e.GetPosition(DrawingCanvas);
            _statusMessage.Message = string.Format(HoloGen.Resources.Properties.Resources.MaskViewer_DrawingCanvas_MouseMove_Mouse0F0, p);

            _drawer?.MouseMove(e.GetPosition(DrawingCanvas));
        }

        private void DrawingCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _drawer?.MouseLeftButtonDown(
                e.GetPosition(DrawingCanvas),
                e.ChangedButton == MouseButton.Left && e.ClickCount == 2);
        }

        private void Item_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var shape = (Shape) sender;

            foreach (var item in ViewModel.DisplayableShapeCollection)
                if (Equals(item, shape))
                    item.Fill = new SolidColorBrush(Colors.Red);
        }

        public void SetPointMode()
        {
            ViewModel.OperationMode = OperationMode.Point;
            DrawingCanvas.Cursor = Cursors.Hand;
            foreach (var item in ViewModel.DisplayableShapeCollection)
            {
                item.Cursor = Cursors.Arrow;
                item.MouseLeftButtonDown += Item_MouseLeftButtonDown;
            }
        }

        public void SetDrawMode()
        {
            ViewModel.OperationMode = OperationMode.Drawing;
            DrawingCanvas.Cursor = Cursors.Cross;
            foreach (var item in ViewModel.DisplayableShapeCollection) item.Cursor = Cursors.Cross;
        }

        public void SetShape(ShapePossibility shape)
        {
            _drawer = _drawers.ContainsKey(shape.GetType()) ? _drawers[shape.GetType()] : _rectDrawer;
        }

        private void OnHotKey(object sender, HotkeyEventArgs e)
        {
            if (ViewModel.DisplayableShapeCollection.Count > 0)
            {
                DrawingCanvas.Children.Remove(ViewModel.DisplayableShapeCollection.Last());
                ViewModel.DisplayableShapeCollection.Remove(ViewModel.DisplayableShapeCollection.Last());
                ViewModel.FireShapeCollectionChanged();
            }

            e.Handled = true;
        }

        private void drawingCanvas_MouseEnter(object sender, MouseEventArgs e)
        {
            StatusMessageFramework.Messages.Add(_statusMessage);
            HotkeyManager.Current.AddOrReplace("delete", _hotKey.Key, _hotKey.ModifierKeys, OnHotKey);
        }

        private void drawingCanvas_MouseLeave(object sender, MouseEventArgs e)
        {
            StatusMessageFramework.Messages.Remove(_statusMessage);
            HotkeyManager.Current.Remove("delete");
        }
    }
}