﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Image;
using HoloGen.Utils;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace HoloGen.UI.Mask.Drawer
{
    /// <summary>
    /// <see cref="Drawer" /> for ellipse shapes.
    /// </summary>
    public class EllipseDrawer : Drawer
    {
        private readonly StatusMessage _statusMessage = new StatusMessage();
        private Ellipse _body;

        private bool _isEllipseInDrawing;
        private Point _p1 = new Point(0, 0);
        private Point _p2 = new Point(0, 0);

        public EllipseDrawer(Canvas canvas, MaskViewModel viewModel) : base(canvas, viewModel)
        {
            BindListBox();
        }

        public void Draw()
        {
        }

        public void BindListBox()
        {
        }

        public override void MouseMove(Point p)
        {
            if (ViewModel.OperationMode == OperationMode.Drawing)
                if (_isEllipseInDrawing)
                {
                    _p2 = p;

                    _body.SetValue(Canvas.LeftProperty, Math.Min(_p1.X, _p2.X));
                    _body.SetValue(Canvas.TopProperty, Math.Min(_p1.Y, _p2.Y));
                    _body.Width = Math.Abs(_p1.X - _p2.X);
                    _body.Height = Math.Abs(_p1.Y - _p2.Y);
                }

            _statusMessage.Message = string.Format
            (Resources.Properties.Resources.EllipseDrawer_MouseMove_Point,
                _p1.X,
                _p1.Y,
                _p2.X,
                _p2.Y);
        }

        public override void MouseLeftButtonDown(Point p, bool doubleClick)
        {
            if (ViewModel.OperationMode == OperationMode.Drawing)
            {
                if (_isEllipseInDrawing == false)
                {
                    _body = new Ellipse
                    {
                        Fill = new SolidColorBrush(ThemeColors.HighlightColor),
                        Stroke = new SolidColorBrush(ThemeColors.AccentColor),
                        StrokeThickness = 2,
                        Opacity = 0.6
                    };
                    Canvas.Children.Add(_body);

                    _p1 = p;
                    _isEllipseInDrawing = true;

                    StatusMessageFramework.Messages.Add(_statusMessage);

                    return;
                }

                if (_isEllipseInDrawing)
                {
                    _p2 = p;
                    _isEllipseInDrawing = false;


                    ViewModel.DisplayableShapeCollection.Add(_body);
                    ViewModel.FireShapeCollectionChanged();

                    var points = new List<Point>
                    {
                        _p1,
                        _p2
                    };
                    ViewModel.SaveableShapeCollection.Add(new Tuple<ShapeType, List<Point>>(ShapeType.Ellipse, points));

                    BindListBox();

                    StatusMessageFramework.Messages.Remove(_statusMessage);
                }
            }
        }
    }
}