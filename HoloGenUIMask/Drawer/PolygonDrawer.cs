﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Image;
using HoloGen.Utils;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace HoloGen.UI.Mask.Drawer
{
    /// <summary>
    /// <see cref="Drawer" /> for polygon shapes.
    /// </summary>
    public class PolygonDrawer : Drawer
    {
        private readonly StatusMessage _statusMessage = new StatusMessage();
        private Polygon _body;
        private bool _messageadded;

        public PolygonDrawer(Canvas canvas, MaskViewModel viewModel) : base(canvas, viewModel)
        {
            BindListBox();
            Draw();
        }

        public void Draw()
        {
            _body = new Polygon
            {
                Fill = new SolidColorBrush(ThemeColors.HighlightColor),
                Stroke = new SolidColorBrush(ThemeColors.AccentColor),
                StrokeThickness = 2,
                Opacity = 0.6
            };
            _body.Points.Add(new Point());

            Canvas.Children.Add(_body);
        }

        public void BindListBox()
        {
        }

        public override void MouseMove(Point p)
        {
            if (ViewModel.OperationMode == OperationMode.Drawing)
                if (_body != null && _body.Points.Count > 0)
                {
                    var pgPointCount = _body.Points.Count;

                    _body.Points[pgPointCount - 1] = p;
                }

            var msg = "";
            if (_body != null)
                for (var i = 1; i <= _body.Points.Count; i++)
                    msg += string.Format(
                        Resources.Properties.Resources.PolygonDrawer_MouseMove_Point01F02F0,
                        i,
                        _body.Points[i - 1].X,
                        _body.Points[i - 1].Y);
            _statusMessage.Message = msg;
        }

        public override void MouseLeftButtonDown(Point p, bool doubleClick)
        {
            if (ViewModel.OperationMode == OperationMode.Drawing)
            {
                if (doubleClick)
                {
                    _body.Points.RemoveAt(_body.Points.Count - 1);

                    ViewModel.DisplayableShapeCollection.Add(_body);
                    ViewModel.FireShapeCollectionChanged();

                    var points = new List<Point>();
                    foreach (var p1 in _body.Points) points.Add(p1);
                    ViewModel.SaveableShapeCollection.Add(new Tuple<ShapeType, List<Point>>(ShapeType.Ellipse, points));

                    _body = new Polygon
                    {
                        Stroke = new SolidColorBrush(Colors.Black),
                        StrokeThickness = 2,
                        Fill = new SolidColorBrush(Colors.Blue),
                        Opacity = 0.6
                    };
                    _body.Points.Add(new Point());

                    Canvas.Children.Add(_body);

                    StatusMessageFramework.Messages.Remove(_statusMessage);

                    StatusMessageFramework.Messages.Add(_statusMessage);

                    BindListBox();
                }
                else if (_body != null)
                {
                    var pgPointCount = _body.Points.Count;
                    _body.Points[pgPointCount - 1] = p;
                    _body.Points.Add(p);

                    if (!_messageadded)
                    {
                        StatusMessageFramework.Messages.Add(_statusMessage);
                        _messageadded = true;
                    }
                }
            }
        }
    }
}