﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace HoloGen.UI.Mask.Model
{
    /// <summary>
    /// Modified from: https://wpfshapedemo.codeplex.com/
    /// </summary>
    public class DPoint : DShape
    {
        public Point PointBody { get; set; }

        public Rectangle PointDrawingBody { get; set; }

        public DPoint()
        {
            CommonProperty = new CommonProperty();

            PointBody = new Point();
            PointDrawingBody = new Rectangle();
        }

        public Rectangle DrawPoint()
        {
            PointDrawingBody.Fill = new SolidColorBrush(Colors.Black);
            PointDrawingBody.Width = 6;
            PointDrawingBody.Height = 6;
            PointDrawingBody.SetValue(Canvas.LeftProperty, PointBody.X - 3);
            PointDrawingBody.SetValue(Canvas.TopProperty, PointBody.Y - 3);

            return PointDrawingBody;
        }
    }
}
