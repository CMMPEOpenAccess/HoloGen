﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace HoloGen.UI.Mask.Model
{
    /// <summary>
    /// Modified from: https://wpfshapedemo.codeplex.com/
    /// </summary>
    public class DEllipse : DShape
    {
        public Ellipse EllipseBody { get; set; }
        public Point P1 { get; set; }
        public Point P2 { get; set; }

        public DEllipse()
        {
            CommonProperty = new CommonProperty();

            EllipseBody = new Ellipse();
            P1 = new Point();
            P2 = new Point();
        }
        
        public double Width
        {
            get
            {
                return Math.Abs(P1.X - P2.X);
            }
            set
            {
                EllipseBody.Width = value;
            }
        }
        
        public double Height
        {
            get
            {
                return Math.Abs(P1.Y - P2.Y);
            }
            set
            {
                EllipseBody.Height = value;
            }
        }
        
        public double Top
        {
            get
            {
                if (P1.Y < P2.Y)
                {
                    return P1.Y;
                }
                else
                {
                    return P2.Y;
                }
            }
            set
            {
                EllipseBody.SetValue(Canvas.TopProperty, value);
            }
        }
        
        public double Left
        {
            get
            {
                if (P1.X < P2.X)
                {
                    return P1.X;
                }
                else
                {
                    return P2.X;
                }
            }
            set
            {
                EllipseBody.SetValue(Canvas.LeftProperty, value);
            }
        }
    }
}
