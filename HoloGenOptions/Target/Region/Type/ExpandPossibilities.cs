﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using HoloGen.Hierarchy.OptionTypes;

namespace HoloGen.Options.Target.Region.Type
{
    /// <summary>
    /// Expand type possibilities.
    /// </summary>
    public sealed class ExpandPossibilities : PossibilityCollection<ExpandPossibility>
    {
        public ExpandBottom ExpandBottom { get; } = new ExpandBottom();
        public ExpandTop ExpandTop { get; } = new ExpandTop();
        public ExpandLeft ExpandLeft { get; } = new ExpandLeft();
        public ExpandRight ExpandRight { get; } = new ExpandRight();
        public ExpandCentre ExpandCentre { get; } = new ExpandCentre();
        public ExpandBottomLeft ExpandBottomLeft { get; } = new ExpandBottomLeft();
        public ExpandBottomRight ExpandBottomRight { get; } = new ExpandBottomRight();
        public ExpandTopLeft ExpandTopLeft { get; } = new ExpandTopLeft();
        public ExpandTopRight ExpandTopRight { get; } = new ExpandTopRight();
    }
}