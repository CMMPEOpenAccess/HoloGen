﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System.IO;

namespace HoloGen.Utils
{
    /// <summary>
    /// Static handle to the log file. Watch threading on this
    /// </summary>
    public class LogFileHandle
    {
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly FileStream _logfilewriter;
        // ReSharper disable once IdentifierTypo
        private readonly StreamWriter _logstreamwriter;

        private LogFileHandle()
        {
            _logfilewriter = new FileStream("Log.txt", FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            _logstreamwriter = new StreamWriter(_logfilewriter) {AutoFlush = true};
        }

        public static LogFileHandle Instance { get; } = new LogFileHandle();

        public void Log(string msg)
        {
            _logstreamwriter.WriteLine(msg);
        }
    }
}