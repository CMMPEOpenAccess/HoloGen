﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System;
using System.Collections;
using System.Linq;
using System.Reflection;

namespace HoloGen.Utils.ExtensionMethods
{
    public static class ObjectExtensions
    {
        public static T DeepCopy<T>(object obj, bool deep = false) where T : new()
        {
            if (!(obj is T)) throw new Exception("Cloning object must match output type");

            return (T) DeepCopy(obj, deep);
        }

        public static object DeepCopy(this object obj, bool deep = false)
        {
            if (obj == null) return null;

            var objType = obj.GetType();

            if (objType.IsPrimitive || objType == typeof(string) || objType.GetConstructors().FirstOrDefault(x => x.GetParameters().Length == 0) == null) return obj;

            var properties = objType.GetProperties().ToList();
            if (deep) properties.AddRange(objType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic));

            var newObj = Activator.CreateInstance(objType);

            foreach (var prop in properties)
                if (prop.GetSetMethod() != null)
                {
                    var proceed = true;
                    if (obj is IList list)
                    {
                        var listType = list.GetType().GetProperty("Item")?.PropertyType;
                        if (prop.PropertyType == listType)
                        {
                            proceed = false;
                            foreach (var item in list)
                            {
                                var clone = DeepCopy(item, deep);
                                (newObj as IList)?.Add(clone);
                            }
                        }
                    }

                    if (proceed)
                    {
                        var propValue = prop.GetValue(obj, null);
                        var clone = DeepCopy(propValue, deep);
                        prop.SetValue(newObj, clone, null);
                    }
                }

            return newObj;
        }
    }
}