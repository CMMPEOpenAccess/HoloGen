﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
namespace HoloGen.Utils
{
    public static class FileTypes
    {
        public const string CombinedFileFilter = "All HoloGen File Types (*.hfset;*.hfimg;*.hfbat) | *.hfset;*.hfimg;*.hfbat";
        public const string SetupFileFilter = "Hologram Setup File (*.hfset) | *.hfset";
        public const string BatchFileFilter = "Hologram Batch File (*.hfbat) | *.hfbat";
        public const string ImageFileFilter = "Hologram Image File (*.hfimg) | *.hfimg";
        public const string AllFileFilter = "All File Types (*.*) | *.*";
        public const string TextFileFilter = "Text Files (*.txt) | *.txt";
        public const string DirectoryFilter = "";

        public const string MatlabFileFilter = "Matlab File (*.mat) | *.mat";
        public const string ExcelFileFilter = "Microsoft Excel File (*.xlsx) | *.xlsx";

        public const string AllImagesFileFilter = "All Importable Image Files (*.bmp;*.gif;*.jpg;*.png;*.tiff) | *.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff";
        public const string BmpFileFilter = "BMP Image File (*.bmp) | *.bmp";
        public const string GifFileFilter = "GIF Image File (*.gif) | *.gif";
        public const string JpgFileFilter = "JPEG Image File (*.jpg;*.jpeg) | *.jpg;*.jpeg";
        public const string PngFileFilter = "PNG Image File (*.png) | *.png";
        public const string TffFileFilter = "TIFF Image File (*.tiff) | *.tiff";

        public const string SetupFileExtension = ".hfset";
        public const string ImageFileExtension = ".hfimg";
        public const string SettingsFileExtension = ".hfcfg";
        public const string BatchFileExtension = ".hfbat";

        public const string BmpFileExtension = ".bmp";
        public const string GifFileExtension = ".gif";
        public const string JpgFileExtension = ".jpg";
        public const string PngFileExtension = ".png";
        public const string TffFileExtension = ".tff";
    }
}