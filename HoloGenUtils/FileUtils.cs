﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace HoloGen.Utils
{
    /// <summary>
    /// Utilities for file managing. <see cref="FileMatchesFilter(FileInfo, string)" /> is equivalent
    /// to the filter in the <see cref="OpenFileDialog" />. <see cref="WildcardToRegex(string)" /> is
    /// used for converting between traditional MS wildcard terminology and a modern regex format.
    /// </summary>
    public class FileUtils
    {
        public static FileInfo LastFileDialogLocation =
            new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

        public static readonly FileInfo DefaultFolder =
            new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

        public static bool FileMatchesFilter(FileInfo file, string filter)
        {
            var regex = new Regex(@"(?<Name>[^|]*)\|(?<Extension>[^|]*)\|?");
            var matches = regex.Matches(filter);

            foreach (Match match in matches)
                if (match.Groups["Extension"].Value.Trim().Remove(0, 1) == file.Extension)
                    return true;
            return false;
        }

        public static bool FilenameMatchesFilter(string filename, string filter)
        {
            var theMatch = Regex.Match(filename, WildcardToRegex(filter), RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return theMatch.Value == filename;
        }

        public static string WildcardToRegex(string wildcard)
        {
            if (wildcard == null) return null;
            var buffer = new StringBuilder();
            var chars = wildcard.ToCharArray();
            foreach (var t in chars)
                if (t == '*')
                    buffer.Append(".*");
                else if (t == '?')
                    buffer.Append(".?");
                else if ("+()^$.{}[]|\\".IndexOf(t) != -1)
                    buffer.Append('\\').Append(t); // prefix all metacharacters with backslash
                else
                    buffer.Append(t);
            return buffer.ToString().ToLower();
        }

        public static FileInfo GetUniqueNameGUID(FileInfo file, string input)
        {
            return new FileInfo(
                file.Directory +
                Path.GetFileNameWithoutExtension(file.Name) +
                "_" +
                input +
                "_" +
                // ReSharper disable once StringLiteralTypo
                DateTime.Now.ToString("yyyyMMddHHmmss") +
                "_" +
                Guid.NewGuid().ToString("N") +
                FileTypes.ImageFileExtension);
        }

        public static FileInfo GetUniqueName(DirectoryInfo dir, string prefix, string extension)
        {
            var lastUsedFileNo = 1;
            FileInfo file = null;

            if (!IsFileNameValid(prefix)) prefix = "file";

            while (file == null)
            {
                file = new FileInfo(dir.FullName + "\\" + prefix + "_" + lastUsedFileNo.ToString("D3") + "." + extension);
                lastUsedFileNo++;
                if (file.Exists) file = null;
            }

            // TODO: Check for overrun

            return file;
        }

        private static bool IsFileNameValid(string prefix)
        {
            return !string.IsNullOrEmpty(prefix) && prefix.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;
        }
    }
}