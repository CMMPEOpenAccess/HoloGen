﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;

namespace HoloGen.Utils
{
    /// <summary>
    /// Class to extract system info into a user readable form. This can then be stored in any save files for
    /// debugging.
    /// </summary>
    public class InfoUtils
    {
        public static string GetInfo()
        {
            var sb = new System.Text.StringBuilder();

            sb.AppendLine(Resources.Properties.Resources.InfoUtils_GetInfo_SystemInformation);
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_WindowsVersion0, Environment.OSVersion));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_64BitProcess0, Environment.Is64BitProcess ? "Yes" : "No"));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_64BitOperatingSystem0, Environment.Is64BitOperatingSystem ? "Yes" : "No"));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_PCName0, Environment.MachineName));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_NumberOfCPUS0, Environment.ProcessorCount));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_WindowsFolder0, Environment.SystemDirectory));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_CurrentFolder0, Environment.CurrentDirectory));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_MachineName0, Environment.MachineName));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_User0, Environment.UserName));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_Domain0, Environment.UserDomainName));
            sb.AppendLine(string.Format(Resources.Properties.Resources.InfoUtils_GetInfo_CLRVersion0, Environment.Version));


            //sb.AppendLine();
            //sb.AppendLine(HoloGen.Resources.Properties.Resources.InfoUtils_GetInfo_EnvironmentVariables);
            //foreach (string envvar in
            //    (from string env in System.Environment.GetEnvironmentVariables().Keys
            //     orderby env
            //     select env))
            //{
            //    sb.AppendLine(envvar + "=" + System.Environment.GetEnvironmentVariable(envvar));
            //}

            return sb.ToString();
        }
    }
}