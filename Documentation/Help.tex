\documentclass[]{article}

%opening
\title{HoloGen User's Manual}
\author{Peter J. Christopher}

\usepackage{tikz}
\usepackage{calc}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	linkcolor=black,
	filecolor=black,      
	urlcolor=black,
}

% Inline images
\newlength\myheight
\newlength\mydepth
\settototalheight\myheight{Xygp}
\settodepth\mydepth{Xygp}
\setlength\fboxsep{0pt}
\newcommand*\inlinegraphics[1]{%
	\settototalheight\myheight{Xygp}%
	\settodepth\mydepth{Xygp}%
	\raisebox{-\mydepth}{\includegraphics[height=\myheight]{#1}}%
}

\begin{document}

\maketitle

\begin{abstract}
	\input{license.tex}
\end{abstract}

\section{Introduction}
\input{WelcomeParagraph.tex}

\section{Credits}
\input{Credits.tex}
\clearpage

\tableofcontents
\clearpage


\section{Getting Started}
\subsection{Installation}

To install HoloGen navigate to the following URL while connected to the engineering department network as shown in Figure~\ref{Capture003}:\footnote{For wireless connection use the CUED network without a VPN.}

\begin{itemize}
	\item \textit{\textbackslash{}\textbackslash{}DESKTOP-PJC016\textbackslash{}Users\textbackslash{}palav\textbackslash{}Documents\textbackslash{}FTP\textbackslash{}HoloGenPublish}
\end{itemize}

Double click on \textit{setup.exe} to install the application and follow the steps through.

\begin{itemize}
	\item \textbf{Note: } You may be asked to install some pre-requisite materials. Click \textit{Accept} and continue.
\end{itemize}

\begin{figure}
	\includegraphics[width=\textwidth]{Figures/Capture003.PNG}
	\caption{HoloGen deployment folder}
	\label{Capture003}
\end{figure}

\subsection{Menus}

Load HoloGen from the start menu or by double clicking on the desktop shortcut. When you load first load HoloGen you should see a screen similar to Figure~\ref{Capture100}.

\begin{figure}
	\includegraphics[width=\textwidth]{Figures/Capture100.PNG}
	\caption{HoloGen welcome screen}
	\label{Capture100}
\end{figure}

You can load the application menu by clicking on the menu \inlinegraphics{Figures/menu.png} icon in the top left of the screen. You should see a screen similar to Figure~\ref{Capture101}. The \textit{File} menu contains all the necessary commands for importing and exporting data from HoloGen. Click on "New Setup File" to open a new HoloGen setup file as in Figure~\ref{Capture106}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture101.PNG}
\caption{HoloGen file menu}
\label{Capture101}
\end{figure}

If you are lost at any time you can open the help documentation from the \textit{Help} menu - Figure~\ref{Capture102}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture102.PNG}
\caption{HoloGen help menu}
\label{Capture102}
\end{figure}

Figure~\ref{Capture103} shows the application \textit{Settings} menu.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture103.PNG}
\caption{HoloGen settings menu}
\label{Capture103}
\end{figure}

You can change the application accent colours and theme in the \textit{Accents} and \textit{Themes} menus shown in Figures~\ref{Capture104} and \ref{Capture105}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture104.PNG}
\caption{HoloGen accents menu - 1}
\label{Capture104}
\end{figure}
\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture105.PNG}
\caption{HoloGen accents menu - 2}
\label{Capture105}
\end{figure}

\subsection{Setup}

The computer hologram generation parameter setup file is shown in Figure~\ref{Capture106}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture106.PNG}
\caption{HoloGen setup algorithm options}
\label{Capture106}
\end{figure}

A discussion of the individual parameters is beyond the scope of this document though brief descriptions are given in tooltips. Refer to the published thesis titled "High Rate Additive Manufacture using Holographic Beam Shaping" which is available upon request from the author. Incorrect parameter values are highlighted similarly to shown in Figure~\ref{Capture107}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture107.PNG}
\caption{HoloGen error reporting - 1}
\label{Capture107}
\end{figure}

A full list of errors is also shown in the output tab - Figure~\ref{Capture108}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture108.PNG}
\caption{HoloGen error reporting - 2}
\label{Capture108}
\end{figure}

Parameters for the system projector, targets and hardware are shown in Figures~\ref{Capture109}, \ref{Capture110} and \ref{Capture111} respectively.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture109.PNG}
\caption{HoloGen setup projector options}
\label{Capture109}
\end{figure}
\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture110.PNG}
\caption{HoloGen setup target options}
\label{Capture110}
\end{figure}
\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture111.PNG}
\caption{HoloGen setup hardware options}
\label{Capture111}
\end{figure}

\subsection{Image Viewer}

HoloGen has a built in image viewer for images of complex values. Lenna is shown in Figure~\ref{Capture112}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture112.PNG}
\caption{HoloGen image viewer}
\label{Capture112}
\end{figure}

Simple transformations can be run directly in the view and a range of visualisation options are available. For example, the phase distribution of the fast fourier transform is shown in Figure~\ref{Capture113}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture113.PNG}
\caption{HoloGen image viewer transforms}
\label{Capture113}
\end{figure}

The color scheme can also be changed - Figure~\ref{Capture114}.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture114.PNG}
\caption{HoloGen image viewer colour schemes}
\label{Capture114}
\end{figure}

Clicking on the \textit{View Style} drop down, Figure~\ref{Capture115}, allows you to select a 3D visualisation of the image.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture115.PNG}
\caption{HoloGen 3D image viewer}
\label{Capture115}
\end{figure}

Figure~\ref{Capture116} shows masked regions being drawn on the image.

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture116.PNG}
\caption{HoloGen image masking}
\label{Capture116}
\end{figure}

\subsection{Running a setup file}

\begin{figure}
\includegraphics[width=\textwidth]{Figures/Capture117.PNG}
\caption{HoloGen process view}
\label{Capture117}
\end{figure}
\section{How Do I?}
\subsection{Change the Language}

HoloGen comes with a number of built in translations including:

\begin{itemize}
	\item \textbf{French} - fr - Translation courtesy of Ralf Mouthaan
	\item \textbf{German} - de - Translation courtesy of Sophia Gruber
	\item \textbf{Chinese} - zh - Translation courtesy of Daoming Dong
	\item \textbf{English} - en - Edition courtesy of Victoria Barrett
\end{itemize}

Where the operating system is already using one of these languages, HoloGen will automatically switch. If desired the application language can be chosen from the command line. To do this:
\begin{itemize}
	\item Open the application shortcut folder shown in Figure~\ref{Capture002}
	\item Run the application with appropriate command line arguments as shown in Figure~\ref{Capture001}. For example, using command line argument \textit{fr} will load the French version of HoloGen
\end{itemize}

\begin{figure}
	\includegraphics[width=\textwidth]{Figures/Capture002.PNG}
	\caption{Loading HoloGen from the command line}
	\label{Capture002}
\end{figure}

\begin{figure}
	\includegraphics[width=\textwidth]{Figures/Capture001.PNG}
	\caption{Loading HoloGen from the command line}
	\label{Capture001}
\end{figure}

\subsection{Import and Export Data}\label{import}

You can import and export data using the \textit{Import/Export} submenu of the \textit{File} menu as shown in Figure~\ref{Capture119}. Hover over individual menu items when in doubt about their use.

\begin{figure}
	\includegraphics[width=\textwidth]{Figures/Capture119.PNG}
	\caption{HoloGen data import and export}
	\label{Capture119}
\end{figure}

\subsection{Generate a  Hologram?}

To generate a hologram, load a \textit{.hfimg} file. You can import this in the way described in Section~\ref{import}. The key settings you will need to change are the \textit{target image} on the \textit{Targets} page and the SLM resolutions on the \textit{Projector} page. Refer to the published thesis titled "High Rate Additive Manufacture using Holographic Beam Shaping" for details of other parameters. Incorrect parameter values are highlighted similarly to shown in Figure~\ref{Capture107}.

\subsection{View Metadata}

Images generated using the HoloGen computer generation algorithms are saved with the generation parameters in the metadata. Clicking on the \textit{Show Original Parameters} button in Figure~\ref{Capture118} will allow you to view them.

\begin{figure}
	\includegraphics[width=\textwidth]{Figures/Capture118.PNG}
	\caption{HoloGen metadata}
	\label{Capture118}
\end{figure}

\subsection{Batch Processing}

Holograms can also be batch generated. Figure~\ref{Capture121} shows the batch processing options. The parameters on the left are all the available algorithm parameters. Using the buttons on the top-left allows for adding rows and columns to the batch processing data grid. Once complete, press \textit{Run} to run.

\begin{figure}
	\includegraphics[width=\textwidth]{Figures/Capture121.PNG}
	\caption{HoloGen metadata}
	\label{Capture121}
\end{figure}

\section{Further Information}

\subsection{Contributing}

Contributions to the project are greatly welcome! The project is hosted on the CMMPE GitLab at:

\begin{itemize}
\item \textit{git@ee-git.eng.cam.ac.uk:CMMPE/HoloGen.git}
\end{itemize}

Connection is via SSH key only. If you don't know how to do this, then speak to Peter Christopher.

\subsection{Learn More}
\begin{itemize}
\item \href{Welcome.pdf}{Introduction to HoloGen}
\item \href{Help.pdf}{HoloGen Manual}
\item \href{About.pdf}{Legal Stuff}
\end{itemize}
\end{document}\textsl{}
