﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace HoloGen.UI.Menus.Commands
{
    /// <summary>
    /// HoloGen menu command
    /// </summary>
    public class ExportImageFileCommand : AbstractExportCommand<ICanExportBitmap>
    {
        public ExportImageFileCommand()
        {
        }

        public ExportImageFileCommand(IMenuWindow context) : base(context)
        {
        }

        public override string Name => Resources.Properties.Resources.ExportImageFileCommand_ExportToImageFile;

        public override string ToolTip => Resources.Properties.Resources.ExportImageFileCommand_ExportTheCurrentHoloGenComplexImageUsingTheCurrentViewAndColourScheme;

        public override bool CanExecute(object parameter)
        {
            return
                Context?.Selected is ICanExportBitmap;
        }

        public override string Export(FileInfo file, ICanExportBitmap tab)
        {
            ImageFormat format;
            switch (file.Extension)
            {
                case FileTypes.BmpFileExtension:
                    format = ImageFormat.Bmp;
                    break;
                case FileTypes.GifFileExtension:
                    format = ImageFormat.Gif;
                    break;
                case FileTypes.JpgFileExtension:
                    format = ImageFormat.Jpeg;
                    break;
                case FileTypes.PngFileExtension:
                    format = ImageFormat.Png;
                    break;
                case FileTypes.TffFileExtension:
                    format = ImageFormat.Tiff;
                    break;
                default:
                    ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                        Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                        Resources.Properties.Resources.ExportImageFileCommand_Execute_ErrorUnknownFileType);
                    return "";
            }

            return tab.ExportBitmap(
                file,
                format,
                Context.ApplicationSettings.GeneralSettingsPage.FileSettingsFolder.SaveScaleBarOnImageExport.Value);
        }

        public override string Filter(ICanExportBitmap tab)
        {
            return tab.BitmapFileFilter;
        }
    }
}