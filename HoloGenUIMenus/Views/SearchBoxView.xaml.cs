﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.UI.Menus.ViewModels;
using MahApps.Metro.Controls;
using NHotkey;
using NHotkey.Wpf;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace HoloGen.UI.Menus.Views
{
    /// <summary>
    /// Search box control.
    /// Currently the search boxes are duplicated but could easily be combined into a new library with some dependancy
    /// properties.
    /// Issue #31 covers this
    /// </summary>
    public sealed partial class SearchBoxView : UserControl, INotifyPropertyChanged
    {
        private readonly HotKey _hotKey = new HotKey(Key.Escape);
        private ContentControl _bottomPane;
        private bool _showingSearchBox;

        public SearchBoxView()
        {
            InitializeComponent();

            Loaded += SearchBoxView_Loaded;
        }

        public ContentControl BottomPane
        {
            get => _bottomPane;
            set
            {
                _bottomPane = value;
                OnPropertyChanged();
            }
        }

        public SearchBoxViewModel SearchBoxViewModel { get; private set; }

        /// <summary>
        /// Occurs when property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void SearchBoxView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                // ReSharper disable once UsePatternMatching
                var model = DataContext as SearchBoxViewModel;
                if ((model != null) & (SearchBoxViewModel == null)) SearchBoxViewModel = model;
            }
        }

        private void SearchButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!_showingSearchBox)
                ShowSearchBox();
            else
                HideSearchBox();
        }

        private void OnHotKey(object sender, HotkeyEventArgs e)
        {
            HideSearchBox();
            e.Handled = true;
        }

        private void ShowSearchBox()
        {
            _showingSearchBox = true;

            SearchBox.Focusable = true;
            SearchBox.Focus();
            SearchBox.SelectAll();
            FocusManager.SetFocusedElement(this, SearchBox);
            Keyboard.Focus(SearchBox);
            var sb = FindResource("ScaleTextBoxStoryboard") as Storyboard;
            Debug.Assert(sb != null, nameof(sb) + " != null");
            Storyboard.SetTarget(sb, SearchBox);
            sb.Begin();
            HotkeyManager.Current.AddOrReplace("esc", _hotKey.Key, _hotKey.ModifierKeys, OnHotKey);
        }

        private void HideSearchBox()
        {
            _showingSearchBox = false;

            var sb = FindResource("ReverseScaleTextBoxStoryboard") as Storyboard;
            Debug.Assert(sb != null, nameof(sb) + " != null");
            Storyboard.SetTarget(sb, SearchBox);
            sb.Begin();

            SearchBoxViewModel.HierarchyRoot.SearchTerm = "";
            HotkeyManager.Current.Remove("esc");
        }

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;

            changed?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}