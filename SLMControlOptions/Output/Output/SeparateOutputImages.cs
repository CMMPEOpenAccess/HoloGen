﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using HoloGen.SLMControl.Options.Params.Params;
using System.Collections.Specialized;

namespace HoloGen.SLMControl.Options.Output.Output
{
    /// <summary>
    /// Should the output images be separated or packed?
    /// </summary>
    public sealed class SeparateOutputImages : BooleanOptionWithChildren
    {
        public override string OnText => Resources.Properties.Resources.SeparateOutputImages_Yes;

        public override string OffText => Resources.Properties.Resources.SeparateOutputImages_No;

        public override string Watermark => Resources.Properties.Resources.SeparateOutputImages_SeparateOutputImagesOrPackThemIntoFewerFiles;

        public override string Name => Resources.Properties.Resources.SeparateOutputImages_SeparateOutputImages;

        public override string ToolTip => Watermark + ". " + Resources.Properties.Resources.SeparateOutputImages_IfTrue48InputImagesWithAResolutionXYWillBeSavedTo48OutputImagesWithResolutionSpecifiedByTheUserIfFalse48InputImagesWithAResolutionXYAndABitRateOf24WillBeSavedAs2OutputImagesWithResolutionXY;

        #if DEBUG
        public override bool Default => true;
        #else
        public override bool Default => false;
#endif

        public OutputResolutionX OutputResolutionX { get; } = new OutputResolutionX();
        public OutputResolutionY OutputResolutionY { get; } = new OutputResolutionY();

        public SeparateOutputImages()
        {
            OutputResolutionX.PropertyChanged += Resolution_PropertyChanged;
            OutputResolutionY.PropertyChanged += Resolution_PropertyChanged;
        }

        private SLMResolutionX _slmResolutionX;
        private SLMResolutionY _slmResolutionY;
        private BitRateTypeOption _bitRateTypeOption;

        public void SetResolutionParameters(SLMResolutionX slmResolutionX, SLMResolutionY slmResolutionY, BitRateTypeOption bitRateTypeOption)
        {
            _slmResolutionX = slmResolutionX;
            _slmResolutionY = slmResolutionY;
            _bitRateTypeOption = bitRateTypeOption;

            _slmResolutionX.PropertyChanged += Resolution_PropertyChanged;
            _slmResolutionY.PropertyChanged += Resolution_PropertyChanged;
            _bitRateTypeOption.PropertyChanged += Resolution_PropertyChanged;

            CheckResolutions();
        }

        private void Resolution_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckResolutions();
        }

        private void CheckResolutions()
        {
            if (_slmResolutionX == null || _slmResolutionY == null)
                return;
            var inputPixels = _slmResolutionX.Value * _slmResolutionY.Value;
            var outputPixels = OutputResolutionX.Value * OutputResolutionY.Value;
            var bitRate = _bitRateTypeOption.Value.BitRate;

            _resolutionError = inputPixels > outputPixels * bitRate ? Resources.Properties.Resources.SLMControlOutputFolder_CheckResolutions_ErrorTheTotalNumberOfPixelsInTheOutputDividedByTheBitRateMustBeGreaterThanOrEqualToTheTotalNumberOfInputPixels : null;

            OnPropertyChanged($"Valid");
        }

        private string _resolutionError;

        public override bool Valid => base.Valid && Value && _resolutionError != null;

        public override StringCollection Error
        {
            get
            {
                var error = base.Error;
                if (Value && _resolutionError != null) error.Add(_resolutionError);
                return error;
            }
        }
    }
}