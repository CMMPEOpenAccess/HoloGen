﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.SLMControl.Options.Output.Output;
using HoloGen.SLMControl.Options.Params.Params;

namespace HoloGen.SLMControl.Options.Output
{
    /// <summary>
    /// Folder of options relating to the image view style.
    /// </summary>
    public sealed class SLMControlOutputFolder : HierarchyFolder
    {
        public SLMControlOutputFolder()
        {
            SeparateOutputImages.SetPushLocation(this);
        }

        public OutputFolder OutputFolder { get; } = new OutputFolder();
        public OutputName OutputName { get; } = new OutputName();
        public OutputOverwrite OutputOverwrite { get; } = new OutputOverwrite();
        public SeparateOutputImages SeparateOutputImages { get; } = new SeparateOutputImages();

        public override string Name => Resources.Properties.Resources.SLMControlOutputFolder_OutputParameters;

        public override string ToolTip => Resources.Properties.Resources.SLMControlOutputFolder_ParametersControllingTheProgramOutput;

        public void SetResolutionParameters(SLMResolutionX slmResolutionX, SLMResolutionY slmResolutionY, BitRateTypeOption bitRateTypeOption)
        {
            SeparateOutputImages.SetResolutionParameters(slmResolutionX, slmResolutionY, bitRateTypeOption);
        }
    }
}