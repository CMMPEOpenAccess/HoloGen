﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using HoloGen.Hierarchy.OptionTypes;

namespace HoloGen.SLMControl.Options.Params.Params
{
    /// <summary>
    /// Binarisation strategy used.
    /// </summary>
    public sealed class BinarisationPossibilities : PossibilityCollection<BinarisationPossibility>
    {
        public BinarisationGrayScale BinarisationGrayScale { get; } = new BinarisationGrayScale();
        public BinarisationMean BinarisationL1Norm { get; } = new BinarisationMean();
        public BinarisationL2Norm BinarisationL2Norm { get; } = new BinarisationL2Norm();
        public BinarisationRed BinarisationRed { get; } = new BinarisationRed();
        public BinarisationGreen BinarisationGreen { get; } = new BinarisationGreen();
        public BinarisationBlue BinarisationBlue { get; } = new BinarisationBlue();
    }
}