﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using HoloGen.Hierarchy.OptionTypes;

namespace HoloGen.SLMControl.Options.Params.Params
{
    /// <summary>
    /// Output BitRate
    /// </summary>
    public sealed class BitRatePossibilities : PossibilityCollection<BitRatePossibility>
    {
        public BitRate24 BitRate24 { get; } = new BitRate24();
        public BitRate8Red BitRate8Red { get; } = new BitRate8Red();
        public BitRate8Green BitRate8Green { get; } = new BitRate8Green();
        public BitRate8Blue BitRate8Blue { get; } = new BitRate8Blue();
        public BitRate1Red BitRate1Red { get; } = new BitRate1Red();
        public BitRate1Green BitRate1Green { get; } = new BitRate1Green();
        public BitRate1Blue BitRate1Blue { get; } = new BitRate1Blue();
    }
}