﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.UI.Utils.MVVM;
using LiveCharts;
using LiveCharts.Configurations;
using System;
using HoloGen.Alg.Base.Managed;

namespace HoloGen.UI.Graph.ViewModels
{
    /// <summary>
    /// ViewModel for the <see cref="GraphViewModel" /> control.
    /// </summary>
    public class GraphViewModel : BindableBase
    {
        private double _axisMax;
        private double _axisMin;
        private double _axisStep;
        private double _axisUnit;
        private ChartValues<GraphItemViewModel> _chartValues;
        private CartesianMapper<GraphItemViewModel> _mapper;
        private MetricType _processViewType = MetricType.MeanSquaredError;
        private bool _reading;

        private ZoomingOptions _zoomingMode;

        public GraphViewModel()
        {
            Mapper = Mappers.Xy<GraphItemViewModel>()
                .X(model => model.DateTime.Ticks) //use DateTime.Ticks as X
                .Y(model => model.Values[ProcessViewType]); //use the value property as Y

            Charting.For<GraphItemViewModel>(Mapper);

            ZoomingMode = ZoomingOptions.X;

            ChartValues = new ChartValues<GraphItemViewModel>();
            DateTimeFormatter = value => new DateTime((long) value).ToString("mm:ss");
            AxisStep = TimeSpan.FromSeconds(1).Ticks;
            AxisUnit = TimeSpan.TicksPerSecond;
            SetAxisLimits(DateTime.Now);
            IsReading = false;
        }

        public Func<double, string> DateTimeFormatter { get; set; }

        public ZoomingOptions ZoomingMode
        {
            get => _zoomingMode;
            set => SetProperty(ref _zoomingMode, value);
        }

        public CartesianMapper<GraphItemViewModel> Mapper
        {
            get => _mapper;
            set => SetProperty(ref _mapper, value);
        }

        public ChartValues<GraphItemViewModel> ChartValues
        {
            get => _chartValues;
            set => SetProperty(ref _chartValues, value);
        }

        public MetricType ProcessViewType
        {
            get => _processViewType;
            set => SetProperty(ref _processViewType, value);
        }

        public bool IsHot
        {
            get
            {
                if (ChartValues.Count >= 2)
                    return
                        ChartValues[ChartValues.Count - 1].Values[MetricType.MeanSquaredError] >
                        ChartValues[ChartValues.Count - 2].Values[MetricType.MeanSquaredError];
                return false;
            }
        }

        public double AxisStep
        {
            get => _axisStep;
            set => SetProperty(ref _axisStep, value);
        }

        public double AxisUnit
        {
            get => _axisUnit;
            set => SetProperty(ref _axisUnit, value);
        }

        public double AxisMax
        {
            get => _axisMax;
            set => SetProperty(ref _axisMax, value);
        }

        public double AxisMin
        {
            get => _axisMin;
            set => SetProperty(ref _axisMin, value);
        }

        public bool IsReading
        {
            get => _reading;
            set => SetProperty(ref _reading, value);
        }

        public void SetAxisLimits(DateTime now)
        {
            AxisMax = now.Ticks + TimeSpan.FromSeconds(10).Ticks;
            AxisMin = now.Ticks - TimeSpan.FromSeconds(30).Ticks;
        }

        public void IncrementAxisLimits(TimeSpan diff)
        {
            AxisMax = AxisMax + diff.Ticks;
            AxisMin = AxisMin + diff.Ticks;
        }

        public void NotifyIsHot()
        {
            OnPropertyChanged(nameof(IsHot));
        }
    }
}