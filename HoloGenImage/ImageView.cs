﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Utils;
using System.Drawing;

namespace HoloGen.Image
{
    /// <summary>
    /// An <see cref="ImageView" /> represents a single view on a <see cref="ComplexImage" /> object.
    /// For example, the frequency spectrum. Each <see cref="ImageView" /> is const, allowing for
    /// value caching. Image view defines the expected <see cref="Dimension" />, <see cref="Scale" /> and
    /// <see cref="Values" />
    /// members and wraps a <see cref="Bitmap" /> object. The <see cref="ColorScheme" /> member keeps track
    /// of the <see cref="HoloGen.Utils.ColorScheme" /> enum used when creating the image.
    /// The actual view generation is typically executed in a <see cref="ComplexImage" /> object.
    /// </summary>
    public class ImageView
    {
        public ImageView(Bitmap image, Bitmap imageWithoutScale, double[,] values, Dimension dimension, Scale scale, ColorScheme colorScheme)
        {
            Image = image;
            Values = values;
            Dimension = dimension;
            Scale = scale;
            ColorScheme = colorScheme;
            ImageWithoutScale = imageWithoutScale;
        }

        public Dimension Dimension { get; }

        public Scale Scale { get; }

        public ColorScheme ColorScheme { get; }

        public double[,] Values { get; }

        public Bitmap Image { get; }

        public Bitmap ImageWithoutScale { get; }
    }
}