﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Options;
using HoloGen.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace HoloGen.Image
{
    /// <summary>
    /// Defines a complex image type with <see cref="Values" /> storing a 2D array of
    /// <see cref="Complex" /> objects. <see cref="Dimension" /> represents the size of the
    /// image.
    /// </summary>
    public sealed class ComplexImage : HierarchySaveable
    {
        public ComplexImage() // : this((y, x) => new Complex((x + 1) * (y + 1), (x + 1) + (y + 1)), new Dimension(250, 250))
        {
        }

        public ComplexImage(Complex[,] values)
        {
            Values = values;
        }

        public ComplexImage(Complex[,] values, OptionsRoot options)
        {
            Values = values;
            OptionsMetadata = options;
            OptionsMetadataVersion = new OptionsRootVersion();
        }

        public ComplexImage(Func<int, int, Complex> initialiser, Dimension dimension)
        {
            Values = new Complex[dimension.YSize, dimension.XSize];

            // Initialise array
            for (var y = 0; y < Dimension.YSize; y++)
            for (var x = 0; x < Dimension.XSize; x++)
                Values[y, x] = initialiser(y, x);
        }

        [JsonIgnore]
        public Complex[,] Values { get; private set; }

        [JsonIgnore]
        public Dimension Dimension => Values != null ? new Dimension(Values.GetLength(1), Values.GetLength(0)) : new Dimension(0, 0);

        [JsonIgnore]
        private Dictionary<ImageViewType, Scale> Scales { get; set; } = new Dictionary<ImageViewType, Scale>();

        public OptionsRoot OptionsMetadata { get; set; }
        public OptionsRootVersion OptionsMetadataVersion { get; set; }

        public SaveableShapeCollection ShapeCollection { get; set; } = new SaveableShapeCollection();

        public double[] JsonExport
        {
            get
            {
                var export = new double[2 + Dimension.XSize * Dimension.YSize * 2];
                export[0] = Dimension.XSize;
                export[1] = Dimension.YSize;
                var i = 2;
                for (var y = 0; y < Dimension.YSize; y++)
                for (var x = 0; x < Dimension.XSize; x++)
                {
                    export[i] = Values[y, x].Real;
                    i++;
                    export[i] = Values[y, x].Imaginary;
                    i++;
                }

                return export;
            }

            set
            {
                if (value.Length >= 2)
                {
                    var dimension = new Dimension((int) value[0], (int) value[1]);
                    if (value.Length == 2 + dimension.XSize * dimension.YSize * 2)
                    {
                        var i = 2;
                        Values = new Complex[dimension.YSize, dimension.XSize];
                        for (var y = 0; y < dimension.YSize; y++)
                        for (var x = 0; x < dimension.XSize; x++)
                        {
                            Values[y, x] = new Complex(value[i], value[i + 1]);
                            i++;
                            i++;
                        }

                        return;
                    }
                }

                Values = new Complex[0, 0];
            }
        }
    }
}