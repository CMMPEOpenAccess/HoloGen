﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Utils;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace HoloGen.Image
{
    /// <summary>
    /// Handles adding scales to bitmap images
    /// </summary>
    public class ScaleAdder
    {
        public static Bitmap AddScale(Bitmap image, ImageScaleType scaleType, ColorScheme colorScheme, Scale scale)
        {
            if (scaleType == ImageScaleType.None) return image;

            // +------- X
            // |
            // |
            // |
            // Y


            const int borderWidth = 1;
            const int margin = 10;
            const int scaleWidth = 20;
            const int textOffset = 5;
            const int textHeight = 15;
            const int textWidth = 40;

            var nx = image.Width;
            var ny = image.Height;

            if (nx < margin * 2 + borderWidth * 2 + scaleWidth + textOffset + textWidth) return image;
            if (ny < margin * 2 + borderWidth * 2 + scaleWidth + textOffset + textWidth) return image;

            using (var g = Graphics.FromImage(image))
            {
                int boxSttX;
                int boxSttY;
                int boxEndX;
                int boxEndY;
                int innSttX;
                int innSttY;
                int innEndX;
                int innEndY;
                Rectangle minText;
                Rectangle midText;
                Rectangle maxText;

                if (scaleType == ImageScaleType.Left)
                {
                    boxSttX = 0 + margin;
                    boxSttY = 0 + margin;
                    boxEndX = 0 + margin + scaleWidth;
                    boxEndY = ny - margin;
                    innSttX = boxSttX + borderWidth;
                    innSttY = boxSttY + borderWidth;
                    innEndX = boxEndX - borderWidth;
                    innEndY = boxEndY - borderWidth;

                    minText = new Rectangle(
                        boxSttX + scaleWidth + textOffset,
                        boxEndY - textHeight,
                        textWidth,
                        textHeight);
                    midText = new Rectangle(
                        boxSttX + scaleWidth + textOffset,
                        (boxSttY + boxEndY - textHeight) / 2,
                        textWidth,
                        textHeight);
                    maxText = new Rectangle(
                        boxSttX + scaleWidth + textOffset,
                        boxSttY,
                        textWidth,
                        textHeight);
                }
                else if (scaleType == ImageScaleType.Right)
                {
                    boxSttX = nx - margin - scaleWidth;
                    boxSttY = 0 + margin;
                    boxEndX = nx - margin;
                    boxEndY = ny - margin;
                    innSttX = boxSttX + borderWidth;
                    innSttY = boxSttY + borderWidth;
                    innEndX = boxEndX - borderWidth;
                    innEndY = boxEndY - borderWidth;

                    minText = new Rectangle(
                        boxSttX - textOffset - textWidth,
                        boxEndY - textHeight,
                        textWidth,
                        textHeight);
                    midText = new Rectangle(
                        boxSttX - textOffset - textWidth,
                        (boxSttY + boxEndY - textHeight) / 2,
                        textWidth,
                        textHeight);
                    maxText = new Rectangle(
                        boxSttX - textOffset - textWidth,
                        boxSttY,
                        textWidth,
                        textHeight);
                }
                else if (scaleType == ImageScaleType.Top)
                {
                    boxSttX = 0 + margin;
                    boxSttY = 0 + margin;
                    boxEndX = nx - margin;
                    boxEndY = 0 + margin + scaleWidth;
                    innSttX = boxSttX + borderWidth;
                    innSttY = boxSttY + borderWidth;
                    innEndX = boxEndX - borderWidth;
                    innEndY = boxEndY - borderWidth;

                    minText = new Rectangle(
                        boxEndX - textWidth,
                        boxSttY + scaleWidth + textOffset,
                        textWidth,
                        textHeight);
                    midText = new Rectangle(
                        (boxSttX + boxEndX - textWidth) / 2,
                        boxSttY + scaleWidth + textOffset,
                        textWidth,
                        textHeight);
                    maxText = new Rectangle(
                        boxSttX,
                        boxSttY + scaleWidth + textOffset,
                        textWidth,
                        textHeight);
                }
                else // if (scaleType == ImageScaleType.Bottom)
                {
                    boxSttX = 0 + margin;
                    boxSttY = ny - margin - scaleWidth;
                    boxEndX = nx - margin;
                    boxEndY = ny - margin;
                    innSttX = boxSttX + borderWidth;
                    innSttY = boxSttY + borderWidth;
                    innEndX = boxEndX - borderWidth;
                    innEndY = boxEndY - borderWidth;

                    minText = new Rectangle(
                        boxEndX - textWidth,
                        boxSttY - textOffset - textHeight,
                        textWidth,
                        textHeight);
                    midText = new Rectangle(
                        (boxSttX + boxEndX - textWidth) / 2,
                        boxSttY - textOffset - textHeight,
                        textWidth,
                        textHeight);
                    maxText = new Rectangle(
                        boxSttX,
                        boxSttY - textOffset - textHeight,
                        textWidth,
                        textHeight);
                }
                
                LinearGradientBrush inverseBrush;
                if (scaleType == ImageScaleType.Left || scaleType == ImageScaleType.Right)
                {
                    inverseBrush = new LinearGradientBrush(
                        new Point(boxSttX, boxSttY - 1),
                        new Point(boxSttX, boxEndY + 1),
                        colorScheme.Color2,
                        colorScheme.Color1);
                }
                else
                {
                    inverseBrush = new LinearGradientBrush(
                        new Point(boxSttX - 1, boxSttY),
                        new Point(boxEndX + 1, boxSttY),
                        colorScheme.Color2,
                        colorScheme.Color1);
                }

                g.FillRectangle(
                    inverseBrush,
                    new Rectangle(
                        new Point(boxSttX, boxSttY),
                        new Size(boxEndX - boxSttX, boxEndY - boxSttY)));

                g.FillRectangle(
                    inverseBrush,
                    new Rectangle(
                        new Point(innSttX, innSttY),
                        new Size(innEndX - innSttX, innEndY - innSttY)));

                g.FillRectangle(new SolidBrush(colorScheme.Color1), minText);
                g.FillRectangle(new SolidBrush(colorScheme.Color1), midText);
                g.FillRectangle(new SolidBrush(colorScheme.Color1), maxText);

                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                var midValue = (scale.MaxValue - scale.MinValue) / 2 + scale.MinValue;

                g.DrawString(
                    string.Format(Resources.Properties.Resources.ScaleAdder_AddScale_0F1, scale.MinValue),
                    new Font("Tahoma", 8),
                    new SolidBrush(colorScheme.Color2),
                    minText);
                g.DrawString(
                    string.Format(Resources.Properties.Resources.ScaleAdder_AddScale_0F1, midValue),
                    new Font("Tahoma", 8),
                    new SolidBrush(colorScheme.Color2),
                    midText);
                g.DrawString(
                    string.Format(Resources.Properties.Resources.ScaleAdder_AddScale_0F1, scale.MaxValue),
                    new Font("Tahoma", 8),
                    new SolidBrush(colorScheme.Color2),
                    maxText);
            }

            return image;
        }
    }
}