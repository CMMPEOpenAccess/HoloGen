﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using HoloGen.Mask.Options.MouseType;
using HoloGen.UI.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace HoloGen.UI.Views
{
    /// <summary>
    /// Interaction logic for HologramTabView.xaml
    /// </summary>
    public partial class HologramTabView : UserControl
    {
        public HologramTabView()
        {
            InitializeComponent();

            Loaded += MaskView_Loaded;
        }

        public HologramTabViewModel ViewModel { get; private set; }

        private void MaskView_Loaded(object sender, RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                var model = DataContext as HologramTabViewModel;
                if ((model != null) & (ViewModel == null))
                {
                    ViewModel = model;

                    ViewModel.Options.MaskFolder.ShapeOption.PropertyChanged += ShapeOption_PropertyChanged;
                    ViewModel.Options.MaskFolder.MouseOption.PropertyChanged += MouseOption_PropertyChanged;
                }
            }
        }

        private void ShapeOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex") Viewer.SetShape(ViewModel.Options.MaskFolder.ShapeOption.Value);
        }

        private void MouseOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex")
                switch (ViewModel.Options.MaskFolder.MouseOption.Value)
                {
                    case MouseDraw _:
                        Viewer.SetDrawMode();
                        break;
                    case MousePoint _:
                        Viewer.SetPointMode();
                        break;
                }
        }
    }
}