﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using HoloGen.Options;
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.UI.ViewModels;
using System.Windows.Navigation;
using HoloGen.UI.Hamburger.Views;
using MenuItem = HoloGen.UI.Hamburger.ViewModels.HamburgerMenuItem;
using HoloGen.Image;
using HoloGen.Utils;
using System.Numerics;
using HoloGen.Mask.Options.MouseType;
using HoloGen.Mask.Options.ShapeType;

namespace HoloGen.UI.Views
{
    /// <summary>
    /// Control for viewing a <see cref="ComplexImage"/> or hologram.
    /// </summary>
    public sealed partial class HologramView : UserControl
    {
        public ComplexImage ComplexImage { get; private set; }

        public HologramViewModel ViewModel { get; private set; }

        public HologramView()
        {
            this.InitializeComponent();
            
            this.Loaded += HologramView_Loaded;
        }

        private void HologramView_Loaded(object sender, RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                var model = DataContext as HologramViewModel;
                if (model != null & ViewModel == null)
                {
                    ViewModel = model;

                    ViewModel.Options.MaskFolder.ShapeOption.PropertyChanged += ShapeOption_PropertyChanged;
                    ViewModel.Options.MaskFolder.MouseOption.PropertyChanged += MouseOption_PropertyChanged;
                }
            }
        }

        private void ShapeOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex")
            {
                viewer.setShape(ViewModel.Options.MaskFolder.ShapeOption.Value);
            }
        }

        private void MouseOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex")
            {
                switch (ViewModel.Options.MaskFolder.MouseOption.Value)
                {
                    case MouseDraw mouse:
                        viewer.setDrawMode();
                        break;
                    case MousePoint mouse:
                        viewer.setPointMode();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
