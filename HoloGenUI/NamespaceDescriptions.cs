﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
// ReSharper disable EmptyNamespace


/// <summary>
/// Base HoloGen namespace
/// </summary>
namespace HoloGen
{
}

/// <summary>
/// Base HoloGen algorithm namespace
/// </summary>
namespace HoloGen.Alg
{
}

/// <summary>
/// HoloGen base-level algorithm utilities
/// </summary>
namespace HoloGen.Alg.Base
{
}

/// <summary>
/// HoloGen base-level algorithm utilities in Cuda
/// </summary>
namespace HoloGen.Alg.Base.Cuda
{
}

/// <summary>
/// HoloGen base-level algorithm utilities in Managed c++
/// </summary>
namespace HoloGen.Alg.Base.Managed
{
}

/// <summary>
/// HoloGen base-level algorithm utilities in C#
/// </summary>
namespace HoloGen.Alg.Base.Export
{
}

/// <summary>
/// HoloGen GS algorithm
/// </summary>
namespace HoloGen.Alg.GS
{
}

/// <summary>
/// HoloGen GS algorithm in Cuda
/// </summary>
namespace HoloGen.Alg.GS.Cuda
{
}

/// <summary>
/// HoloGen GS algorithm in Managed c++
/// </summary>
namespace HoloGen.Alg.GS.Managed
{
}

/// <summary>
/// HoloGen GS algorithm in C#
/// </summary>
namespace HoloGen.Alg.GS.Export
{
}

/// <summary>
/// HoloGen DS algorithm
/// </summary>
namespace HoloGen.Alg.DS
{
}

/// <summary>
/// HoloGen DS algorithm in Cuda
/// </summary>
namespace HoloGen.Alg.DS.Cuda
{
}

/// <summary>
/// HoloGen DS algorithm in Managed c++
/// </summary>
namespace HoloGen.Alg.DS.Managed
{
}

/// <summary>
/// HoloGen DS algorithm in C#
/// </summary>
namespace HoloGen.Alg.DS.Export
{
}

/// <summary>
/// HoloGen GS algorithm
/// </summary>
namespace HoloGen.Alg.GS
{
}

/// <summary>
/// HoloGen GS algorithm in Cuda
/// </summary>
namespace HoloGen.Alg.GS.Cuda
{
}

/// <summary>
/// HoloGen GS algorithm in Managed c++
/// </summary>
namespace HoloGen.Alg.GS.Managed
{
}

/// <summary>
/// HoloGen GS algorithm in C#
/// </summary>
namespace HoloGen.Alg.GS.Export
{
}

/// <summary>
/// HoloGen OSPR algorithm
/// </summary>
namespace HoloGen.Alg.OSPR
{
}

/// <summary>
/// HoloGen OSPR algorithm in Cuda
/// </summary>
namespace HoloGen.Alg.OSPR.Cuda
{
}

/// <summary>
/// HoloGen OSPR algorithm in Managed c++
/// </summary>
namespace HoloGen.Alg.OSPR.Managed
{
}

/// <summary>
/// HoloGen OSPR algorithm in C#
/// </summary>
namespace HoloGen.Alg.OSPR.Export
{
}

/// <summary>
/// Hologram generation target options.
/// </summary>
namespace HoloGen.Options.Target.Target
{
}

///Interface between application and algorithm levels
/// </summary>
namespace HoloGen.Controller
{
}

/// <summary>
/// Definition of the abstract command types.
/// </summary>
namespace HoloGen.Hierarchy.CommandTypes
{
}

/// <summary>
/// Definition of the abstract option types.
/// </summary>
namespace HoloGen.Hierarchy.OptionTypes
{
}

/// <summary>
/// Definition of the abstract options hierarchy.
/// </summary>
namespace HoloGen.Hierarchy.Hierarchy
{
}

/// <summary>
/// Defines the abstract HoloGen option tree hierarchy which is used for serialisation and control.
/// </summary>
namespace HoloGen.Hierarchy
{
}

/// <summary>
/// Hologram generation projector options.
/// </summary>
namespace HoloGen.Options.Projector.Illumination
{
}

/// <summary>
/// Hologram generation output options.
/// </summary>
namespace HoloGen.Options.Output.Error
{
}

/// <summary>
/// Hologram generation output options.
/// </summary>
namespace HoloGen.Options.Output.Reference
{
}

/// <summary>
/// Type of termination options available.
/// </summary>
namespace HoloGen.Options.Algorithm.Termination.Type
{
}

/// <summary>
/// Algorithm options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms
{
}

/// <summary>
/// Hologram generation output options.
/// </summary>
namespace HoloGen.Options.Output.Run
{
}

/// <summary>
/// Miscellaneous common utilities for the HoloGen framework including serialisation, colour schemes and messaging.
/// </summary>
namespace HoloGen.Utils
{
}

/// <summary>
/// C# style extension methods.
/// </summary>
namespace ExtensionMethods
{
}

/// <summary>
/// Miscellaneous common utilities for the HoloGen framework including serialisation, colour schemes and messaging.
/// </summary>
namespace HoloGen.UI.Utils
{
}

/// <summary>
/// Navigation utilities for the user interface.
/// </summary>
namespace HoloGen.UI.Utils.Navigation
{
}

/// <summary>
/// MVVM related utilities for the user interface.
/// </summary>
namespace HoloGen.UI.Utils.MVVM
{
}

/// <summary>
/// Top level commands for the user interface.
/// </summary>
namespace HoloGen.UI.Utils.Commands
{
}

/// <summary>
/// User interface editors for the different <see cref="HoloGen.Options.Option"/> types.
/// </summary>
namespace HoloGen.UI.OptionEditors
{
}

/// <summary>
/// User interface menu elements.
/// </summary>
namespace HoloGen.UI.Menus
{
}

/// <summary>
/// User interface menu elements.
/// </summary>
namespace HoloGen.UI.Menus.Properties
{
}

/// <summary>
/// User interface menu elements.
/// </summary>
namespace HoloGen.UI.Menus.ViewModels
{
}

/// <summary>
/// User interface menu elements.
/// </summary>
namespace HoloGen.UI.Menus.Views
{
}

/// <summary>
/// Themes for the HoloGen application.
/// </summary>
namespace HoloGen.UI.Menus.Theme
{
}

/// <summary>
/// Commands for the HoloGen application.
/// </summary>
namespace HoloGen.UI.Commands
{
}

/// <summary>
/// User interface controls relating to image masks.
/// </summary>
namespace HoloGen.UI.Mask
{
}

/// <summary>
/// User interface controls relating to image masks.
/// </summary>
namespace HoloGen.UI.Mask.Drawer
{
}

/// <summary>
/// User interface controls relating to image masks.
/// </summary>
namespace HoloGen.UI.Mask.Model
{
}

/// <summary>
/// Serialises and deserialises application data structures in a Json format.
/// </summary>
namespace HoloGen.Serial
{
}

/// <summary>
/// User interface control for viewing a 2D <see cref="ComplexImage"/>.
/// </summary>
namespace HoloGen.UI.Image
{
}

/// <summary>
/// User interface control for viewing a 2D <see cref="ComplexImage"/>.
/// </summary>
namespace HoloGen.UI.Image.Viewer
{
}

/// <summary>
/// Framework for viewing an options hierarchy in a hamburger style.
/// </summary>
namespace HoloGen.UI.Hamburger
{
}

/// <summary>
/// Controls for viewing an options hierarchy in a hamburger style.
/// </summary>
namespace HoloGen.UI.Hamburger.Views
{
}

/// <summary>
/// ViewModels for viewing an options hierarchy in a hamburger style.
/// </summary>
namespace HoloGen.UI.Hamburger.ViewModels
{
}

/// <summary>
/// Framework for viewing process progress in a dynamically updating async graph control.
/// </summary>
namespace HoloGen.UI.Graph
{
}

/// <summary>
/// Controls for viewing process progress in a dynamically updating async graph control.
/// </summary>
namespace HoloGen.UI.Graph.Views
{
}

/// <summary>
/// ViewModels for viewing process progress in a dynamically updating async graph control.
/// </summary>
namespace HoloGen.UI.Graph.ViewModels
{
}

/// <summary>
/// Framework for viewing a <see cref="ComplexImage"/> in 3D.
/// </summary>
namespace HoloGen.UI.X3
{
}

/// <summary>
/// Framework for viewing a <see cref="ComplexImage"/> in 3D.
/// </summary>
namespace HoloGen.UI.X3.Viewer
{
}

/// <summary>
/// User interface elements for the HoloGen application.
/// </summary>
namespace HoloGen.UI
{
}

/// <summary>
/// Handles navigation within HoloGen frames.
/// </summary>
namespace HoloGen.UI.Navigation
{
}

/// <summary>
/// Process viewer.
/// </summary>
namespace HoloGen.UI.Process
{
}

/// <summary>
/// Process monitor.
/// </summary>
namespace HoloGen.UI.Process.Monitor
{
}

/// <summary>
/// Process monitor.
/// </summary>
namespace HoloGen.UI.Process.Monitor.ViewModels
{
}

/// <summary>
/// Process monitor.
/// </summary>
namespace HoloGen.UI.Process.Monitor.Views
{
}

/// <summary>
/// Base level HoloGen package.
/// </summary>
namespace HoloGen.UI
{
}

/// <summary>
/// Controls for the HoloGen application.
/// </summary>
namespace HoloGen.UI.Views
{
}

/// <summary>
/// ViewModels for the HoloGen application.
/// </summary>
namespace HoloGen.UI.ViewModels
{
}

/// <summary>
/// Common elements for the HoloGen application.
/// </summary>
namespace HoloGen.UI.Common
{
}

/// <summary>
/// Defines the options hierarchy tree used for application wide settings.
/// </summary>
namespace HoloGen.Settings
{
}

/// <summary>
/// Defines the options hierarchy tree used for application wide settings.
/// </summary>
namespace HoloGen.Settings.General
{
}

/// <summary>
/// Defines the options hierarchy tree used for application wide settings.
/// </summary>
namespace HoloGen.Settings.General.File
{
}

/// <summary>
/// Defines the HoloGen option hierarchy for controlling an image mask.
/// </summary>
namespace HoloGen.Image
{
}

/// <summary>
/// Defines the HoloGen option hierarchy for controlling a process.
/// </summary>
namespace HoloGen.Process.Options
{
}

/// <summary>
/// Process view and related utilities.
/// </summary>
namespace HoloGen.Process
{
}

/// <summary>
/// Process view and related utilities.
/// </summary>
namespace HoloGen.Process.Settings
{
}

/// <summary>
/// Hologram generation options.
/// </summary>
namespace HoloGen.Options
{
}

/// <summary>
/// Hologram generation target options.
/// </summary>
namespace HoloGen.Options.Target
{
}

/// <summary>
/// Hologram generation target options.
/// </summary>
namespace HoloGen.Options.Target.Region
{
}

/// <summary>
/// Hologram generation target options.
/// </summary>
namespace HoloGen.Options.Target.Region.Type
{
}

/// <summary>
/// Hologram generation projector options.
/// </summary>
namespace HoloGen.Options.Projector
{
}

/// <summary>
/// Hologram generation projector options.
/// </summary>
namespace HoloGen.Options.Projector.Illumination.Type
{
}

/// <summary>
/// Hologram generation projector options.
/// </summary>
namespace HoloGen.Options.Projector.Hologram
{
}

/// <summary>
/// Hologram generation output options.
/// </summary>
namespace HoloGen.Options.Output
{
}

/// <summary>
/// Hologram generation hardware options.
/// </summary>
namespace HoloGen.Options.Hardware
{
}

/// <summary>
/// Hologram generation hardware options.
/// </summary>
namespace HoloGen.Options.Hardware.Hardware
{
}

/// <summary>
/// Hologram generation hardware options.
/// </summary>
namespace HoloGen.Options.Hardware.Hardware.Type
{
}

/// <summary>
/// Hologram generation hardware options.
/// </summary>
namespace HoloGen.Options.Hardware.Details
{
}

/// <summary>
/// Algorithm related options.
/// </summary>
namespace HoloGen.Options.Algorithm
{
}

/// <summary>
/// Options for hologram generation termination.
/// </summary>
namespace HoloGen.Options.Algorithm.Termination
{
}

/// <summary>
/// Hologram generation target options.
/// </summary>
namespace HoloGen.Options.Algorithm.Target
{
}

/// <summary>
/// Hologram generation target options.
/// </summary>
namespace HoloGen.Options.Algorithm.Target.Type
{
}

/// <summary>
/// Hologram generation error metric options.
/// </summary>
namespace HoloGen.Options.Algorithm.ErrorCalculation
{
}

/// <summary>
/// Hologram generation error metric options.
/// </summary>
namespace HoloGen.Options.Algorithm.ErrorCalculation.Type
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Type.Variants
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Type.Variants.SA
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Type.Variants.OSPR
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Type.Variants.GS
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Type.Variants.DS
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Type.Variants.Common
{
}

/// <summary>
/// Algorithm type options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Type
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Variants
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Variants.SA
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Variants.OSPR
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Variants.GS
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Variants.DS
{
}

/// <summary>
/// Algorithm variant options.
/// </summary>
namespace HoloGen.Options.Algorithm.Algorithms.Variants.Common
{
}

/// <summary>
/// Defines the HoloGen option hierarchy for controlling an image mask.
/// </summary>
namespace HoloGen.Mask.Options
{
}

/// <summary>
/// Masked image and related utilities.
/// </summary>
namespace HoloGen.Mask
{
}

/// <summary>
/// Defines the image mask shape type selection option.
/// </summary>
namespace HoloGen.Mask.Options.ShapeType
{
}

/// <summary>
/// Defines the image mask mouse type selection option.
/// </summary>
namespace HoloGen.Mask.Options.MouseType
{
}

/// <summary>
/// Handles file input and output of serialised objects.
/// </summary>
namespace HoloGen.IO
{
}

/// <summary>
/// Handles converting to/from external formats
/// </summary>
namespace HoloGen.IO.Convertors
{
}

/// <summary>
/// Handles converting to/from external formats
/// </summary>
namespace HoloGen.IO.Convertors.Export
{
}

/// <summary>
/// Handles converting to/from external formats
/// </summary>
namespace HoloGen.IO.Convertors.Import
{
}

/// <summary>
/// Handles file input and output of serialised objects.
/// </summary>
namespace HoloGen.IO
{
}

/// <summary>
/// Handles converting to/from external formats
/// </summary>
namespace HoloGen.IO.Convertors
{
}

/// <summary>
/// Handles converting to/from external formats
/// </summary>
namespace HoloGen.IO.Convertors.Export
{
}

/// <summary>
/// Handles converting to/from external formats
/// </summary>
namespace HoloGen.IO.Convertors.Import
{
}

/// <summary>
/// Inport/output file type schemas.
/// </summary>
namespace HoloGen.IO.Schema
{
}

/// <summary>
/// Library wrapper for ported code from prior CMMPE projects.
/// </summary>
namespace HoloGen.Imports
{
}

/// <summary>
/// Defines the HoloGen option hierarchy for controlling a image view.
/// </summary>
namespace HoloGen.Image.Options
{
}

/// <summary>
/// Defines the HoloGen option hierarchy for controlling a image view.
/// </summary>
namespace HoloGen.Image.Options.Type
{
}

/// <summary>
/// Defines the options hierarchy tree used for application wide settings.
/// </summary>
namespace HoloGen.Settings
{
}

/// <summary>
/// Batch processing widgets and controls
/// </summary>
namespace HoloGen.UI.Batch
{
}

/// <summary>
/// Batch processing widgets and controls
/// </summary>
namespace HoloGen.UI.Batch.Views
{
}

/// <summary>
/// Batch processing widgets and controls
/// </summary>
namespace HoloGen.UI.Batch.ViewModels
{
}