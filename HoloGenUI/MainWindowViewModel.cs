﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using Dragablz;
using HoloGen.Batch.Options;
using HoloGen.Image;
using HoloGen.IO;
using HoloGen.Options;
using HoloGen.Settings;
using HoloGen.SLMControl.Options;
using HoloGen.UI.Menus;
using HoloGen.UI.Menus.ViewModels;
using HoloGen.UI.Utils;
using HoloGen.UI.ViewModels;
using HoloGen.Utils;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Windows;
using HoloGen.UI.Menus.Commands;

namespace HoloGen.UI
{
    /// <summary>
    /// ViewModel for the <see cref="MainWindow" />.
    /// </summary>
    public class MainWindowViewModel : ObservableObject, ITabHandler, IMenuWindow
    {
        private static bool _instanceAlreadyExists;

        private static ApplicationSettings _applicationSettings;

        private readonly IDialogCoordinator _dialogCoordinator;

        private MenuViewModel _menuViewModel;

        private NewBatchFile _newBatchFile;

        private NewSetupFile _newSetupFile;

        private OpenFileCommand _openFileCommand;

        private SaveFileCommand _saveFileCommand;

        private ITabViewModel _selected;

        public MainWindowViewModel(IInterTabClient interTabClient, object partition, IDialogCoordinator dialogCoordinator)
        {
            TabHandlerFramework.RegisterHandler(this);

            InterTabClient = interTabClient;
            Partition = partition;
            _dialogCoordinator = dialogCoordinator;

            if (!_instanceAlreadyExists)
            {
                _instanceAlreadyExists = true;

                var file = new FileInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + FileLocations.WelcomeFileName);

                TabViewModels.CollectionChanged += TabViewModels_CollectionChanged;

                if (file.Exists)
                {
                    TabViewModels.Add(new BrowserTabViewModel(file));
                }
                else
                {
                    LogFileHandle.Instance.Log(Time.Timestamp() + file.FullName);
                    throw new Exception(file.FullName);
                }

                Selected = TabViewModels[0];
            }
        }

        public Version Version => Assembly.GetEntryAssembly().GetName().Version;

        public string Title => string.Format(
            Resources.Properties.Resources.MainWindowViewModel_HologramGeneratorV0123PeterChristopher456,
            Version.Major,
            Version.Minor,
            Version.Build,
            Version.Revision,
            "\"",
            Selected != null ? Selected.Name : "",
            "\"");

        public NewSetupFile NewSetupFile => _newSetupFile ?? (_newSetupFile = new NewSetupFile(this));
        public NewBatchFile NewBatchFile => _newBatchFile ?? (_newBatchFile = new NewBatchFile(this));
        public OpenFileCommand OpenFileCommand => _openFileCommand ?? (_openFileCommand = new OpenFileCommand(this));
        public SaveFileCommand SaveFileCommand => _saveFileCommand ?? (_saveFileCommand = new SaveFileCommand(this));

        public IInterTabClient InterTabClient { get; }

        public object Partition { get; }
        public object DialogCoordinator => _dialogCoordinator;

        public ObservableCollection<ITabViewModel> TabViewModels { get; } = new ObservableCollection<ITabViewModel>();
        public MenuViewModel MenuViewModel => _menuViewModel ?? (_menuViewModel = new MenuViewModel(this, ApplicationSettings));

        public ItemActionCallback ClosingTabItemHandler => ClosingTabItemHandlerImpl;

        public void HideMenu()
        {
            // Touch the seleced tab to close the menu
            OnPropertyChanged($"Selected");
        }

        public ApplicationSettings ApplicationSettings
        {
            set
            {
                _applicationSettings = value;
                OnPropertyChanged();
            }
            get => _applicationSettings ?? LoadApplicationSettings();
        }

        public ITabViewModel Selected
        {
            get => _selected;
            set
            {
                SetProperty(ref _selected, value);
                OnPropertyChanged(nameof(Title));
            }
        }

        public void AddTab(OptionsRoot data, FileInfo fileLocation, bool selectNewTab = true)
        {
            ITabViewModel newTab = new SetupTabViewModel(fileLocation, data);
            TabViewModels.Add(newTab);
            if (selectNewTab) Selected = newTab;
        }

        public void AddTab(ComplexImage data, FileInfo fileLocation, bool selectNewTab = true, bool needsSaving = false)
        {
            ITabViewModel newTab = new HologramTabViewModel(fileLocation, data, needsSaving);
            TabViewModels.Add(newTab);
            if (selectNewTab) Selected = newTab;
        }

        public void AddTab(BatchDataObject data, FileInfo fileLocation, bool selectNewTab = true)
        {
            ITabViewModel newTab = new BatchTabViewModel(
                new BatchOptions(),
                FileUtils.GetUniqueName(FileUtils.LastFileDialogLocation.Directory, "Untitled", "hfbat"),
                data.ParentOptions,
                data.ChildOptions);
            TabViewModels.Add(newTab);
            if (selectNewTab)
                Selected = newTab;
        }

        public void AddTab(ITabViewModel newTab, bool selectNewTab = true)
        {
            TabViewModels.Add(newTab);
            if (selectNewTab) Selected = newTab;
        }

        public void NewHologramSetupFile()
        {
            ITabViewModel newTab = new SetupTabViewModel(
                new FileInfo(FileUtils.LastFileDialogLocation.Directory?.FullName + "\\New Setup.hfset"),
                new OptionsRoot());
            TabViewModels.Add(newTab);

            Selected = newTab;
        }

        public void NewSLMControlTab()
        {
            ITabViewModel newTab = new SLMControlTabViewModel(new SLMControlOptions());
            TabViewModels.Add(newTab);

            Selected = newTab;
        }

        public void NewBatchProcessingFile()
        {
            ITabViewModel newTab = new BatchTabViewModel(
                new BatchOptions(),
                new FileInfo(FileUtils.LastFileDialogLocation.Directory?.FullName + "\\New Batch.hfbat"),
                new OptionsRoot());
            TabViewModels.Add(newTab);

            Selected = newTab;
        }

        public void NewBrowserTab(FileInfo pdf)
        {
            ITabViewModel newTab = new BrowserTabViewModel(pdf);
            TabViewModels.Add(newTab);

            Selected = newTab;
        }

        public void RemoveTab(ITabViewModel oldTab)
        {
            TabViewModels.Remove(oldTab);
        }

        private void TabViewModels_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                foreach (var str in e.OldItems)
                    ((ITabViewModel) str).PropertyChanged -= Tab_PropertyChanged;

            if (e.NewItems != null)
                foreach (var str in e.NewItems)
                    ((ITabViewModel) str).PropertyChanged += Tab_PropertyChanged;
        }

        private void Tab_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FileLocation" || e.PropertyName == "Name") OnPropertyChanged(nameof(Title));
        }

        ~MainWindowViewModel()
        {
            SaveApplicationSettings();
            TabHandlerFramework.DeregisterHandler(this);
        }

        /// <summary>
        /// Callback to handle tab closing.
        /// </summary>
        private static void ClosingTabItemHandlerImpl(ItemActionCallbackArgs<TabablzControl> args)
        {
            if (!(args.DragablzItem.DataContext is ITabViewModel tab)) return;

            if (tab is ICanSave save && save.NeedsSaving &&
                _applicationSettings.GeneralSettingsPage.WarningSettingsFolder.ShowCloseUnsavedFileWarning.Value)
            {
                var cont = ((MainWindow) Application.Current.MainWindow).ShowModalMessageExternal(
                    Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Warning,
                    Resources.Properties.Resources.MainWindowViewModel_ClosingTabItemHandlerImpl_ClosingTheFileWillLoseAnyChangesMadeWouldYouLikeToContinue,
                    MessageDialogStyle.AffirmativeAndNegative);
                if (cont == MessageDialogResult.Negative) args.Cancel();
            }
        }

        private static ApplicationSettings LoadApplicationSettings()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HoloGen";

            try
            {
                var dir = new DirectoryInfo(path);
                if (!dir.Exists) dir.Create();

                path = path + "\\ApplicationSettings" + FileTypes.SettingsFileExtension;

                var file = new FileInfo(path);

                if (file.Exists)
                {
                    var res = new JsonSettingsFileLoader().Load(file);
                    var error = res.Item1;
                    var loaded = res.Item2;
                    if (error != null && error == "")
                    {
                        _applicationSettings = loaded;
                        return _applicationSettings;
                    }
                }
            }
            catch (Exception)
            {
                // Run silently and don't bother with error reporting for application settings
            }

            _applicationSettings = new ApplicationSettings();
            return _applicationSettings;
        }

        private static void SaveApplicationSettings()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HoloGen";
            try
            {
                var dir = new DirectoryInfo(path);
                if (!dir.Exists) dir.Create();

                path = path + "\\ApplicationSettings" + FileTypes.SettingsFileExtension;

                var file = new FileInfo(path);

                new JsonSettingsFileSaver().Save(file, _applicationSettings);
            }
            catch (Exception)
            {
                // Run silently and don't bother with error reporting for application settings
            }
        }
    }
}