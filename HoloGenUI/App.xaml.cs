﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Utils;
using MahApps.Metro;
using System;
using System.Globalization;
using System.Threading;
using System.Windows;

namespace HoloGen.UI
{
    /// <summary>
    /// Top level application informatiuon and WPF setup.
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // add custom accent and theme resource dictionaries to the ThemeManager
            // you should replace MahAppsMetroThemesSample with your application name
            // and correct place where your custom accent lives
            ThemeManager.AddAccent("CreatorsThesis", new Uri("pack://application:,,,/HoloGenUIUtils;component/Themes/CreatorsThesis.xaml"));

            // get the current app style (theme and accent) from the application
            var theme = ThemeManager.DetectAppStyle(Current);

            // now change app style to the custom accent and current theme
            ThemeManager.ChangeAppStyle(Current,
                ThemeManager.GetAccent("CreatorsThesis"),
                theme.Item1);

            LogFileHandle.Instance.Log(Time.Timestamp() + e.Args);
            // Process the command line args
            foreach (var arg in e.Args)
            {
                LogFileHandle.Instance.Log(Time.Timestamp() + arg);
                ProcessArg(arg);
            }

            // Process the click once args
            if (AppDomain.CurrentDomain.SetupInformation.ActivationArguments != null &&
                AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData != null)
                foreach (var arg in AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData)
                {
                    LogFileHandle.Instance.Log(Time.Timestamp() + arg);
                    ProcessArg(arg);
                }

            base.OnStartup(e);
        }

        private void ProcessArg(string arg)
        {
            if (arg.Equals("fr", StringComparison.InvariantCultureIgnoreCase) ||
                arg.Equals("-fr", StringComparison.InvariantCultureIgnoreCase) ||
                arg.Equals("/fr", StringComparison.InvariantCultureIgnoreCase))
            {
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("fr-FR");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr");
            }
            else if (arg.Equals("en", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("-en", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("/en", StringComparison.InvariantCultureIgnoreCase))
            {
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-UK");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            }
            else if (arg.Equals("es", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("-es", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("/es", StringComparison.InvariantCultureIgnoreCase))
            {
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("es-ES");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("es");
            }
            else if (arg.Equals("de", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("-de", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("/de", StringComparison.InvariantCultureIgnoreCase))
            {
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("de-DE");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("de");
            }
            else if (arg.Equals("zh", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("-zh", StringComparison.InvariantCultureIgnoreCase) ||
                     arg.Equals("/zh", StringComparison.InvariantCultureIgnoreCase))
            {
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("zh-Hans");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("zh");
            }
        }
    }
}