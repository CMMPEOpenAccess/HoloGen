﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Batch.Options;
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.IO;
using HoloGen.Options;
using HoloGen.UI.Batch.ViewModels;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Windows.Input;
using System.Linq;
using System.Threading.Tasks;
using HoloGen.IO.Convertors.Export;
using System.Collections.Generic;
using HoloGen.UI.Utils.Commands;

namespace HoloGen.UI.ViewModels
{
    /// <summary>
    /// HoloGen tab viewmodel
    /// </summary>
    public class BatchTabViewModel : AbstractTabViewModel<OptionsRoot>, ICanSave, ICanExportExcel
    {
        private FileInfo _fileLocation;

        private string _numErrors = "";
        private ICommand _resetCommand;

        private ICommand _runCommand;

        private string _runToolTip = "";

        public BatchTabViewModel(BatchOptions batchOptions, FileInfo fileLocation, OptionsRoot baseOptions)
            : base(baseOptions)
        {
            FileLocation = fileLocation;
            NeedsSaving = FileLocation != null && FileLocation.Exists;
            ViewModel = new BatchViewModel(batchOptions, baseOptions);
            baseOptions.Changed += Data_Changed;
            ViewModel.ChildOptions.CollectionChanged += ChildOptions_CollectionChanged;
            ViewModel.Columns.CollectionChanged += Data_Changed;
            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
            UpdateErrors();
        }

        public BatchTabViewModel(BatchOptions batchOptions, FileInfo fileLocation, OptionsRoot baseOptions, List<OptionsRoot> childOptions)
            : this(batchOptions, fileLocation, baseOptions)
        {
            ViewModel.AddRows(childOptions);
        }

        public ICommand RunCommand => _runCommand ?? (_runCommand = new SimpleCommand {CanExecuteDelegate = x => true, ExecuteDelegate = x => RunCommandFunc()});

        public ICommand ResetCommand => _resetCommand ?? (_resetCommand = new SimpleCommand {CanExecuteDelegate = x => true, ExecuteDelegate = x => ResetCommandFunc()});

        public string NumErrors
        {
            get => _numErrors;
            set => SetProperty(ref _numErrors, value);
        }

        public string RunToolTip
        {
            get => _runToolTip;
            set => SetProperty(ref _runToolTip, value);
        }

        public BatchViewModel ViewModel { get; }

        public string ExcelFileFilter => FileTypes.ExcelFileFilter;

        public bool CanCurrentlyExport => true;

        public string ExportExcel(FileInfo file)
        {
            var task = Task.Run(() =>
            {
                var exporter = new HierarchyExporter();
                var terror = exporter.Export(file, ViewModel.ChildOptions.ToList());
                return new Tuple<string, HierarchyExporter>(terror, exporter);
            });

            task.Wait();

            var res = task.Result;
            return res.Item1;
        }

        public override string Name => (FileLocation?.Name ?? "Empty Tab") + (NeedsSaving ? "*" : "");

        public string FileFilter => FileTypes.BatchFileFilter;

        public FileInfo FileLocation
        {
            get => _fileLocation;
            set => SetProperty(ref _fileLocation, value);
        }

        public bool NeedsSaving { get; private set; }

        public override string SaveToFile(FileInfo file)
        {
            base.SaveToFile(file);

            FileLocation = file;
            new JsonBatchFileSaver().Save(file,
                new BatchDataObject(
                    ViewModel.ParentOptions as OptionsRoot,
                    ViewModel.ChildOptions.Cast<OptionsRoot>().ToList()));

            NeedsSaving = false;
            OnPropertyChanged(nameof(Name));
            OnPropertyChanged(nameof(NeedsSaving));
            OnPropertyChanged(nameof(Name));
            OnPropertyChanged(nameof(Name));

            return ""; // err;
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Valid")
                UpdateErrors();
        }

        private void ChildOptions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Data_Changed(sender, e);
            if (e.OldItems != null)
                foreach (var str in e.OldItems)
                {
                    if (str is HierarchyRoot str2) str2.Changed -= Data_Changed;
                }

            if (e.NewItems != null)
                foreach (var str in e.NewItems)
                {
                    if (str is HierarchyRoot str2) str2.Changed += Data_Changed;
                }
        }

        private void Data_Changed(object sender, EventArgs e)
        {
            if (!NeedsSaving)
            {
                NeedsSaving = true;
                OnPropertyChanged(nameof(Name));
                OnPropertyChanged(nameof(NeedsSaving));
            }
        }

        private void UpdateErrors()
        {
            if (!ViewModel.Valid)
            {
                NumErrors = (ViewModel.ParentOptions.Error.Count + ViewModel.ChildOptions.Sum(child => child.Error.Count)).ToString();
                RunToolTip = string.Format(Resources.Properties.Resources.SetupViewModel_Options_PropertyChanged_SorryYouCanTRunAtTheMomentYouStillHave0Errors, NumErrors);
            }
            else
            {
                NumErrors = "";
                RunToolTip = Resources.Properties.Resources.SetupViewModel_Options_PropertyChanged_AreYouReadyToRockNRoll;
            }
        }

        private void ResetCommandFunc()
        {
            ViewModel.Reset();
        }

        private void RunCommandFunc()
        {
            var mainWindowViewModel = TabHandlerFramework.GetActiveHandler();

            var clonedOptions = ViewModel.ChildOptions.ToList().ConvertAll(option => (OptionsRoot) option.Clone());

            mainWindowViewModel.AddTab(new ProcessTabViewModel(clonedOptions));
        }
    }
}