﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Image;
using HoloGen.IO;
using HoloGen.IO.Convertors.Export;
using HoloGen.Mask.Options;
using HoloGen.Options;
using HoloGen.UI.Hamburger.ViewModels;
using HoloGen.UI.Mask;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using HoloGen.Image.Options.Type;
using HoloGen.UI.Utils.Commands;

namespace HoloGen.UI.ViewModels
{
    /// <summary>
    /// HoloGen tab viewmodel
    /// </summary>
    public class HologramTabViewModel : AbstractTabViewModel<ComplexImage>, ICanSave, ICanExportBitmap, ICanExportMat
    {
        private FileInfo _fileLocation;

        private ICommand _metadataCommand;

        private MaskedImageOptions _options;

        private bool _plotterVisibility;

        private SearchBoxViewModel _searchBoxViewModel;

        public HologramTabViewModel(FileInfo fileLocation, ComplexImage complexImage, bool needsSaving = false)
            : base(complexImage)
        {
            FileLocation = fileLocation;
            SavedHereBefore = !needsSaving;
            MaskViewModel = new MaskViewModel(complexImage);

            NeedsSaving = needsSaving;

            SearchBoxViewModel = new SearchBoxViewModel(Options);

            MaskViewModel.ShapeCollectionChanged += MaskViewModel_ShapeCollectionChanged;

            Options.ImageViewFolder.ImageViewOption.PropertyChanged += ImageViewOption_PropertyChanged;
            Options.ImageViewFolder.ColorSchemeOption.PropertyChanged += ColorSchemeOption_PropertyChanged;
            Options.ImageViewFolder.DimensionOption.PropertyChanged += DimensionOption_PropertyChanged;
            Options.ImageViewFolder.ScaleLocationOption.PropertyChanged += ScaleLocationOption_PropertyChanged;
            Options.ImageViewFolder.TransformTypeOption.PropertyChanged += TransformTypeOption_PropertyChanged;

            UpdateImage();
        }

        private void MaskViewModel_ShapeCollectionChanged(object sender, EventArgs e)
        {
            if (!NeedsSaving)
            {
                NeedsSaving = true;
                OnPropertyChanged(nameof(Name));
                OnPropertyChanged(nameof(NeedsSaving));
            }
        }

        public MaskViewModel MaskViewModel { get; }

        public SearchBoxViewModel SearchBoxViewModel
        {
            get => _searchBoxViewModel;
            set => SetProperty(ref _searchBoxViewModel, value);
        }

        public ICommand MetadataCommand => _metadataCommand ?? (_metadataCommand = new SimpleCommand {CanExecuteDelegate = x => MetadataEnabled, ExecuteDelegate = x => MetadataCommandFunc()});

        public bool MetadataEnabled =>
            MaskViewModel.ComplexImage.OptionsMetadata != null &&
            MaskViewModel.ComplexImage.OptionsMetadataVersion != null &&
            MaskViewModel.ComplexImage.OptionsMetadataVersion.CurrentVersion <= new OptionsRootVersion().CurrentVersion &&
            MaskViewModel.ComplexImage.OptionsMetadataVersion.CurrentVersion >= new OptionsRootVersion().MinimumCompatibleVersion;

        public MaskedImageOptions Options => _options ?? (_options = new MaskedImageOptions());

        public bool PlotterVisibility
        {
            get => _plotterVisibility;
            set
            {
                _plotterVisibility = value;
                OnPropertyChanged(nameof(PlotterVisibility));
                OnPropertyChanged(nameof(ViewerVisibility));
            }
        }

        public bool ViewerVisibility
        {
            get => !_plotterVisibility;
            set => PlotterVisibility = !value;
        }

        public string BitmapFileFilter =>
            FileTypes.BmpFileFilter + "|" +
            FileTypes.GifFileFilter + "|" +
            FileTypes.JpgFileFilter + "|" +
            FileTypes.PngFileFilter + "|" +
            FileTypes.TffFileFilter;

        public bool CanCurrentlyExport => true;

        public string ExportBitmap(FileInfo file, ImageFormat format, bool includeScale)
        {
            Bitmap image;

            if (includeScale)
                image = MaskViewModel.ImageView.Image;
            else
                image = MaskViewModel.ImageCache.GetAppropriateImage(
                    Options.ImageViewFolder.TransformTypeOption.Value.TransformType,
                    Options.ImageViewFolder.ImageViewOption.Value.ImageViewType,
                    Options.ImageViewFolder.ColorSchemeOption.Value.ColorScheme,
                    ImageScaleType.None).Image;

            image.Save(file.FullName, format);
            return "";
        }

        public string MatFileFilter => FileTypes.MatlabFileFilter;

        public string ExportMat(FileInfo file)
        {
            var task = Task.Run(() =>
            {
                var timporter = new MatExporter();
                var terror = timporter.Export(file, MaskViewModel.ComplexImage);
                return new Tuple<string, MatExporter>(terror, timporter);
            });

            task.Wait();

            var res = task.Result;
            return res.Item1;
        }

        public override string Name => (FileLocation?.Name ?? "Empty Tab") + (NeedsSaving ? "*" : "");

        public string FileFilter => FileTypes.ImageFileFilter;

        public FileInfo FileLocation
        {
            get => _fileLocation;
            set => SetProperty(ref _fileLocation, value);
        }

        public bool NeedsSaving { get; private set; }

        public override string SaveToFile(FileInfo file)
        {
            base.SaveToFile(file);

            var err = new JsonImageFileSaver().Save(file, MaskViewModel.ComplexImage);
            FileLocation = file;

            NeedsSaving = false;
            OnPropertyChanged(nameof(Name));
            OnPropertyChanged(nameof(NeedsSaving));

            return err;
        }

        private void DimensionOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex") PlotterVisibility = Options.ImageViewFolder.DimensionOption.Value.GetType() == typeof(Dimension3D);
        }

        private void ColorSchemeOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex") UpdateImage();
        }

        private void ImageViewOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex") UpdateImage();
        }

        private void ScaleLocationOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex") UpdateImage();
        }

        private void TransformTypeOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedIndex") UpdateImage();
        }

        private void UpdateImage()
        {
            MaskViewModel.ImageView = MaskViewModel.ImageCache.GetAppropriateImage(
                Options.ImageViewFolder.TransformTypeOption.Value.TransformType,
                Options.ImageViewFolder.ImageViewOption.Value.ImageViewType,
                Options.ImageViewFolder.ColorSchemeOption.Value.ColorScheme,
                Options.ImageViewFolder.ScaleLocationOption.Value.ImageScaleType);
            MaskViewModel.Image = BitmapUtils.ToBitmapImage(MaskViewModel.ImageView.Image);
        }

        private void MetadataCommandFunc()
        {
            if (MetadataEnabled)
            {
                MaskViewModel.ComplexImage.OptionsMetadata.RecursivelySetEnabled(false);
                TabHandlerFramework.GetActiveHandler()?.AddTab(MaskViewModel.ComplexImage.OptionsMetadata, null);
            }
        }
    }
}