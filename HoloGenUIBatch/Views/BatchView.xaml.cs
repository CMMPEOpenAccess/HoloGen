﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.UI.Batch.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Linq;
using HoloGen.Hierarchy.OptionTypes;
using HoloGen.UI.Batch.Columns;
using System.Collections.Specialized;
using System.Diagnostics;
using HoloGen.Utils;

namespace HoloGen.UI.Batch.Views
{
    /// <summary>
    /// Control for viewing <see cref="HierarchyRoot" /> elements in a Batch view style.
    /// Warning: The WPF DataGrid setup is *shit*. Sorry. The view model here is ok-ish, but the interface logic
    /// is hideous. That's largely on microsoft, tbh, but I could improve it if I could be arsed.
    /// Issue #30
    /// </summary>
    public sealed partial class BatchView : UserControl, INotifyPropertyChanged, IDataErrorInfo
    {
        private readonly Dictionary<IOption, DataGridColumn> _columnOptionMap = new Dictionary<IOption, DataGridColumn>();
        private readonly Dictionary<DataGridColumn, IOption> _optionColumnMap = new Dictionary<DataGridColumn, IOption>();

        /// <summary>
        /// List of all selected columns
        /// </summary>
        private readonly List<DataGridColumn> _selectedColumns = new List<DataGridColumn>();

        private ContentControl _bottomPane;

        public BatchView()
        {
            InitializeComponent();

            OptionList.Items.SortDescriptions.Add(
                new SortDescription("LongName",
                    ListSortDirection.Ascending));

            Loaded += BatchView_Loaded;
        }

        public ContentControl BottomPane
        {
            get => _bottomPane;
            set
            {
                _bottomPane = value;
                OnPropertyChanged();
            }
        }

        public BatchViewModel BatchViewModel { get; private set; }

        public bool OpenBaseOptionsEnabled =>
            BatchViewModel?.ParentOptions != null;

        public bool AddOptionEnabled =>
            BatchViewModel?.ParentOptions != null && OptionList.SelectedItems.Count > 0;

        public bool RemoveOptionEnabled =>
            BatchViewModel?.ParentOptions != null && _selectedColumns.Count > 0;

        public bool AddRowEnabled =>
            BatchViewModel?.ParentOptions != null;

        public bool RemoveRowEnabled =>
            BatchViewModel?.ParentOptions != null && DataGridInstance.SelectedItems.Count > 0 && _selectedColumns.Count <= 0;

        public string Error => "error2";

        public string this[string columnName] => "error2";

        /// <summary>
        /// Occurs when property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void BatchView_Loaded(object sender, RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                // ReSharper disable once UsePatternMatching
                var model = DataContext as BatchViewModel;
                if ((model != null) & (BatchViewModel == null))
                {
                    BatchViewModel = model;
                    BatchViewModel.Columns.CollectionChanged += Columns_CollectionChanged;
                    BatchViewModel.AddNewRows();
                    ClearGridSelection();

                    OnPropertyChanged($"OpenBaseOptionsEnabled");
                    OnPropertyChanged($"AddOptionEnabled");
                    OnPropertyChanged($"RemoveOptionEnabled");
                    OnPropertyChanged($"AddRowEnabled");
                    OnPropertyChanged($"RemoveRowEnabled");
                }
            }
        }

        private void Columns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                foreach (var str in e.OldItems)
                {
                    var str2 = str as IOption;
                    RemoveColumn(str2);
                }

            if (e.NewItems != null)
                foreach (var str in e.NewItems)
                {
                    var str2 = str as IOption;
                    AddColumn(str2);
                }
        }

        private void AddColumn(IOption parentOption)
        {
            DataGridColumn column;
            // Fuck wpf and its stupidly limited data binding
            if (parentOption is IntegerOption option)
                column = new IntegerColumnFactory(option, DataGridInstance.Resources).Create();
            else if (parentOption is DoubleOption doubleOption)
                column = new DoubleColumnFactory(doubleOption, DataGridInstance.Resources).Create();
            else if (parentOption is TextOption textOption)
                column = new TextColumnFactory(textOption, DataGridInstance.Resources).Create();
            else if (parentOption is BooleanOption booleanOption)
                column = new BooleanColumnFactory(booleanOption, DataGridInstance.Resources).Create();
            else if (parentOption is PathOption pathOption)
                column = new PathColumnFactory(pathOption, DataGridInstance.Resources).Create();
            else if (parentOption is ISelectOption selectOption)
                column = new SelectColumnFactory(selectOption, DataGridInstance.Resources).Create();
            else
                column = new DefaultColumnFactory(parentOption, DataGridInstance.Resources).Create();
            DataGridInstance.Columns.Add(column);
            _columnOptionMap.Add(parentOption, column);
            _optionColumnMap.Add(column, parentOption);
            BatchViewModel.AddableParentOptions.Remove(parentOption);

            _selectedColumns.Add(column);
            foreach (var item in DataGridInstance.Items)
                if (!DataGridInstance.SelectedCells.Contains(new DataGridCellInfo(item, column)))
                    DataGridInstance.SelectedCells.Add(new DataGridCellInfo(item, column));
        }

        private void RemoveColumn(IOption parentOption)
        {
            if (_columnOptionMap.ContainsKey(parentOption))
            {
                var column = _columnOptionMap[parentOption];
                DataGridInstance.Columns.Remove(column);
                _columnOptionMap.Remove(parentOption);
                _optionColumnMap.Remove(column);
                BatchViewModel.AddableParentOptions.Add(parentOption);
                OptionList.SelectedItems.Add(parentOption);
            }
        }

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;

            changed?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OptionList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnPropertyChanged($"OpenBaseOptionsEnabled");
            OnPropertyChanged($"AddOptionEnabled");
            OnPropertyChanged($"RemoveOptionEnabled");
            OnPropertyChanged($"AddRowEnabled");
            OnPropertyChanged($"RemoveRowEnabled");
        }

        private void OptionList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedOptions = new List<IOption>();
            foreach (var item in OptionList.SelectedItems) selectedOptions.Add((IOption) item);
            foreach (var item in selectedOptions) BatchViewModel.Columns.Add(item);
        }

        private void OpenBaseOptions_Click(object sender, RoutedEventArgs e)
        {
            BatchViewModel.ShowingParentOptions = true;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            BatchViewModel.ParentOptions.Reset();
        }

        private void FinishedButton_Click(object sender, RoutedEventArgs e)
        {
            BatchViewModel.ShowingParentOptions = false;
        }

        private void AddOption_Click(object sender, RoutedEventArgs e)
        {
            ClearGridSelection();
            var selectedOptions = new List<IOption>();
            foreach (var item in OptionList.SelectedItems) selectedOptions.Add((IOption) item);
            foreach (var item in selectedOptions) BatchViewModel.Columns.Add(item);
        }

        private void RemoveOption_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in _selectedColumns)
                if (_optionColumnMap.ContainsKey(item))
                    BatchViewModel.Columns.Remove(_optionColumnMap[item]);
            _selectedColumns.Clear();
        }

        private void AddRow_Click(object sender, RoutedEventArgs e)
        {
            BatchViewModel.AddNewRows(Math.Max(DataGridInstance.SelectedItems.Count, 1));
        }

        private void RemoveRow_Click(object sender, RoutedEventArgs e)
        {
            var itemsToRemove = new List<HierarchyRoot>();
            foreach (var item in DataGridInstance.SelectedItems) itemsToRemove.Add((HierarchyRoot) item);
            if (itemsToRemove.Count > 0) BatchViewModel.Remove(itemsToRemove);
        }

        /// <summary>
        /// Somewhat hacky way to enable column selection in wpf
        /// </summary>
        private void DataGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var dep = (DependencyObject) e.OriginalSource;

            // Iteratively traverse the visual tree to find the columnheader or cell clicked on
            while (dep != null && !(dep is DataGridCell) && !(dep is DataGridColumnHeader)) dep = VisualTreeHelper.GetParent(dep);

            if (dep == null) return;

            // If its a column header, change the selection mode to column and select the column
            if (dep is DataGridColumnHeader header)
            {
                var column = header.Column;
                DataGridInstance.SelectionUnit = DataGridSelectionUnit.Cell;
                if (!Keyboard.IsKeyDown(Key.LeftCtrl))
                {
                    foreach (var col in _selectedColumns)
                        if (col != column)
                            col.SortDirection = null;
                    DataGridInstance.SelectedCells.Clear();
                    _selectedColumns.Clear();
                }

                if (!_selectedColumns.Contains(column))
                {
                    _selectedColumns.Add(column);

                    foreach (var item in DataGridInstance.Items)
                        if (!DataGridInstance.SelectedCells.Contains(new DataGridCellInfo(item, column)))
                            DataGridInstance.SelectedCells.Add(new DataGridCellInfo(item, column));
                }

                foreach (var col in _selectedColumns)
                    if (col != column)
                        column.SortDirection = null;

                column.SortDirection = column.SortDirection != ListSortDirection.Ascending ? ListSortDirection.Ascending : ListSortDirection.Descending;
            }

            if (dep is DataGridCell cell)
            {
                // First element, switch to row select
                _selectedColumns.Clear();
                if (cell.Column.DisplayIndex == 0)
                {
                    if (DataGridInstance.SelectionUnit != DataGridSelectionUnit.FullRow)
                        DataGridInstance.SelectionUnit = DataGridSelectionUnit.FullRow;
                }
                else
                {
                    if (DataGridInstance.SelectionUnit != DataGridSelectionUnit.Cell)
                        DataGridInstance.SelectionUnit = DataGridSelectionUnit.Cell;
                }
            }
        }

        private void DataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            OnPropertyChanged($"OpenBaseOptionsEnabled");
            OnPropertyChanged($"AddOptionEnabled");
            OnPropertyChanged($"RemoveOptionEnabled");
            OnPropertyChanged($"AddRowEnabled");
            OnPropertyChanged($"RemoveRowEnabled");
        }

        private void DataGridInstance_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            // Hacky way to stop editting of disabled options
            var dataGrid = sender as DataGrid;
            var optionsRoot = dataGrid?.CurrentCell.Item as HierarchyRoot;
            if (optionsRoot == null)
                return;
            var path = e.Column.GetValue(BatchViewModel.BasePathProperty) as string;
            if (path == null)
                return;
            var option = BindingEvaluator.GetPropertyObject(optionsRoot, new PropertyPath(path)) as IOption;
            Debug.Assert(option != null, nameof(option) + " != null");
            if (!option.Enabled)
                e.Cancel = true;
        }

        private void ClearGridSelection()
        {
            _selectedColumns.Clear();
            if (DataGridInstance.SelectionUnit != DataGridSelectionUnit.FullRow)
                DataGridInstance.SelectedCells.Clear();

            if (DataGridInstance.SelectionMode != DataGridSelectionMode.Single) //if the Extended mode
                DataGridInstance.SelectedItems.Clear();
            else
                DataGridInstance.SelectedItem = null;
        }

        private void CanPaste(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void CanAddNew(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void AddNew(object sender, ExecutedRoutedEventArgs e)
        {
            BatchViewModel.AddNewRows(Math.Max(DataGridInstance.SelectedItems.Count, 1));
        }

        private void OnPaste(object sender, ExecutedRoutedEventArgs e)
        {
            // Original Credits: Reto Reviso
            // First row is the headers containing the paths

            // Desired behaviour
            // - When in row selection mode, insert the copied cells beneath the currently selected row
            // - When in cell selection mode, overwrite the existing cells, adding new rows as required


            // 2-dim array containing clipboard data
            var clipboardData =
                ((string) Clipboard.GetData(DataFormats.Text)).Split('\n')
                .Select(row =>
                    row.Split('\t')
                        .Select(cell =>
                            cell.Length > 0 && cell[cell.Length - 1] == '\r' ? cell.Substring(0, cell.Length - 1) : cell).ToArray())
                .Where(a => a.Any(b => b.Length > 0))
                .ToArray();

            // the index of the first DataGridRow          
            var startRow = DataGridInstance.ItemContainerGenerator.IndexFromContainer(
                (DataGridRow) DataGridInstance.ItemContainerGenerator.ContainerFromItem
                    (DataGridInstance.SelectedCells[0].Item));

            // Get the number of rows we want to add and add any more rows required
            var targetRowCount = clipboardData.Length - 1;
            if (DataGridInstance.SelectionUnit != DataGridSelectionUnit.FullRow)
            {
                if (startRow + targetRowCount > DataGridInstance.Items.Count) BatchViewModel.AddNewRows(startRow + targetRowCount - DataGridInstance.Items.Count);
            }
            else
            {
                startRow++;
                BatchViewModel.AddNewRows(targetRowCount, startRow);
            }

            // the destination rows 
            //  (from startRow to either end or length of clipboard rows)
            var rows =
                Enumerable.Range(startRow, targetRowCount)
                    .Select(rowIndex =>
                        DataGridInstance.ItemContainerGenerator.ContainerFromIndex(rowIndex) as DataGridRow)
                    .Where(a => a != null).ToArray();

            // the list of destination columns 
            var columns =
                DataGridInstance.Columns.Where(
                        column => clipboardData[0].Any(
                            header => header == (string) column.GetValue(BatchViewModel.BasePathProperty)))
                    .ToArray();

            for (var rowIndex = 0; rowIndex < rows.Length; rowIndex++)
            {
                var rowContent = clipboardData[rowIndex % clipboardData.Length + 1];
                for (var colIndex = 0; colIndex < columns.Length; colIndex++)
                {
                    var cellContent = colIndex >= rowContent.Length ? "" : rowContent[colIndex];
                    var column = DataGridInstance.Columns.First(col => clipboardData[0][colIndex] == (string) col.GetValue(BatchViewModel.BasePathProperty));
                    if (cellContent != "")
                        column.OnPastingCellClipboardContent(rows[rowIndex].Item, cellContent);
                }
            }
        }
    }
}