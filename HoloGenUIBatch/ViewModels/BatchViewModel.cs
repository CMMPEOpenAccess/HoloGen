﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Utils;
using System;
using System.Collections.ObjectModel;
using HoloGen.Options;
using HoloGen.Hierarchy.OptionTypes;
using System.Linq;
using HoloGen.Utils.ExtensionMethods;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using HoloGen.Batch.Options;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using HoloGen.UI.Hamburger.ViewModels;

namespace HoloGen.UI.Batch.ViewModels
{
    /// <summary>
    /// Batch editor view model
    /// </summary>
    public class BatchViewModel : ObservableObject, IDataErrorInfo
    {
        public static readonly DependencyProperty BasePathProperty = DependencyProperty.RegisterAttached(
            "BasePath", typeof(string), typeof(DataGridColumn), new FrameworkPropertyMetadata(null));

        private ObservableCollection<IOption> _addableParentOptions;

        private BatchOptions _batchOptions;

        private ObservableCollection<HierarchyRoot> _childOptions = new ObservableCollection<HierarchyRoot>();

        private ObservableCollection<IOption> _columns = new ObservableCollection<IOption>();

        private string _name = "Batch Run";

        private HierarchyRoot _parentOptions;

        private ObservableCollection<IOption> _parentOptionsFlattened;

        private string _searchTerm = "";

        private bool _showingParentOptions;

        private int _maxId;

        public BatchViewModel(BatchOptions batchOptions, HierarchyRoot childSource)
        {
            BatchOptions = batchOptions;
            ParentOptions = childSource;
            ParentOptionsFlattened = ParentOptions.Flatten().Distinct().OrderBy(o => o.Name).ToList().ToObservableCollection();
            AddableParentOptions = ParentOptions.Flatten().Distinct().OrderBy(o => o.Name).ToList().ToObservableCollection();
            FilterAddableParentOptions();
            SearchedParentOptions.Source = AddableParentOptions;
            SearchedParentOptions.Filter += SearchedChildren_Filter;
            ParentOptions.Changed += Options_Changed;
            ChildOptions.CollectionChanged += ChildOptions_CollectionChanged;
            HViewModel = new HamburgerTabViewModel(ParentOptions);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public bool ShowingParentOptions
        {
            get => _showingParentOptions;
            set => SetProperty(ref _showingParentOptions, value);
        }

        public bool Valid => ParentOptions.Valid && ChildOptions.All(child => child.Valid);

        public HierarchyRoot ParentOptions
        {
            get => _parentOptions;
            set => SetProperty(ref _parentOptions, value);
        }

        public BatchOptions BatchOptions
        {
            get => _batchOptions;
            set => SetProperty(ref _batchOptions, value);
        }

        public ObservableCollection<IOption> ParentOptionsFlattened
        {
            get => _parentOptionsFlattened;
            set => SetProperty(ref _parentOptionsFlattened, value);
        }

        public ObservableCollection<IOption> AddableParentOptions
        {
            get => _addableParentOptions;
            set => SetProperty(ref _addableParentOptions, value);
        }

        public CollectionViewSource SearchedParentOptions { get; } = new CollectionViewSource();

        public string SearchTerm
        {
            get => _searchTerm;
            set
            {
                SetProperty(ref _searchTerm, string.IsNullOrEmpty(value) ? "" : value);
                foreach (var child in AddableParentOptions)
                    child.SearchTerm = _searchTerm;
                SearchedParentOptions.View.Refresh();
            }
        }

        public ObservableCollection<IOption> Columns
        {
            get => _columns;
            set => SetProperty(ref _columns, value);
        }

        public ObservableCollection<HierarchyRoot> ChildOptions
        {
            get => _childOptions;
            set => SetProperty(ref _childOptions, value);
        }

        public HamburgerTabViewModel HViewModel { get; }
        public string Error => "error";

        public string this[string columnName] => "error";

        private void ChildOptions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                foreach (var str in e.OldItems)
                {
                    var str2 = str as HierarchyRoot;
                    Debug.Assert(str2 != null, nameof(str2) + " != null");
                    str2.Changed -= Options_Changed;
                }

            if (e.NewItems != null)
                foreach (var str in e.NewItems)
                {
                    var str2 = str as HierarchyRoot;
                    Debug.Assert(str2 != null, nameof(str2) + " != null");
                    str2.Changed += Options_Changed;
                }
        }

        private void Options_Changed(object sender, EventArgs e)
        {
            OnPropertyChanged(nameof(Valid));
        }

        private void FilterAddableParentOptions()
        {
            // TODO: Dont assume that this is a Options Root
            // It is in every current use but can't be assumed in the wider case. 
            // Should be fixed automatically by fixing issue #23
            var options = ParentOptions as OptionsRoot;
            Debug.Assert(options != null);
            if (AddableParentOptions.Contains(options.OutputPage.RunFolder.RunFile)) AddableParentOptions.Remove(options.OutputPage.RunFolder.RunFile);
            if (AddableParentOptions.Contains(options.OutputPage.ErrorFolder.ErrorText)) AddableParentOptions.Remove(options.OutputPage.ErrorFolder.ErrorText);
        }

        protected void SearchedChildren_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as ICanSearch)?.HasResult ?? false;
        }

        public void AddNewRows(int num = 1, int insertAt = -1)
        {
            var options = new List<HierarchyRoot>();
            for (var i = 0; i < num; i++) options.Add(ParentOptions.Clone());

            AddRows(options, insertAt);
        }

        public void AddRows(IEnumerable<HierarchyRoot> options, int insertAt = -1)
        {
            var incr = 0;
            foreach (var c in options)
            {
                _maxId++;
                c.ID = _maxId;
                var f = c.Flatten().Distinct().OrderBy(o => o.Name).ToList().ToObservableCollection();
                CreateReferences(ParentOptionsFlattened, f);
                if (insertAt == -1)
                {
                    ChildOptions.Add(c);
                }
                else
                {
                    ChildOptions.Insert(insertAt + incr, c);
                    incr++;
                }

                c.RecursivelySetEnabled(true);
            }
        }

        public void Remove(HierarchyRoot child)
        {
            var index = ChildOptions.IndexOf(child);
            ChildOptions.RemoveAt(index);
        }

        public void Remove(IEnumerable<HierarchyRoot> children)
        {
            foreach (var child in children) Remove(child);
        }

        public void CreateReferences(ObservableCollection<IOption> p, ObservableCollection<IOption> c)
        {
            for (var i = 0; i < c.Count; i++)
                if (c[i] != null)
                    CreateReferences(p[i], c[i]);
        }

        /// <summary>
        /// Horrible way of cloning with references
        /// </summary>
        public void CreateReferences(IOption p, IOption c)
        {
            foreach (var prop in c.GetType().GetProperties())
                if (prop.Name == "Reference" &&
                    prop.CanWrite &&
                    prop.SetMethod != null &&
                    prop.SetMethod.IsPublic &&
                    prop.GetMethod != null &&
                    prop.GetMethod.IsPublic)
                    prop.SetValue(c, p);
        }

        public void Reset()
        {
            Columns.Remove(s => true); // Don't use Clear() as it doesn't notify correctly
            ChildOptions.Clear();
            ParentOptions.Reset();
            _maxId = 0;
            AddNewRows();
        }

        public static object GetBasePath(DependencyObject dependencyObject)
        {
            return dependencyObject.GetValue(BasePathProperty);
        }

        public static void SetBasePath(DependencyObject dependencyObject, object value)
        {
            dependencyObject.SetValue(BasePathProperty, value);
        }
    }
}