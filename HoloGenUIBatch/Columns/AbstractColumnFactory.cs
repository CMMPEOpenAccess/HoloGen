﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Hierarchy.OptionTypes;
using HoloGen.UI.Batch.Utils;
using HoloGen.UI.Batch.ViewModels;
using HoloGen.Utils;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace HoloGen.UI.Batch.Columns
{
    public abstract class AbstractColumnFactory<T>
        where T : IOption
    {
        protected ResourceDictionary GridResources;
        protected T ParentOption;

        protected AbstractColumnFactory(T parentOption, ResourceDictionary gridResources)
        {
            ParentOption = parentOption;
            GridResources = gridResources;
        }

        protected Binding SubBinding(string property)
        {
            return new Binding(ParentOption.BindingPath + ".Name")
            {
                Source = GridResources["proxy"]
            };
        }

        protected void CreateValueBinding(DataGridBoundColumn col)
        {
            col.Binding = new Binding(ParentOption.BindingPath + ".Value")
            {
                Mode = BindingMode.TwoWay,
                ValidatesOnDataErrors = true,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                NotifyOnValidationError = true
            };
        }

        protected void CreateEnabledBinding(DataGridColumn col)
        {
            var binding = new Binding(ParentOption.BindingPath + ".Enabled")
            {
                Converter = (IValueConverter) GridResources["InverseBooleanConverter"]
            };
            BindingOperations.SetBinding(col, DataGridColumn.IsReadOnlyProperty, binding);
        }

        public abstract DataGridColumn Create();

        protected Style ToolTipHeaderStyle(string toolTip)
        {
            try
            {
                var style = (Style) Application.Current.FindResource("AzureDataGridColumnHeader");
                style = new Style(typeof(DataGridColumnHeader), style);
                style.Setters.Add(new Setter(ToolTipService.ToolTipProperty, toolTip));
                return style;
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected DataGridTemplateColumn GetDefaultColumn(string subBind, IValueConverter converter = null)
        {
            var col = new DataGridTemplateColumn
            {
                SortMemberPath = ParentOption.BindingPath + subBind
            };

            var binding = new Binding(ParentOption.BindingPath)
            {
                Mode = BindingMode.OneWay
            };
            col.ClipboardContentBinding = binding;

            col.CopyingCellClipboardContent += Col_CopyingCellClipboardContent;
            col.PastingCellClipboardContent += Col_PastingCellClipboardContent;

            col.CellTemplate = GetTextBlockTemplate(subBind, converter);
            col.MinWidth = 80;
            var header = new HeaderTextBlock(ParentOption.Name, ParentOption.BindingPath)
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                TextAlignment = TextAlignment.Center
            }; // TODO: Make bound
            col.Header = header;
            col.HeaderStyle = ToolTipHeaderStyle(ParentOption.ToolTip); // TODO: Make bound
            col.SetValue(BatchViewModel.BasePathProperty, ParentOption.BindingPath);
            return col;
        }

        private void Col_CopyingCellClipboardContent(object sender, DataGridCellClipboardEventArgs e)
        {
            var path = e.Column.GetValue(BatchViewModel.BasePathProperty) as string;
            if (path == null)
                return;
            var option = BindingEvaluator.GetPropertyObject(e.Item as HierarchyRoot, new PropertyPath(path)) as IOption;
            Debug.Assert(option != null, nameof(option) + " != null");
            e.Content = option.ConvertToString();
        }

        private void Col_PastingCellClipboardContent(object sender, DataGridCellClipboardEventArgs e)
        {
            var path = e.Column.GetValue(BatchViewModel.BasePathProperty) as string;
            if (path == null)
                return;
            var option = BindingEvaluator.GetPropertyObject(e.Item as HierarchyRoot, new PropertyPath(path)) as IOption;
            try
            {
                Debug.Assert(option != null, nameof(option) + " != null");
                option.ConvertFromString(e.Content as string);
            }
            catch (Exception)
            {
                Debug.WriteLine("Invalid option format");
            }

            e.Content = null; // Marks as handled so the default does nothing
        }

        protected DataTemplate GetTextBlockTemplate(string subBind, IValueConverter converter = null)
        {
            return TemplateGenerator.CreateDataTemplate(
                () =>
                {
                    var block = new TextBlock
                    {
                        TextWrapping = TextWrapping.Wrap,
                        HorizontalAlignment = HorizontalAlignment.Stretch,
                        VerticalAlignment = VerticalAlignment.Stretch,
                        TextAlignment = TextAlignment.Center
                    };
                    block.VerticalAlignment = VerticalAlignment.Center;

                    var binding = new Binding(ParentOption.BindingPath + subBind)
                    {
                        Mode = BindingMode.OneWay,
                        ValidatesOnDataErrors = true,
                        UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                        NotifyOnValidationError = true
                    };
                    if (converter != null)
                        binding.Converter = converter;
                    BindingOperations.SetBinding(block, TextBlock.TextProperty, binding);

                    binding = new Binding(ParentOption.BindingPath + ".ToolTip")
                    {
                        Mode = BindingMode.OneWay
                    };
                    BindingOperations.SetBinding(block, FrameworkElement.ToolTipProperty, binding);

                    binding = new Binding(ParentOption.BindingPath + ".ValueEqualToReference")
                    {
                        Mode = BindingMode.OneWay,
                        Converter = GridResources["BooleanToFontWeightConverter"] as IValueConverter
                    };
                    BindingOperations.SetBinding(block, TextBlock.FontWeightProperty, binding);

                    var multi = new MultiBinding();
                    multi.Bindings.Add(new Binding(ParentOption.BindingPath + ".Enabled")
                    {
                        Mode = BindingMode.OneWay
                    });
                    multi.Bindings.Add(new Binding(ParentOption.BindingPath + ".Valid")
                    {
                        Mode = BindingMode.OneWay
                    });
                    multi.Converter = GridResources["BooleanToFontBrushConverter"] as IMultiValueConverter;
                    BindingOperations.SetBinding(block, TextBlock.ForegroundProperty, multi);

                    return block;
                });
        }
    }
}