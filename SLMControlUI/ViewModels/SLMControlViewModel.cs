﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.SLMControl.Converter;
using HoloGen.SLMControl.Options;
using HoloGen.UI.Hamburger.ViewModels;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Diagnostics;
using HoloGen.UI.Utils.Commands;

namespace HoloGen.SLMControl.UI.ViewModels
{
    /// <summary>
    /// ViewModel for the <see cref="SLMControlView" /> control.
    /// </summary>
    public class SLMControlViewModel : ObservableObject
    {
        private string _message = default(string);
        private string _numErrors = "";

        private SLMControlOptions _options;
        private ICommand _resetCommand;

        private ICommand _runCommand;
        private string _runName = Resources.Properties.Resources.SetupViewModel_Run;
        private string _runToolTip = "";

        public SLMControlViewModel(SLMControlOptions options)
        {
            Options = options;
            ViewModel = new HamburgerTabViewModel(Options);

            Options.PropertyChanged += Options_PropertyChanged;
            UpdateErrors();
        }

        public Version Version => System.Reflection.Assembly.GetEntryAssembly().GetName().Version;

        public string Title => string.Format(
            Resources.Properties.Resources.MainWindow_SLMControllerV0123PeterChristopher,
            Version.Major,
            Version.Minor,
            Version.Build,
            Version.Revision);

        public string Message
        {
            get => _message;
            set => SetProperty(ref _message, value);
        }

        public SLMControlOptions Options
        {
            get => _options;
            set => SetProperty(ref _options, value);
        }

        public ICommand RunCommand => _runCommand ?? (_runCommand = new SimpleCommand {CanExecuteDelegate = x => true, ExecuteDelegate = x => RunCommandFunc()});

        public ICommand ResetCommand => _resetCommand ?? (_resetCommand = new SimpleCommand {CanExecuteDelegate = x => true, ExecuteDelegate = x => ResetCommandFunc()});

        public string NumErrors
        {
            get => _numErrors;
            set => SetProperty(ref _numErrors, value);
        }

        public string RunToolTip
        {
            get => _runToolTip;
            set => SetProperty(ref _runToolTip, value);
        }

        public string RunName
        {
            get => _runName;
            set => SetProperty(ref _runName, value);
        }

        public HamburgerTabViewModel ViewModel { get; }

        private void Options_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Valid")
                UpdateErrors();
        }

        private void UpdateErrors()
        {
            if (Options.Error.Count > 0)
            {
                NumErrors = Options.Error.Count.ToString();
                RunToolTip = string.Format(Resources.Properties.Resources.SLMControlViewModel_UpdateErrors_SorryYouCanTRunAtTheMomentYouStillHave0Errors, NumErrors);
            }
            else
            {
                NumErrors = "";
                RunToolTip = Resources.Properties.Resources.SLMControlViewModel_UpdateErrors_AreYouReadyToRockNRoll;
            }
        }

        private void ResetCommandFunc()
        {
            Options.Reset();
        }

        private void RunCommandFunc()
        {
            using (new WaitCursor())
            {
                var converter = new ImageConverter(Options);
                converter.Convert(out var error);
                if (error == null)
                {
                    ((MetroWindow) Application.Current.MainWindow).ShowModalMessageExternal(
                        Resources.Properties.Resources.SLMControlViewModel_RunCommandFunc_Success,
                        Resources.Properties.Resources.SLMControlViewModel_RunCommandFunc_ConversionWasSuccessfulNavigateToTheOutputFolderToSeeYourResults);
                }
                else
                {
                    ((MetroWindow) Application.Current.MainWindow).ShowModalMessageExternal(
                        Resources.Properties.Resources.SLMControlViewModel_RunCommandFunc_Error,
                        error);
                }

                try
                {
                    var proc = new ProcessStartInfo
                    {
                        FileName = Options.SLMOutputPage.SLMControlOutputFolder.OutputFolder.Value.FullName,
                        Verb = "open"
                    };
                    Process.Start(proc);
                }
                catch (Exception)
                {
                    // Ignore errors
                }
            }
        }
    }
}