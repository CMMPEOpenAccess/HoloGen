﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.SLMControl.UI.ViewModels;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HoloGen.SLMControl.UI.Views
{
    /// <summary>
    /// Interaction logic for SLMControlView.xaml
    /// </summary>
    public partial class SLMControlView : UserControl, INotifyPropertyChanged
    {
        private ContentControl _bottomPane;

        public SLMControlViewModel ViewModel;

        public SLMControlView()
        {
            InitializeComponent();

            Loaded += SLMControlView_Loaded;
        }

        public ContentControl BottomPane
        {
            get => _bottomPane;
            set
            {
                _bottomPane = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Occurs when property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void SLMControlView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                // ReSharper disable once UsePatternMatching
                var model = DataContext as SLMControlViewModel;
                if ((model != null) & (ViewModel == null))
                {
                    ViewModel = model;

                    // We have to listen to changes in the collection because of the refresh mechanic
                    // of the hamburger control
                    ViewModel.Options.SearchedChildren.View.CollectionChanged += View_CollectionChanged;

                    BottomPane = new OptionsCommandsPanel
                    {
                        DataContext = ViewModel
                    };
                    TabView.BottomPane = BottomPane;
                }
            }
        }

        private void View_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
        }

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;

            changed?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}